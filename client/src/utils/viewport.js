const inViewportPartially = ( top, bottom, left, right, height, width ) => {
	return (
		( bottom >= 0 || top >= 0 ) &&
		( top <= height ) &&
		( left >= 0 || right >= 0 ) &&
		( left <= width )
	);
}

const inViewportCompletely = ( top, bottom, left, right, height, width ) => {
	return (
		top >= 0 &&
		left >= 0 &&
		bottom <= height &&
		right <= width
	);
}

export const inViewport = ( el, partialOk = true ) => {
	const { top, bottom, left, right } = el.getBoundingClientRect();
	const height = window.innerHeight || document.documentElement.clientHeight;
	const width = window.innerWidth || document.documentElement.clientWidth;
	return partialOk ? inViewportPartially( top, bottom, left, right, height, width ) : inViewportCompletely( top, bottom, left, right, height, width )
}

