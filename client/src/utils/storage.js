const localStorageAvailable = () => {
	const test = "__test";
	try {
		localStorage.setItem( test, test );
		localStorage.removeItem(test);
		return true;
	} catch(e) {
		return false;
	}
};

const cache = {};

const store = localStorageAvailable() ? localStorage : {
	getItem: ( key ) => cache[key],
	setItem: ( key, value ) => cache[key] = value,
	removeItem: ( key ) => delete cache[key],
	key: ( i ) => Object.keys( cache) [i],
	length: () => cache.length
};
export default store;
