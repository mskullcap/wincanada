export const firstOfMonth = ( d ) => new Date(d.getFullYear(), d.getMonth(), 1, 0, 0, 0, 0);

const firstOfWeek = ( d ) => {
	const dd = cloneDate( d );
	return new Date( dd.getTime() - dd.getDay() * 86400000 );
};

const startOfDay = ( d ) => cloneDate( d );

const weeksInMonth = ( d ) => {
	const weeks = [];
	let date = firstOfMonth( d );
	const week = getWeek( date );
	weeks.push( daysInWeek( date ));
	for( let i = 1; i < 6; i++ ){
		date = setWeek( d, week + i );
		const diw = daysInWeek( date );
		weeks.push( diw );
	}
	return weeks;
};

const daysInWeek = ( d ) => {
	const week = [];
	const date = firstOfWeek( d );
	for( let i = 0; i < 7; i++ ) week.push( new Date( date.getTime() + i * 86400000));
	return week;
};

const getWeek = ( dd ) => {
	const d = startOfDay( dd );
	d.setHours( 0, 0, 0, 0 );
	const onejan = new Date(d.getFullYear(),0,1);
	return Math.ceil((((d - onejan) / 86400000) + onejan.getDay()+1)/7);
};

const setWeek = ( dd, w ) => {
	const d = firstOfWeek( dd );
	const ww = getWeek( d );
	const www = new Date( d.getTime() + ( w - ww ) * 7 * 86400000 );
	// console.log( `set week ${ w } from ${ dd } with first of week ${ d } : ${ www }` );
	return www;
};

const cloneDate = d => new Date( d.getFullYear(), d.getMonth(), d.getDate(), 2, 0, 0, 0 ); // avoid DST issues

export const sameDate = ( d1, d2 ) => d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth() && d1.getDate() === d2.getDate();

export const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
export const shortDays = days.map( d => d.substr( 0, 1 ));
export const mediumDays = days.map( d => d.substr( 0, 3 ));
export const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
export const mediumMonths = months.map( d => d.substr( 0, 3 ));

export const formatDateCompressed = ( dd, join = "") => {
	const d = new Date( dd );
	const day = d.getDate() < 10 ? "0" + d.getDate() : "" + d.getDate();
	const m = d.getMonth() + 1;
	const month = m < 10 ? "0" + m : "" + m;
	return d.getFullYear() + join + month + join + day;
}

class Month {
	constructor( d ){
		this.date = d;
		this._today = new Date();
	}

	get daysOfWeek(){
		return shortDays;
	}

	get monthsOfYear(){
		return months;
	}

	get weeks(){
		return this._weeks;
	}

	get date(){
		return this._date;
	}

	isToday( d ){
		return sameDate( d, this._today );
	}

	sameDay( d ){
		return sameDate( d, this.date );
	}

	sameMonth( d ){
		return d.getMonth() === this.date.getMonth();
	}

	set date( d ){
		this._date = cloneDate( d );
		this._weeks = weeksInMonth( d );
	}

	get month(){
		return months[this.date.getMonth()];
	}

	get year(){
		return this.date.getFullYear();
	}
}

export default Month;
