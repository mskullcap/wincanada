export const registerListeners = ( el, types, func ) => {
	const remover = [];
	types.forEach( t => {
		el.addEventListener( t , func );
		remover.push( () => el.removeEventListener( t, func ));
	})
	return () => remover.forEach( r => r());
}
