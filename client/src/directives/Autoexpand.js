
const resize = e => e.target.style.flex = "1 0 " + e.target.scrollHeight + "px";

export default function( Vue ){
	Vue.directive( "autoexpand", {
		bind( el, { value } ){
			["input", "change", "cut", "paste", "drop", "keydown"].forEach( type => el.addEventListener( type, resize ));
			setTimeout( () => el.style.flex = `1 0 ${ el.scrollHeight }px`, 200 );
		},
		unbind( el ){
			["input", "change", "cut", "paste", "drop", "keydown"].forEach( type => el.removeEventListener( type, resize ));
		}
	} );
};


// var observe;
// if (window.attachEvent) {
// 	observe = function (element, event, handler) {
// 		element.attachEvent('on'+event, handler);
// 	};
// }
// else {
// 	observe = function (element, event, handler) {
// 		element.addEventListener(event, handler, false);
// 	};
// }
// function init () {
// 	var text = document.getElementById('text');
// 	function resize () {
// 		text.style.height = 'auto';
// 		text.style.height = text.scrollHeight+'px';
// 	}
// 	/* 0-timeout to get the already changed text */
// 	function delayedResize () {
// 		window.setTimeout(resize, 0);
// 	}
// 	observe(text, 'change',  resize);
// 	observe(text, 'cut',     delayedResize);
// 	observe(text, 'paste',   delayedResize);
// 	observe(text, 'drop',    delayedResize);
// 	observe(text, 'keydown', delayedResize);
//
// 	text.focus();
// 	text.select();
// 	resize();
// }
