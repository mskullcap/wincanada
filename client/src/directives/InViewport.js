import { inViewport } from "../utils/viewport";
import { registerListeners } from "../utils/listeners";

export default function (Vue) {
    Vue.directive( "inviewport", {
        bind( el, binding ){
            const { value, modifiers } = binding;
            const onViewable = value;
            const test = () => {
                if( binding.ready === false ) return;
                if( inViewport( el )){
                    onViewable( el );
                    if( modifiers.once ) binding.listeners();
                }
            }
            binding.test = test;
            binding.ready = false;
            binding.listeners = registerListeners( window, ["resize", "scroll"], test );
        },

        inserted( el, binding ){
            binding.ready = true;
            binding.test();
        },

        unbind( el, binding ){
            binding.listeners();
        }
    });
};
