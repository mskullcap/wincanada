import marked from "marked";

export default function (Vue) {
	const mark = ( el, value ) => {
		if( !value ) return;
		el.innerHTML = marked( value );
	};

	Vue.directive( "marked", {
		bind( el, { value }){
			mark( el, value )
		},
		update( el, { value }){
			mark( el, value )
		}
	});
};
