const mask = ( data, mask ) =>{
	if( !mask ) return data;
	let text = "";
	for( let i = 0, x = 1; x && i < mask.length; ++i ){
		let c = data.charAt( i );
		let m = mask.charAt( i );

		switch( m ){
			case "#" :
				if( /\d/.test( c )) text += c;
				else x = 0;
				break;
			case "A" :
				if( /[a-z]/i.test( c )) text += c;
				else x = 0;
				break;
			case "N" :
				if( /[a-z0-9]/i.test( c )) text += c;
				else x = 0;
				break;
			case "X" :
				text += c;
				break;
			default  :
				text += m;
				break;
		}
	}
	return text;
};

const handler = ({ target }) => {
	const { previousValue } = target.dataset;
	if( previousValue && previousValue.length < target.value.length) target.value = mask( target.value, this.format );
	target.dataset.previousValue = target.value;
};

export default function (Vue) {
	Vue.directive( "mask", {
		bind( el, { value }){
			const handlerFunc = handler.bind({ format: value });
			el.addEventListener("input", handlerFunc, false);
			return handlerFunc({target: el});
		},
		unbind: el => el.removeEventListener( "input", handler, false )
	});
};
