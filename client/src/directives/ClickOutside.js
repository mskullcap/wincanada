export default function (Vue) {
	Vue.directive( "clickoutside", {
		bind( el, { expression }, vnode ){
			const listener = event => {
				if( el === event.target || el.contains( event.target )){
//					setTimeout(() => vnode.context[expression]( event ), 300 );
				} else {
					vnode.context[expression]( event );
				}
			};

			document.body.addEventListener( "click", listener );
			el.listeners = () => document.body.removeEventListener( "click", listener );
		},

		unbind( el ){
			el.listeners();
		}
	});
};
