import Vue from "vue"
import App from "./App.vue"
import { sync } from "vuex-router-sync"
import store from "./store"
import router from "./router"

import "date-input-polyfill"
import ClickOutside from "./directives/ClickOutside"
import Mask from "./directives/Mask"
import Marked from "./directives/Marked"
import Autoexpand from "./directives/Autoexpand"
import Analytics from "vue-analytics"

Vue.config.productionTip = false;

Vue.use( ClickOutside );
Vue.use( Mask );
Vue.use( Marked );
Vue.use( Autoexpand );
Vue.use( Analytics, {
	id: "UA-106541214-1",
	router
});

sync( store, router );

const app = new Vue({
	el: "#app",
	router,
	store,
	template: "<App/>",
	components: { App }
});

export { store, router, app };
