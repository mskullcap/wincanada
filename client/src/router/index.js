import Vue from "vue";
import Router from "vue-router";

import HomeView from "../views/Home/HomeView.vue";
import CalendarView from "../views/Calendar/CalendarView.vue";
import MembersView from "../views/Members/MembersView.vue";
import ConferenceView from "../views/Conference/ConferenceView.vue";
import AboutView from "../views/About/AboutView.vue";
import NewsItemView from "../views/Home/News/NewsItemView.vue";
import ConfirmView from "../views/Confirm/ConfirmView.vue";
import SpeakersView from "../views/Speakers/SpeakersView.vue";

const ProfileView = () => import( "../views/Profile/ProfileView.vue" );
const UsersView = () => import( "../views/Admin/Users/Users.vue" );
const MailChimp = () => import( "../views/Admin/MailChimp/MailChimp.vue" );
const Publisher = () => import( "../views/Admin/Publishing/Publisher.vue" );
const LoginView = () => import( "../views/Login/LoginView.vue" );
const PasswordView = () => import( "../views/Password/PasswordView.vue" );

import store from "../store";

Vue.use( Router );

const router = new Router({
	mode: "history",
	scrollBehavior( to, from, savedPosition ){
		if( savedPosition ) return savedPosition;
		if( to.hash ) return { selector: to.hash };
		if( to.meta.restoreScroll ) return { x: 0, y: document.body.scrollTop };
		return { x: 0, y: 0 }
	},
	routes: [
		{ path: "/profiles/:username", name: "My Profile", component: ProfileView, props: true },
		{ path: "/confirm", name: "Confirmation", component: ConfirmView, props: ( route ) => ( { token: route.query.token } )},
		{ path: "/password", name: "Password ", component: PasswordView, props: ( route ) => ( { token: route.query.token } )},
		{ path: "/admin/users", name: "Admin: Users", component: UsersView },
		{ path: "/admin/publish", name: "Admin: Publisher", component: Publisher },
		{ path: "/admin/mailinglist", name: "Admin: Mailing List", component: MailChimp },
		{ path: "/calendar", name: "Calendar", component: CalendarView },
		{ path: "/members", name: "Our Members", component: MembersView },
		{ path: "/conference", name: "Conference", component: ConferenceView },
		{ path: "/speakers", name: "Speakers", component: SpeakersView },
		{ path: "/about", name: "About", component: AboutView },
		{ path: "/login", name: "Login", component: LoginView },
		{
			path: "/",
			component: HomeView,
			name: "News",
			children: [
				{
					path: ":type/:id",
					component: NewsItemView,
					meta: { restoreScroll: true }
				}
			]
		},
		{ path: "*", redirect: "/" }
	]
});

// const guardItems = ( to, from, next ) => {
// 	const type = to.params.type;
// 	const id = to.params.id;
// 	if( !( type === "news" || type === "events" )){
// 		next( false );
// 		return;
// 	}
//
// 	console.log( store.state[type]);
//
// 	const item = store.state[type].find( n => n.id === id );
// 	console.log( item );
// 	item ? next() : next( "/" );
// }

router.afterEach(( to, from ) => {
	store.dispatch( "syncRoute", { to, from });
});

export default router;
