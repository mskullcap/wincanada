import Vue from "vue";
import Vuex from "vuex";

import { registerListeners } from "../utils/listeners";
import debounce from "lodash.debounce";

import {
	updateUser,
	loadPublicProfile,
	deleteUser,
	listMailChimpMembers,
	listUsers,
	listProfiles,
	listNews,
	listContacts,
	listChapters,
	listEvents,
	loadProfile,
	login,
	logout,
	loadSession,
	createNews,
	updateNews,
	deleteNews,
	createEvent as apiCreateEvent,
	updateEvent as apiUpdateEvent,
	deleteEvent as apiDeleteEvent
} from "../api"

Vue.use( Vuex );

const routeSnapshot = ( r ) =>{
	if( !r ) return undefined;
	return Object.freeze( {
		name: r.name,
		path: r.path,
		hash: r.hash,
		query: r.query,
		params: r.params,
		fullPath: r.fullPath,
		meta: r.meta
	});
};

const historyEntry = ( to, from ) => {
	return {
		to: routeSnapshot( to ),
		from: routeSnapshot( from )
	}
};

const isLoggedIn = session => session && session.username;

const hasAnyRoles = ( session, roles ) => {
	if( !isLoggedIn( session ) ) return false;
	if( roles.constructor !== Array ) roles = [roles];
	for( let i = 0; i < roles.length; i++ ){
		const result = session.roles.find( r => r === roles[i] );
		if( result ) return true;
	}
	return false;
};


const store = new Vuex.Store({
	state: {
		windowSize: 0,
		activeProfile: undefined,
		profiles: [],
		users: [],
		chapters: [],
		contacts: [],
		news: [],
		events: [],
		history: [],
		session: loadSession(),
		requestErrors: []
	},

	getters: {
		isAdmin: state => hasAnyRoles( state.session, "Administrator" ),
		isSuperUser: state => hasAnyRoles( state.session, "Super User" ),
		isChapterLead: state => hasAnyRoles( state.session, "Chapter Lead" ),
		isLoggedIn: state => state.session && state.session.username,

		unpublishedNews: state => {
			return state.news.filter( n => n.publishedOn === null );
		},

		publishedNews: state => {
			return state.news.filter( n => n.publishedOn !== null );
		},

		historyLength: state => state.history.length,

		previousRoute: state => {
			if( state.history.length === 0 ) return "/";
			const mostRecent = state.history[state.history.length - 1];
			if( !mostRecent.from ) return "/";
			return mostRecent.from.fullPath;
		}
	},

	actions: {
		resize({ commit }, size ){
			commit( "WINDOW_RESIZE", size );
		},

		syncRoute( { commit }, r ){
			commit( "ROUTE_SYNC", r );
		},

		requestError({ commit }, details ){
			commit( "REQUEST_ERROR", details );
		},

		clearRequestErrors({ commit }){
			commit( "CLEAR_REQUEST_ERRORS" );
		},

		async login({ commit }, { username, password, redirect }) {
			try{
				const after = await login( username, password, redirect );
				commit( "SIGNED_IN", after.token );
				after.redirect();
			} catch( err ){
				commit( "SIGN_IN_ERROR", err );
			}
		},

		clearSession({ commit }, hard = true ){
			commit( "SESSION_CLEARED" );
			if( hard ) logout();
		},

		refreshThumbnail({ commit }, username ){
			commit( "REFRESH_THUMBNAIL", username );
		},

		clearActiveProfile( { commit } ){
			commit( "CLEAR_PROFILE" );
		},

		async updateActiveProfile( { commit }, profile ){
			await updateUser( profile );
			commit( "UPDATE_ACTIVE_PROFILE", profile );
		},

		async fetchProfile( { commit, getters }, username ){
			try {
				const { session } = store.state;
				if( getters.isAdmin || getters.isSuperUser ){
					commit( "PROFILE_LOADED", await loadProfile( username ));
					return;
				}
				if( getters.isLoggedIn && session.username === username ){
					commit( "PROFILE_LOADED", await loadProfile( username ));
					return;
				}
				commit( "PROFILE_LOADED", await loadPublicProfile( username ));
			} catch( err ){
				commit( "SESSION_CLEARED" );
			}
		},

		async loadPublicProfile( { commit }, username){
			try {
				// if admin or super, or I own the profile, load the profile
				// otherwise attempt to load the public version
				commit( "PROFILE_LOADED", await loadPublicProfile( username ));
			} catch( err ){
				commit( "SESSION_CLEARED" );
			}
		},

		async fetchUsers({ commit }) {
			try {
				if( Object.keys( store.state.users ).length < 5 ) commit("SET_USERS", await listUsers());
			} catch( err ){
				commit( "SESSION_CLEARED" );
			}
		},

		async deleteUser({ commit }, username ) {
			try {
				await deleteUser( username );
				commit( "DELETE_USER", username );
			} catch( err ){
				// commit( "SESSION_CLEARED" );
			}
		},

		async fetchMailChimpMembers({ commit }, chapter ) {
			try {
				if( Object.keys( store.state.users ).length < 5 ) commit("ADD_MAILCHIMPMEMBERS", await listMailChimpMembers( chapter ));
			} catch( err ){
				commit( "SESSION_CLEARED" );
			}
		},

		async fetchProfiles({ commit }, publish = false ) {
			try {
				if( store.state.profiles.length === 0 || publish ) commit("SET_PROFILES", await listProfiles( publish ));
			} catch( err ){
				commit( "SESSION_CLEARED" );
			}
		},

		async createNewsItem({ commit }){
			const item = await createNews();
			commit( "CREATE_NEWS_ITEM", item );
		},

		async updateNewsItem({ commit }, item ){
			item = await updateNews( item );
			commit( "UPDATE_NEWS_ITEM", item );
		},

		async deleteNewsItem({ commit }, item ) {
			try {
				await deleteNews( item );
				commit( "DELETE_NEWS_ITEM", item );
			} catch( err ){
				// commit( "SESSION_CLEARED" );
			}
		},

		async fetchNews({ commit }, publish = false) {
			try {
				if( !store.state.news.length || publish ) commit("SET_NEWS", await listNews( publish ));
			} catch( err ){
				// ok, this is public api, so if it fails, we should just report it - NOT clear the session
				// err.status, err.location, err.statusText
				commit( "SESSION_CLEARED" );
			}
		},

		async fetchContacts({ commit }, publish = false) {
			try {
				if( !store.state.contacts.length || publish ) commit("SET_CONTACTS", await listContacts( publish ));
			} catch( err ){
				// ok, this is public api, so if it fails, we should just report it - NOT clear the session
				// err.status, err.location, err.statusText
				commit( "SESSION_CLEARED" );
			}
		},

		async createEvent({ commit }){
			const item = await apiCreateEvent();
			commit( "CREATE_EVENT", item );
		},

		async updateEvent({ commit }, item ){
			item = await apiUpdateEvent( item );
			commit( "UPDATE_EVENT", item );
		},

		async deleteEvent({ commit }, item ) {
			try {
				await apiDeleteEvent( item );
				commit( "DELETE_EVENT", item );
			} catch( err ){
				// commit( "SESSION_CLEARED" );
			}
		},

		async fetchEvents({ commit }, publish = false ) {
			try{
				if( !store.state.events.length || publish ) commit("SET_EVENTS", await listEvents( publish ));
			} catch( err ){
				commit( "SESSION_CLEARED" );
			}
		},

		async fetchChapters({ commit }, publish = false ) {
			try{
				if( !store.state.chapters.length || publish ) commit("SET_CHAPTERS", await listChapters( publish ));
			} catch( err ){
				commit( "SESSION_CLEARED" );
			}
		}
	},

	mutations: {
		UPDATE_THUMBNAIL: ( state, item ) => state[item.type].find( i => i.id === item.id ).thumbnail = item.thumbnail,
		SET_NEWS: (state, news ) => state.news = news,
		DELETE_USER: (state, username ) => {
			const index = state.users.findIndex( u => u.username === username );
			if( index !== -1 ) state.users.splice( index, 1 );
		},
		SET_USERS: (state, users ) => state.users = users,
		SET_EVENTS: (state, events) => state.events = events,
		SET_CHAPTERS: (state, chapters ) => state.chapters = chapters,
		SET_CONTACTS: (state, contacts ) => state.contacts = contacts,
		SIGNED_IN: ( state, session ) => {
			state.session = session;
			console.log( "session set" );
		},
		SIGN_IN_ERROR: ( state, session ) => state.session = session,
		SESSION_CLEARED: ( state ) => {
			state.users = [];
			state.requestErrors = [];
			state.activeProfile = undefined;
			state.session = loadSession();
		},
		UPDATE_ACTIVE_PROFILE: ( state, profile ) => {
			console.log( "UPDATE_ACTIVE_PROFILE", profile );
			state.activeProfile = undefined;
			state.activeProfile = profile;
			state.profiles = []; // force a reload in case we went public with a profile
			store.dispatch( "fetchProfiles" );
		},
		CLEAR_PROFILE: ( state ) => state.activeProfile = undefined,
		SET_PROFILES: (state, profiles ) => state.profiles = profiles,
		PROFILE_LOADED: ( state, profile ) => state.activeProfile = profile,
		ROUTE_SYNC: ( state, { to, from } ) => state.history.push( historyEntry( to, from ) ),
		WINDOW_RESIZE: ( state, size ) => state.windowSize = size,
		CLEAR_REQUEST_ERRORS: state => {
			state.requestErrors = []
		},
		REQUEST_ERROR: ( state, err ) => {
			state.requestErrors.push( err );
		},
		REFRESH_THUMBNAIL: ( state, username ) => {
			const cachebuster = "cachebuster=" + Date.now();
			if( !username ){
				if( state.session && state.session.thumbnail ){
					state.session.thumbnail = state.session.thumbnail.replace( /thumbnail\.jpg.*$/, `thumbnail.jpg?${ cachebuster }`);
				}
				return;
			}

			const p = state.profiles.find( p => p.username === username );
			if( p ) p.thumbnail = p.thumbnail.replace( /thumbnail\.jpg.*$/, `thumbnail.jpg?${ cachebuster }`);
		},
		CREATE_EVENT: ( state, item ) => {
			state.events.unshift( item );
		},
		UPDATE_EVENT: ( state, item ) => {
			const index = state.events.findIndex( n => n.id === item.id );
			if( index === -1 ) return;
			state.events.splice(index, 1, item);
		},
		DELETE_EVENT: ( state, item ) => {
			const index = state.events.findIndex( u => u.id === item.id );
			if( index !== -1 ) state.events.splice( index, 1 );
		},
		CREATE_NEWS_ITEM: ( state, item ) => {
			state.news.unshift( item );
		},
		UPDATE_NEWS_ITEM: ( state, item ) => {
			const cachebuster = "cachebuster=" + Date.now();
			const index = state.news.findIndex( n => n.id === item.id );
			if( index === -1 ) return;
			item.thumbnail = state.news[index].thumbnail.replace( /thumbnail\.jpg.*$/, `thumbnail.jpg?${ cachebuster }`);
			console.log( "updated item ", item );
			state.news.splice(index, 1, item);
		},
		DELETE_NEWS_ITEM: ( state, item ) => {
			const index = state.news.findIndex( u => u.id === item.id );
			if( index !== -1 ) state.news.splice( index, 1 );
		}
	}
});

registerListeners( window, ["resize"], debounce(
	( size ) => store.dispatch( "resize", { width: window.innerWidth, height: window.innerHeight })
	, 300
));
store.dispatch( "resize", { width: window.innerWidth, height: window.innerHeight });

export default store;
