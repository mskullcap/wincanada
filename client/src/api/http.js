import store from "../utils/Storage";
import HttpService from "./HttpService";
import vuexStore from "../store";
import { redirectToLogin, logout } from "./index";

const http = new HttpService();

const TOKEN = "win_jwt";

http.getToken = () => store.getItem( TOKEN );

http.addRequestHandler( xhr => {
	const token = store.getItem( TOKEN );
	if( token ) xhr.setRequestHeader( "Authorization", `Bearer ${ token }` );
});

http.addRequestHandler(( xhr, url, method, type ) => {
	if( type === "json" ){
		xhr.setRequestHeader( "Accept", "application/json" );
		xhr.setRequestHeader( "Content-Type", "application/json" );
	} else if( type === "text" ) {
		xhr.setRequestHeader( "Accept", "text/plain" );
		xhr.setRequestHeader( "Content-Type", "text/plain" );
	}
});

http.addResponseHandler( xhr => {
	const authHeader = xhr.getResponseHeader( "Authorization" );
	if( !authHeader ) return;

	const token = store.getItem( TOKEN );
	const t = authHeader.substring( "Bearer ".length );
	if( !token || token !== t ) store.setItem( TOKEN, t );
});

http.addErrorHandler( status  => {
	if( status !== -1 ) return false;
	return true; // ignore cancelled requests
});

http.addErrorHandler(( status, xhr, url, method, type, body )  => {
	if( [422].indexOf( status ) !== -1 ) return true;
	if( [401].indexOf( status ) === -1 ) return false;
	logout();
	return true;
});

http.addErrorHandler(( status, xhr, url, method, type, body, error )  => {
	vuexStore.dispatch( "requestError", error );
	return true;
});

http.addResetHandler(() => {
	store.removeItem( TOKEN );
});

export default http;
