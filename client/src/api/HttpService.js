export const objectToQueryString = ( queryParams ) =>{
	return "?" + Object.keys( queryParams )
		.reduce(
			( tmp, key ) =>{
				tmp.push( key + "=" + encodeURIComponent( queryParams[key] ) );
				return tmp;
			}, []
		)
		.join( "&" );
};

export const queryStringToObject = ( queryString ) =>{
	if(queryString[0] === "?") return queryStringToObject( queryString.substring( 1 ));
	let obj = {};
	if( queryString ){
		queryString.split( '&' ).map( ( item ) =>{
			const [k, v] = item.split( '=' );
			return v ? obj[k] = v : null;
		} );
	}
	return obj;
};

class HttpService {
	rejectAll = false;
	requests = [];
	urlModifiers = [];
	responseHandlers = [];
	errorHandlers = [];
	requestHandlers = [];
	resetHandlers = [];

	constructor( createXMLHttpRequest = () => new XMLHttpRequest()){
		this.createXMLHttpRequest = createXMLHttpRequest; // in case we want to mock XMLHttpRequest
	}

	addResetHandler( rh ){
		this.resetHandlers.push( rh );
	}

	addUrlModifier( um ){
		this.urlModifiers.push( um );
	}

	addRequestHandler( rh ){
		this.requestHandlers.push( rh );
	}

	addResponseHandler( rh ){
		this.responseHandlers.push( rh );
	}

	addErrorHandler( eh ){
		this.errorHandlers.push( eh );
	}

	deleteCookie( name ){
		document.cookie = `${ name }=;expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
	}

	setCookie( name, value, expiresFromNow ){
		const expires = new Date( Date.now() + expiresFromNow);
		document.cookie = `${ name }=${ value }; expires=${ expires.toUTCString() }; path=/`;
	}

	get( url, cancellable = true ){
		return this.getJson( url, cancellable );
	}

	getText( url, cancellable = true ){
		return this.fetch( url, "GET", undefined, undefined, cancellable );
	}

	getJson( url, cancellable = true ){
		return this.fetch( url, "GET", "json", undefined, cancellable );
	}

	post( url, body, cancellable = true, headers ){
		const type = typeof body === "string" ? "json" : "multipart/form-data";
		return this.fetch( url, "POST", type, body, cancellable, headers );
	}

	delete( url, cancellable = true ){
		return this.fetch( url, "DELETE", "json", undefined, cancellable );
	}

	cancelAll(){
		this.requests.forEach( r => {
			if( r.cancellable ) r.cancel()
		} );
	}

	reset(){
		this.rejectAll = true;
		this.cancelAll();
		return new Promise( resolve =>{
			this.rejectAll = false;
			this.resetHandlers.forEach( rh => rh());
			resolve( true );
		} );
	}

	currentUrl(){
		return location.pathname + location.search + location.hash;
	}

	createError( status, statusText, url, method, type, xhr ){
		const contentType = xhr.getResponseHeader( "content-type" );
		let body = xhr.response.toString();
		if( contentType.indexOf( "json" ) !== -1 ) body = JSON.parse( body );

		const reason = xhr.getResponseHeader( "x-status-reason" );

		return {
			timestamp: Date.now(),
			status,
			statusText,
			url,
			method,
			type,
			body,
			reason
		}
	};

	fetch( u, method, type, body, cancellable = true, headers ){
		if( this.rejectAll ) throw new Error( "The http service is not ready." );

		let url = u;
		this.urlModifiers.forEach( um => url = um( url, method, body ));

		const active = this.requests.find( r => r.url === url && r.method === method );
		if( active ) return active.request;

		let cancellor = () => {};
		const xhr = this.createXMLHttpRequest();
		if( headers ) Object.keys( headers ).forEach(( k => xhr.setRequestHeader( k, headers[k] )));

		xhr.open( method, url );

		this.requestHandlers.forEach( rh => rh( xhr, url, method, type, body ));

		const req = new Promise( ( resolve, reject ) =>{
			const _http = this;
			xhr.onload = function(){ // we need "this" defined to the block to access the status for xhr
				_http.removeRequest( method, url );
				const { status } = this;
				if( status >= 200 && status <= 300 ){
					let content;
					if( type === "json" ) content = JSON.parse( xhr.response.toString());
					else content = xhr.responseText ? xhr.responseText : xhr.response;
					_http.responseHandlers.forEach( rh => rh( xhr, content, reject, method, type, body ));
					resolve( content );
					return;
				}
				const error = _http.createError( status, undefined, url, method, type, xhr );
				_http.errorHandlers.some( rh => rh( status, xhr, url, method, type, body, error )); // note we we stop processing if handler returns true
				reject( error );
			};
			cancellor = () => {
				xhr.abort();
				_http.removeRequest( method, url );
				reject( _http.createError( -1, "Cancelled", url, method, type, body ) );
			};
		} );

		this.requests.push( { url, request: req, cancel: cancellor, method, cancellable } );

		if( body ) xhr.send( body );
		else xhr.send();

		return req;
	};

	removeRequest( method, url ){
		const i = this.requests.findIndex( r => r.url === url && r.method === method );
		if( i === -1 ) return false;
		this.requests.splice( i, 1 );
		return true;
	}
}

export default HttpService;

