import jwt_decode from "jwt-decode";
import router from "../router";
import http from "./http";
import { formatDateCompressed as formatDate } from "../utils/Month";

const PUBLIC_API = "/pub";
const PRIVATE_API = "/api";

const websafeUsername = username => encodeURIComponent( username ).replace( /[.]/, "%2E" );

export const redirectToLogin = () => {
	router.replace( `/login?redirect=${router.currentRoute.fullPath}` );
};

export const login = ( username, password, redirect ) => {
	return http.post(`${ PRIVATE_API }/login`, JSON.stringify({
		username: username,
		password: password
	}))
		.then( token => decodeToken( token.token ))
		.then( decodedToken => {
			const r = () => {
				if( !redirect ) router.replace( `/profiles/${ websafeUsername( decodedToken.username ) }` );
				else if( redirect.indexOf( "login" ) !== -1 ) router.replace( "/" );
				else router.replace( redirect );
			};
			return {
				token: decodedToken,
				redirect: r
			};
		})
		.catch( oops => Promise.reject({ ...oops, error: "Invalid username or password."}))
};

export const gotoProfile = username => router.push( `/profiles/${ websafeUsername( username ) }` );

export const join = ( email, username, password ) => {
	return http.post(`${ PRIVATE_API }/join`, JSON.stringify({ email, username, password }));
};

export const resetPassword = username => http.get( `${ PRIVATE_API }/password/${ websafeUsername( username )}`);

export const updatePassword = ( token, password ) => {
	return http.post( `${ PRIVATE_API }/password`, JSON.stringify( {
		token,
		password
	} ));
};

export const logout = () => {
	http.reset().then(() => window.location.href = "/" );
};

export const ipgeolocate = () => http.get( `${ PRIVATE_API }/geo` );

export const listNews = ( publish ) => {
	let n;
	if( publish ) n = http.get( `${ PRIVATE_API }/news?publish=true?since=-1 years` );
	else n = http.get( `${ PUBLIC_API }/news.json` );

	return n.then( news => {
			return news.map( p => ({
				type: "news",
				published: p.publishedOn !== null,
				thumbnail: `/static/news/${ p.id }/thumbnail.jpg`,
				image: `/static/news/${ p.id }/image.jpg`,
				...p
			}));
		}
	);
};

export const updateNews = item => {
	return http.post( `${ PRIVATE_API }/news/${ item.id }`, JSON.stringify( item ))
		.then( p =>  ({
			type: "news",
			published: p.publishedOn !== null,
			thumbnail: `/static/news/${ p.id }/thumbnail.jpg`,
			image: `/static/news/${ p.id }/image.jpg`,
			...p
		})
	);
};

export const createNews = () => {
	return http.post( `${ PRIVATE_API }/news`, JSON.stringify( {
		title: "Add a title",
		description: "Add a description",
		content: "<p>Add some content</p>"
	} )).then( p => ({
		type: "news",
		published: p.publishedOn !== null,
		thumbnail: `/static/news/${ p.id }/thumbnail.jpg`,
		image: `/static/news/${ p.id }/image.jpg`,
		...p
	}));
};

export const deleteNews = item => http.delete( `${ PRIVATE_API }/news/${ item.id }` );

export const confirmUser = ( $data ) => http.post( `${ PRIVATE_API }/confirm`, JSON.stringify( $data ));

export const deleteUser = username => http.delete( `${ PRIVATE_API }/users/${ websafeUsername( username )}` );

export const listUsers = () => http.get( `${ PRIVATE_API }/users` ).then( users => {
	users.forEach( user => {
		user.roles = user.roles.split( "," ).filter( role => role !== "Standard" );
		user.searchFields = `${ user.firstName } ${ user.lastName } ${ user.username } ${ user.email }`.toLowerCase();
	});
	return users;
});

export const updateUser = user => {
	const u = JSON.parse( JSON.stringify( user ));
	u.public = user.public === true || user.public == 1 ? 1 : 0;
	u.optIn = user.optIn === true || user.optIn == 1 ? 1 : 0;
	return http.post( `${ PRIVATE_API }/users/${ websafeUsername( user.username )}`, JSON.stringify( u ));
};

export const listMailChimpMembers = ( chapter = "Canada" ) => http.get( `${ PRIVATE_API }/mc/${ chapter }/members` ).then( users => {
	users.forEach( user => {
		console.log( user );
		// user.roles = user.roles.split( "," );
		// user.searchFields = `${ user.firstName } ${ user.lastName } ${ user.username } ${ user.email } ${ user.chapter } `.toLowerCase();
	});
	return users;
});

export const listContacts = ( publish ) => {
	if( publish ) return http.get( `${ PRIVATE_API }/contacts?publish=true` );
	return http.get( `${ PUBLIC_API }/contacts.json` );
};

export const listChapters = ( publish ) => {
	if( publish ) return http.get( `${ PRIVATE_API }/chapters?publish=true` );
	return http.get( `${ PUBLIC_API }/chapters.json` );
};

export const mailChimpDiff = ( chapter = "Canada" ) => http.get( `${ PRIVATE_API }/mc/${ chapter }/diff` );

export const loadProfile = ( username ) => http.get( `${ PRIVATE_API }/users/${ websafeUsername( username ) }` ).then( profile => {
	profile.thumbnail = `/static/profiles/${ websafeUsername( profile.username )}/thumbnail.jpg`;
	profile.name = `${profile.firstName} ${profile.lastName}`;
	profile.public = profile.public == 1 ? true : false;
	profile.optIn = profile.optIn == 1 ? true : false;
	profile.roles = profile.roles.split( "," );
	profile.readonly = false;
	return profile;
});

export const loadPublicProfile = ( username ) => http.get( `${ PRIVATE_API }/public/users/${ websafeUsername( username ) }` ).then( profile => {
	profile.thumbnail = `/static/profiles/${ websafeUsername( profile.username )}/thumbnail.jpg`;
	profile.name = `${profile.firstName} ${profile.lastName}`;
	profile.readonly = true;
	return profile;
});

export const updateConference = item => {
	return http.post( `${ PRIVATE_API }/conference`, JSON.stringify( item ))
		.then( conference => conference );
};

export const listProfiles = ( publish ) => {
	let p;
	if( publish ) p = http.get( `${ PRIVATE_API }/users?publish=true` );
	else p = http.get( `${ PUBLIC_API }/profiles.json` );

	return p.then( profiles => {
		return profiles.map( p => ({
			firstName: p[0],
			lastName: p[1],
			name: `${p[0]} ${p[1]}`,
			username: p[2],
			job: p[3],
			employer: p[4],
			chapter: p[5],
			thumbnail: `/static/profiles/${ websafeUsername( p[2] )}/thumbnail.jpg`,
			searchFields: `${ p[0] } ${ p[1] } ${ p[3] } ${ p[4] }`.toLowerCase()
		}));
	});
};

export const listEvents = ( publish ) => {
	let p;
	if( publish ) p = http.get( `${ PRIVATE_API }/events?publish=true&since=-1 year` );
	else p = http.get( `${ PUBLIC_API }/events.json` );

	return p.then( events => {
		events.forEach(( event, i ) => {
			event.type = "event";
			const start = event.startDate.split( "-" );
			const end = event.endDate.split( "-" );
			event.startDate = new Date( start[0], start[1]-1, start[2], 0, 0, 0, 0 );
			event.endDate = new Date( end[0], end[1]-1, end[2], 0, 0, 0, 0 );
			if( i === 0 ){
				event.firstEventInMonth = true;
			} else {
				const previousEvent = events[i-1];
				if( previousEvent.startDate.getMonth() !== event.startDate.getMonth() ) event.firstEventInMonth = true;
			}
		});
		return events;
	});
};

export const createEvent = () => {
	// require title, description, content, start_date, end_date, city, province, country
	return http.post( `${ PRIVATE_API }/events`, JSON.stringify( {
		title: "Add a title",
		description: "Add a short description",
		content: "Add your main content",
		location: "Add the venue, city, province, country",
		chapter: "Canada",
		startDate: formatDate( new Date(), "-" ),
		endDate: formatDate( new Date( (new Date()).getTime() + 24 * 60 * 60 * 1000 ), "-" )
	} ))
		.then( event =>  {
			const start = event.startDate.split( "-" );
			const end = event.endDate.split( "-" );
			event.startDate = new Date( start[0], start[1]-1, start[2], 0, 0, 0, 0 );
			event.endDate = new Date( end[0], end[1]-1, end[2], 0, 0, 0, 0 );
			return event;
		});
};

export const updateEvent = item => {
	const cloned = JSON.parse( JSON.stringify( item ));
	cloned.startDate = formatDate( item.startDate, "-" );
	cloned.endDate = formatDate( item.endDate, "-" );

	return http.post( `${ PRIVATE_API }/events/${ item.id }`, JSON.stringify( cloned ))
		.then( event =>  {
			const start = event.startDate.split( "-" );
			const end = event.endDate.split( "-" );
			event.startDate = new Date( start[0], start[1]-1, start[2], 0, 0, 0, 0 );
			event.endDate = new Date( end[0], end[1]-1, end[2], 0, 0, 0, 0 );
			return event;
		});
};

export const deleteEvent = item => http.delete( `${ PRIVATE_API }/events/${ item.id }` );

export const loadSession = () => {
	return decodeToken( http.getToken());
};

const decodeToken = ( token ) => {
	if( !token || token === null ) return undefined;
	try {
		const jwt = jwt_decode( token );
		const exp = new Date( 1000 * jwt.exp );
		if( Date.now() > exp.getTime()){
			http.reset();
			return undefined;
		}
		return {
			"status" : 200,
			"iat": new Date( 1000 * jwt.iat ),
			"exp": new Date( 1000 * jwt.exp ),
			"thumbnail": `/static/profiles/${ websafeUsername( jwt.context.username )}/thumbnail.jpg`,
			...jwt.context
		};
	} catch( e ){
		console.error( e );
		return undefined;
	}
};


const uploadPhoto = ( type, key, file ) => {
	return upload( `${ type }/${ key }/thumbnail`, file )
};

const upload = ( path, file ) => {
	const formData = new FormData();
	formData.append( "file", file );

	return http.post( `${ PRIVATE_API }/${ path }`, formData )
		.then( resp => JSON.parse( resp.toString())
	);
};


export const updateUserPhoto = ( username, file ) => uploadPhoto( "users", username, file );
export const updateProfilePhoto = ( username, file ) => uploadPhoto( "profiles", username, file );
export const updateNewsPhoto = ( id, file ) => uploadPhoto( "news", id, file );
export const updateEventPhoto = ( id, file ) => uploadPhoto( "events", id, file );
export const updateConferenceBackgroundPhoto = ( file ) => upload( "conference/background", file );
export const updateConferenceSponsorsPhoto = ( file ) => upload( "conference/sponsors", file );
