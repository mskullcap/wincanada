# WiN Canada

In the rewrite of the win-canada.org site, I recreated the whole app with a more modern
approach.  The api written in PHP (using Slim), the frontend is a single page
web app written using the framework vuejs, and the data store is a sqlite database.  Security is 
handled with JWT.

Because the original win-canada site is mostly static content, I tried to take advantage of this by
"publishing" from the sqlite database to static files, and to drive the UI mostly from 
these static files.  Whenever and administrator changes data in the db, we need to publish the 
changes through the api.  So really I have create a mostly static site generator.  I am hoping
the benefits of this approach will let us host in very modest server environments, and to
have very high performance.

##Repo
`https://mskullcap@bitbucket.org/mskullcap/wincanada.git`

## Install
This project expects you to have PHP 5.6+, composer and npm installed.
~~~
cd server
php composer.phar install

cd ../client
npm install

cd ../data/mysql2sqlite
npm install

cd ..
./populate.sh
~~~
The populate.sh script creates the sqlite database and imports mysql data, so the 
following steps aren't really required.

## Create: SQLite Database

To create a new database in `_db/win.db`
~~~
./data/sqlite/create.sh
~~~

## Import: from MySQL

1. Go to justhost.com and add your IP so you can connect to mysql remotely.
1. Run `scripts/mysql2sqlite/import.sh`


## After: Fix Imported Data

Chapters are a bit messed up in mysql, so after import you need to massage the sqlite db a bit.
./data/sqlite/after.sh

## Update PHP Packages

If you need to update php to the latest packages:
`php composer.phar update`

## Run PHP for Development

Go to the dir api/public and run
~~~
php -S 0.0.0.0:8080 index.php
~~~

## Update PHP Packages
~~~
cd server
php composer.phar update
~~~

Note the 0.0.0.0... for some reason if I use localhost I can't proxy to this server through
the node server.  Also, make sure you specify index.php or else you cannot have dots in the path 
(see: https://github.com/marmelab/phpcr-browser/issues/20).

To minimize cpu/database usage, most of the content for the site will be based 
around the concept of publishing from the database to json files that will be served
as static assets.

## Localhost Security Issues...

If you are trying to show off your development work on a local network, you might very well end up 
with 500 errors.  Slim will protect you from sending passwords over http 
([Slim Security](https://github.com/tuupola/slim-basic-auth#security)).  The error you will see
in the PHP server logs is:
~~~
[2017-01-01 10:36:53] [CRITICAL]: Insecure use of middleware over HTTP denied by configuration. 
~~~

## Hosting

Permissions are a bit persnickety... Pretty much set everything to 770.  I am sure I could be more
restrictive, but this works for now - without opening up permissions totally.

## Image Processing with Cloudinary

### Bulk Thumbnail Generation
Thumbnails for profile pics can be processed using the cloud service "Cloudinary".
The process is to log in to Cloudinary, create a new folder, upload your images to the folder, 
then create a zip of the images and download them.  See the scripts in scripts/facedetect.

Cloudinary credentials: user: cyze, password: daedalus

Cloudinary is also being used for create profile thumbnails.  The process is:
- upload file to cloudinary 
- get a path to the file back
- send the path to win server
- win server downloads the jpg and puts it in static/profiles/{username}/thumbnail.jpg

To make this work, I had to allow unsigned uploads on Cloudinary -> Settings -> Upload.  The trasformation 
preset I am using is c_thumb,g_face,h_250,w_250,q:auto_good

Cloudinary does not make it easy to delete images.  The fastest way to do it is to go to the 
cloudinary media library, open a javascript console, then enter:

~~~
document.querySelectorAll(".media_item").forEach( el => el.className += " selected");
~~~

then click the checkbox for one of the images, and then hit the trashcan.

### Normal Thumbnail Generation
When an admin or user uploads a new Profile, News or Event pic, the image is uploaded to 
Cloudinary - when I get a delete_token back from Cloudinary, I first upload to the local
PHP server (and the server copies to the static site folder), and then I delete the Cloudinary
pic from the client.

## Client

The client is a vuejs app created with [vue-cli](https://github.com/vuejs/vue-cli).  I had to edit
the default configuration a bit:

~~~
	build: {
		env: require( './prod.env' ),
		index: path.resolve( __dirname, '../../site/client/index.html' ),
		assetsRoot: path.resolve( __dirname, '../../site/client/' ),
		assetsSubDirectory: '/',
		assetsPublicPath: '/',
		productionSourceMap: true,
		productionGzip: true,
		productionGzipExtensions: ['js', 'css']
	},
	dev: {
		env: require( './dev.env' ),
		port: 9090,
		assetsSubDirectory: '/',
		assetsPublicPath: '/',
		proxyTable: {
			"/api/": {
				target: "http://localhost:8080",
				changeOrigin: true
			},
		},
		cssSourceMap: false
	}
~~~

and modify the dev-server script too:

~~~
// serve pure static assets
var staticPath = path.posix.join( config.dev.assetsPublicPath, config.dev.assetsSubDirectory )
app.use( staticPath, express.static( './static' ) )

// MJC - my additions
app.use('/static', express.static('../site/static'))
app.use('/pub', express.static('../site/published'))
~~~

and finally, to make loading json and svg a bit better, add json and svg loaders to webpack.base.conf.js:

~~~
	{
		test: /\.json$/,
		loader: 'json-loader'
	},
	{
		test: /\.svg$/,
		include: /assets\/icons/,
		loader: "svg-sprite-loader"
	},

	{
        test: /\.(png|jpe?g|gif)(\?.*)?$/,
~~~
Note that you have to remove "svg" from the test.


## Updating the Client
~~~
cd client
npm update --save
~~~

## Publishing vs Reading from the DB

## Zoho Email Accounts

## MailChimp Integration

## Backups

## Hosting

## Future Stepsnode


CMS
