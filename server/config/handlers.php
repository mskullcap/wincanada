<?php
use \App\MailChimpApi;
use \App\CloudinaryApi;
use \App\Config;
use \Utils\GeoLocator;

$container = $app->getContainer();

$container["errorHandler"] = function ($container) {
	return new Slim\Handlers\ApiError($container["logger"]);
};

$container["cloudinary"] = function( $container ){
	\Cloudinary::config(array(
		"cloud_name" => Config::CLOUDINARY["cloudName"],
		"api_key" => Config::CLOUDINARY["apiKey"],
		"api_secret" => Config::CLOUDINARY["apiSecret"]
	));
	return new CloudinaryApi( $container["logger"]);
};

$container["geo"] = function( $container ){
	return new GeoLocator( $container["logger"]);
};

$container["mail"] = function( $container ){


};

$container["mailChimp"] = function($container){
	$container["logger"]->info( "preparing mailchimp cache..." );
	$mcCache = [];
	return function( $chapterName ) use ( $container, $mcCache ){
		if( !isset( $chapterName )) $chapterName = "canada";
		$chapterName = strtolower( $chapterName );
		$chapters = $container["chapters"];
		$chapterDetails = $chapters->getDetails( $chapterName );
		if( $chapterDetails === NULL ) return NULL;

		$name = $chapterDetails["name"];
		if( !isset( $mcCache[ $name ])){
			$container["logger"]->info( "creating mailchimp cache for chapter " . $name );
			$mcCache[$name] = new MailChimpApi(
				$chapterDetails,
				$container["logger"]
			);
		}
		return $mcCache[$name];
	};
};

$container["mailChimpUserManager"] = function( $container ){
	return new \App\MailChimpUserManager( $container["mailChimp"], $container["chapters"], $container["logger"]);
};
