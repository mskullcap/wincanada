<?php
use \App\Config;
use \Utils\Chapters;

$container = $app->getContainer();

$container["db"] = function ($c) {
    $file_db = new PDO("sqlite:" . $c["settings"]["db"]["path"]);
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $file_db;
};

$container["news"] = function( $c ){
	return new \App\NewsDAO( $c["db"], $c["logger"]);
};

$container["users"] = function( $c ){
	return new \App\UserDAO( $c["db"], $c["logger"]);
};

$container["profiles"] = function( $c ){
	return new \App\ProfileDAO( $c["db"], $c["logger"]);
};

$container["events"] = function( $c ){
	return new \App\EventsDAO( $c["db"], $c["logger"]);
};

$container["chapters"] = function( $c ){
	$dao = new \App\ChaptersDAO( $c["db"], $c["logger"]);
	return new Chapters( Config::CHAPTER_DETAILS, $dao );
};
