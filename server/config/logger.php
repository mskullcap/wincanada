<?php
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Formatter\LineFormatter;

date_default_timezone_set("UTC");

$container = $app->getContainer();

$container["logger"] = function ($c) {
    $logger = new Logger("slim");
    $formatter = new LineFormatter(
        "[%datetime%] [%level_name%]: %message% %context%\n",
        null,
        true,
        true
    );

    $rotating = new RotatingFileHandler( $c["settings"]["logs"]["path"], 0, Logger::DEBUG);
    $rotating->setFormatter($formatter);
    $logger->pushHandler($rotating);
    return $logger;
};
