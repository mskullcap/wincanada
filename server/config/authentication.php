<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Middleware\JwtAuthentication;
use Firebase\JWT\JWT;

$container = $app->getContainer();

$container["token"] = function ($container) {
    return new StdClass;
};

$container["JwtAuthentication"] = function ($container) {
	$jwtSettings =  $container['settings']['jwt'];
    return new JwtAuthentication([
        "path" => $jwtSettings['path'],
        "passthrough" => $jwtSettings['passthrough'],
        "secure" => false,
        "secret" =>  $jwtSettings['secret'],
        "logger" => $container['logger'],
        "error" => function ( Request $request, Response $response, $arguments) {
            $data["status"] = "error";
            $data["message"] = $arguments["message"];
            return $response
                ->withHeader("Content-Type", "application/json")
                ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        },
        "callback" => function ( Request $request, Response $response, $arguments) use ($container) {
			$container["token"] = $arguments["decoded"];
    	}
    ]);
};

$tokenRefresherMiddleware = function ( $request, $response, $next ) use ( $container ){
	// passthrough taken from https://github.com/tuupola/slim-jwt-auth/blob/2.x/src/JwtAuthentication/RequestPathRule.php
	$uri = "/" . $request->getUri()->getPath();
	$uri = preg_replace("#/+#", "/", $uri);
	foreach ((array)$container["settings"]["jwt"]["passthrough"] as $passthrough) {
		$passthrough = rtrim($passthrough, "/");
		if (!!preg_match("@^{$passthrough}(/.*)?$@", $uri)) return $next( $request, $response );
	}

	$token = $container["token"];
	$logger = $container["logger"];
	$users = $container["users"];

	if( !isset( $token )) return $next( $request, $response );

	if(( time() - $token->iat ) < $container["settings"]["jwt"]["tokenRefreshRate"] ) return $next( $request, $response );

	$username = $token->context->username;
	$user = $users->listByUsername( $username );

	if( empty( $user )){
		$logger->notice( "An attempt to refresh the token for $username failed because this user no longer exists.");
		return $response->withStatus( 401 );
	}

	if( $user["status"] !== "ACTIVATED" ){
		$logger->notice( "An attempt to refresh the token for $username failed because this user is not activated.");
		return $response->withStatus( 401 );
	}

	$roles = $users->findRolesForUser( $user["id"] );
	$r = array();
	forEach( $roles as $role ) $r[] = $role['role'];

	$t = JWT::encode(generatePayload( $user, $r ), $container["settings"]["jwt"]["secret"], "HS256" );
	$logger->debug( "JWT token automatically refreshed for {$username}.");
	$response = $next( $request, $response->withHeader( "Authorization", "Bearer $t" ));
	return $response->withHeader( "Authorization", "Bearer $t" );
};

$app
	->add( $tokenRefresherMiddleware )
	->add( $container["JwtAuthentication"] );

