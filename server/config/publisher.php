<?php

$container = $app->getContainer();

$container["publisher"] = function( $c ){
	return new \App\Publisher(
		$c->settings["publishFolder"],
		$c->settings["tmpFolder"],
		$c->settings["staticFolder"],
		$c["logger"]
	);
};
