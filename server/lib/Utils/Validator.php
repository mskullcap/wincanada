<?php
namespace Utils;

use App\Config;
use App\Roles;

function isDefined($key, $value, &$reasons ){
	if( !isset( $reasons[$key] )) $reasons[$key] = array();
	if( !isset( $value ) || strlen( $value ) == 0 ){
		array_push( $reasons[$key], "$key is not set" );
		return false;
	}
	return true;
}

function isDate( $key, $value, &$reasons ){
	if( !isset( $reasons[$key] )) $reasons[$key] = array();
	if( empty( $value )){
		array_push( $reasons[$key], "$key is not set" );
		return false;
	}
	if( !preg_match( '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $value )){
		array_push( $reasons[$key], "$key must be in the form YYYY-MM-DD." );
		return false;
	}
	return true;
}

function isAtLeastThisLong($key, $value, $length, &$reasons ){
	if( !isset( $reasons[$key] )) $reasons[$key] = array();
	if( strlen( $value ) < $length ){
		array_push( $reasons[$key], "$key must be a minimum of $length characters long." );
		return false;
	}
	return true;
}

function matchesPattern($key, $value, $pattern, $patternDescription, &$reasons ){
	if( !isset( $reasons[$key] )) $reasons[$key] = array();
	if( !preg_match( $pattern, $value )){
		array_push( $reasons[$key], "$key can only contain $patternDescription" );
		return false;
	}
	return true;
}

function isAnEmail($key, $value, &$reasons ){
	if( !isset( $reasons[$key] )) $reasons[$key] = array();
	if( !filter_var($value, FILTER_VALIDATE_EMAIL)) {
		array_push( $reasons[$key], 'Invalid email format.' );
		return false;
	}
	return true;
}

function isOneOf($key, $value, $possibilities, &$reasons ){
	if( !isset( $reasons[$key] )) $reasons[$key] = array();
	if( !in_array( $value, $possibilities )){
		$p = implode( ', ', $possibilities );
		array_push( $reasons[$key], "$key must be one of $p." );
		return false;
	}
	return true;
}

function isOf($key, $values, $possibilities, &$reasons ){
	if( !isset( $reasons[$key] )) $reasons[$key] = array();
	$result = false;
	foreach( $values as $value ){
		$result = $result || isOneOf( $key, $value, $possibilities, $reasons );
	}
	return $result;
}


class Validator {
	const PROVINCES = array( "AB", "BC", "MB", "NB", "NL", "NS", "NT", "NU", "ON", "PE", "QC", "SK", "YT" );
	const JOIN_FIELDS = array( "email", "firstName", "lastName", "salutation", "jobTitle", "employer", "city", "province", "postalCode", "background", "qualifications", "optIn");
	const POSTAL_CODE = '/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/';

	final public static function validUsername($value, &$reasons ){
		$key = "username";
		return isDefined( $key, $value, $reasons ) &&
			isAtLeastThisLong( $key, $value, 4, $reasons ) &&
			matchesPattern( $key, $value, '/^[0-9a-zA-Z _@.]*$/', " latin characters, numbers, _, @ or '.'.", $reasons );
	}

	final public static function validPassword($value, &$reasons ){
		$key = "password";
		return isDefined( $key, $value, $reasons ) && isAtLeastThisLong( $key, $value, 6, $reasons );
	}

	final public static function validEmail($value, &$reasons ){
		$key = "email";
		return isDefined( $key, $value, $reasons ) && isAnEmail( $key, $value, $reasons );
	}

	final public static function validFirstName($value, &$reasons ){
		$key = "firstName";
		return isDefined( $key, $value, $reasons ) && isAtLeastThisLong( $key, $value, 2, $reasons );
	}

	final public static function validLastName($value, &$reasons ){
		$key = "lastName";
		return isDefined( $key, $value, $reasons ) && isAtLeastThisLong( $key, $value, 2, $reasons );
	}

	final public static function validJobTitle($value, &$reasons ){
		$key = "jobTitle";
		return isDefined( $key, $value, $reasons ) && isAtLeastThisLong( $key, $value, 3, $reasons );
	}

	final public static function validEmployer($value, &$reasons ){
		$key = "employer";
		return isDefined( $key, $value, $reasons ) && isAtLeastThisLong( $key, $value, 3, $reasons );
	}

	final public static function validCity($value, &$reasons ){
		$key = "city";
		return isDefined( $key, $value, $reasons ) && isAtLeastThisLong( $key, $value, 2, $reasons );
	}

	final public static function validLocation($value, &$reasons ){
		$key = "location";
		return isDefined( $key, $value, $reasons ) && isAtLeastThisLong( $key, $value, 2, $reasons );
	}

	final public static function validProvince($value, &$reasons ){
		$key = "province";
		return isDefined( $key, $value, $reasons ) && isOneOf( $key, $value, Validator::PROVINCES, $reasons );
	}

	final public static function validRoles($roles, &$reasons ){
		$key = "roles";
		return isOf( $key, $roles, Roles::ROLES, $reasons );
	}

	final public static function validPostalCode($value, &$reasons ){
		$key = "postalCode";
		return isDefined( $key, $value, $reasons ) &&
			matchesPattern(
				$key, $value, '/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/',
				" a valid Canadian postal code.", $reasons
			);
	}

	final public static function validChapter($value, &$reasons ){
		$key = "chapter";
		return isDefined( $key, $value, $reasons ) && isOneOf( $key, $value, Config::CHAPTER_NAMES, $reasons );
	}

	final public static function validOptIn($value, &$reasons){
		$key="optIn";
		return isDefined( $key, $value, $reasons ) && isOneOf( $key, $value, [0,1], $reasons );
	}

	final public static function validPublic($value, &$reasons){
		$key="public";
		return isDefined( $key, $value, $reasons ) && isOneOf( $key, $value, [0,1], $reasons );
	}

	final public static function validWinGlobal($value, &$reasons){
		$key="winGlobal";
		return isDefined( $key, $value, $reasons ) && isOneOf( $key, $value, [0,1], $reasons );
	}

	final public static function validTitle($value, &$reasons ){
		return isDefined( "title", $value, $reasons );
	}

	final public static function validLede($value, &$reasons ){
		return isDefined( "lede", $value, $reasons );
	}

	final public static function validDescription($value, &$reasons ){
		return isDefined( "description", $value, $reasons );
	}

	final public static function validRemoveBy($value, &$reasons ){
		return self::validDate( "removeBy", $value, $reasons );
	}

	final public static function validStartDateEndDate($startDate, $endDate, &$reasons ){
		return self::validDate( "startDate", $startDate, $reasons ) &&
			self::validDate( "endDate", $endDate, $reasons );
	}

	final public static function validDate($key, $value, &$reasons ){
		return isDefined( $key, $value, $reasons ) && isDate( $key, $value, $reasons );
	}

	final public static function validContent($value, &$reasons ){
		return isDefined( "content", $value, $reasons );
	}

	final public static function validField($value, $name, &$reasons ){
		return isDefined( $name, $value, $reasons );
	}
}
