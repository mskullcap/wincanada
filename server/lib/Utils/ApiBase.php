<?php
namespace Utils;

abstract class ApiBase {
	protected $logger;
	private $headers = [];

	function __construct( $userAgent, $serviceName, $logger ){
		$this->logger = $logger;
		$this->userAgent = $userAgent;
		$this->serviceName = $serviceName;
		$this->addHeader( "Content-Type: application/json" );
	}

	function addHeader( $header ){
		array_push( $this->headers, $header );
	}

	abstract function urlFromPath( $path );

	function connect($path, $request_type, $data = [] ){
		$ch = curl_init();
		if( $request_type == "GET" ){
			$path .= "?" . http_build_query($data);
		} else {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request_type ); // according to MailChimp API: POST/GET/PATCH/PUT/DELETE
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data) );
		}
		$url = $this->urlFromPath( $path );
		curl_setopt($ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 40);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);

		$request = [
			"method" => $request_type,
			"url" => $url,
			"data" => $data
		];

		$this->logger->info( "$this->serviceName request\n" . json_encode( $request, JSON_PRETTY_PRINT ));

		$result = json_decode( curl_exec($ch), true );
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$errors = curl_error($ch);
		curl_close($ch);

		$response = [
			"request" => $request,
			"result" => $result,
			"status" => $httpCode,
			"errors" => $errors,
			"isError" => !($httpCode >= 200 && $httpCode < 300)
		];

		if(!( $httpCode >= 200 && $httpCode < 300 )){
			$this->logger->warning( "$this->serviceName response error\n" . json_encode( $response, JSON_PRETTY_PRINT ));
		} else {
			$this->logger->debug( "$this->serviceName response success\n" . json_encode( $response["request"], JSON_PRETTY_PRINT ));
		}
		return $response;
	}
}
