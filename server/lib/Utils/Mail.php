<?php
namespace Utils;

use \App\Config as Config;
use Firebase\JWT\JWT;
use DateTime;

class Mail {
	final public static function createMailer(){
		$mail = new \PHPMailer;
		$mail->isSMTP();
		$mail->SMTPOptions = array(
			"ssl" => array(
				"verify_peer" => false,
				"verify_peer_name" => false,
				"allow_self_signed" => true
			)
		);
		$mail->SMTPAuth = true;
		$mail->Host = Config::SMTP_HOST;
		$mail->Username = Config::ADMIN_EMAIL;
		$mail->Password = Config::ADMIN_EMAIL_PASSWORD;
		$mail->SMTPSecure = "tls";
		$mail->Port = 587;
		$mail->setFrom( Config::ADMIN_EMAIL, "Women in Nuclear Canada" );
		$mail->addReplyTo( Config::ADMIN_EMAIL, "Women in Nuclear Canada" );
		return $mail;
	}

	final public static function emailVerificationToken( $username, $email ){
		$now = new DateTime();
		$future = new DateTime( Config::JWT_EMAIL_TOKEN_LIFE );
		$payload = [
			"iat" => $now->getTimeStamp(),
			"exp" => $future->getTimeStamp(),
			"context" => [
				"username" => $username,
				"email" => $email
			]
		];
		return JWT::encode( $payload, Config::JWT_EMAIL_SECRET, "HS256" );
	}

	final public static function sendWelcomeEmail( $email, $username ){
		$mail = Mail::createMailer();
		$mail->addAddress( $email, $username );
		$mail->Subject = "Welcome to Women in Nuclear Canada!";

		$token = Mail::emailVerificationToken( $username, $email );

		$ip = $_SERVER["REMOTE_ADDR"];
		if( $ip === "127.0.0.1" ) $host = "http://localhost:9090";
		else $host = "https://canada.womeninnuclear.org";

		$link = "{$host}/confirm?token={$token}";

		$body = file_get_contents(  __DIR__ . "/../../templates/welcome.phtml" );
		$body = str_replace( '$link', $link, $body );
		$body = str_replace( '$username', $username, $body );

		$mail->Body = $body;
		$mail->IsHTML(true);
		if(!$mail->send()) return false;
		return true;
	}

	final public static function sendForgotPasswordEmail( $user ){
		$mail = Mail::createMailer();
		$mail->addAddress( $user["email"], $user["firstName"] . " " . $user["lastName"] );
		$mail->Subject = "Women in Nuclear Canada Password Recovery";

		$firstName = $user["firstName"];
		$token = Mail::emailVerificationToken( $user["username"], $user["email"] );

		$ip = $_SERVER["REMOTE_ADDR"];
		if( $ip === "127.0.0.1" ) $host = "http://localhost:9090";
		else $host = "https://canada.womeninnuclear.org";

		$link = "{$host}/password?token={$token}";

		$body = file_get_contents(  __DIR__ . "/../../templates/password_reset.phtml" );
		$body = str_replace( '$link', $link, $body );
		$body = str_replace( '$firstName', $firstName, $body );
		$body = str_replace( '$username', $user["username"], $body );

		$mail->Body = $body;
		$mail->IsHTML(true);
		if(!$mail->send()) return false;
		return true;
	}
}
