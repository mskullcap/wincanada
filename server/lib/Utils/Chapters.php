<?php
namespace Utils;

use \App\ChaptersDAO;

function chapterNametoPath($name ){
	$path = strtolower( $name );
	$path = str_replace( ' ', '', $path );
	$path = str_replace( '/', '', $path );
	return str_replace( 'saskatchewan', '', $path );
}

class Chapters{
	function __construct( $chapterDetails, ChaptersDAO $dao ){
		$this->dao = $dao;
		$this->chapterDetails = $chapterDetails;
		$this->nameKeys = array_keys( $chapterDetails );

		$this->lowerNameKeys = [];
		$this->pathKeys = [];
		$this->idKeys = [];
		$this->webhookKeys = [];
		foreach( $this->nameKeys as $chapterName ){
			array_push( $this->pathKeys, $chapterDetails[$chapterName]["path"]);
			array_push( $this->idKeys, $chapterDetails[$chapterName]["dbId"]);
			array_push( $this->webhookKeys, $chapterDetails[$chapterName]["mcWebHookKey"]);
			array_push( $this->lowerNameKeys, strtolower( $chapterName ));
		}
	}

	function getPaths(){
		return $this->pathKeys;
	}

	function findAll(){
		return $this->dao->findAll();
	}

	function findAllContacts(){
		return $this->dao->findAllContacts();
	}

	function guessChapter( $province, $latitude, $longitude ){
		if( $province === "NB" ) $chapterGuess = $this->chapterDetails["New Brunswick"]["name"];
		elseif( $province === "SK" ) $chapterGuess = $this->chapterDetails["WIM/WiN Saskatchewan"]["name"];
		elseif( $province === "ON" ) {
			if( isset( $longitude )){
				if( $longitude < -80.1 ) $chapterGuess = $this->chapterDetails["Bruce"]["name"];
				else if( $longitude < -79.2 && $latitude < 45 ) $chapterGuess = $this->chapterDetails["Golden Horseshoe"]["name"];
				else if( $longitude < -76 && $latitude < 45 ) $chapterGuess = $this->chapterDetails["Durham"]["name"];
				else $chapterGuess = $this->chapterDetails["Eastern Ontario"]["name"];
			}
		}
		if( isset( $chapterGuess )) return $chapterGuess;
		return "Canada";
	}

	function getDetailsFromWebhookKey( $c ){
		$index = array_search( $c, $this->webhookKeys );
		if( $index !== FALSE ) return $this->chapterDetails[$this->nameKeys[$index]];
		return NULL;
	}

	// do our best to guess the chapter based on db id, name, lower case name, path
	function getDetails( $c = NULL ){
		if( $c === NULL ) return $this->chapterDetails;

		// database chapter id
		if( is_numeric( $c )){
			$dbId = $c + 0;
			$index = array_search( $dbId, $this->idKeys );
			if( $index === FALSE ) return NULL;
			return $this->chapterDetails[$this->nameKeys[$index]];
		}

		// perfect name match
		if( isset( $this->chapterDetails[$c])) return $this->chapterDetails[$c];

		// lowercase name match
		$index = array_search( $c, $this->lowerNameKeys );
		if( $index !== FALSE ) return $this->chapterDetails[$this->nameKeys[$index]];

		// path match
		$c = chapterNametoPath( $c );
		$index = array_search( $c, $this->pathKeys );
		if( $index === FALSE ) return NULL;
		return $this->chapterDetails[$this->nameKeys[$index]];
	}
}
