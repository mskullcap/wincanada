<?php
namespace Utils;

use \App\Roles as Roles;

class Perms {
	final public static function tokenIsUser( $username, $token ){
		return $token->context->username == $username;
	}

	final public static function tokenHasRole( $role, $token ){
		return in_array( $role, $token->context->roles);
	}

	final public static function tokenIsSuper( $token ){
		return in_array( Roles::SUPERUSER, $token->context->roles);
	}

	final public static function tokenIsAdministrator( $token ){
		return in_array( Roles::ADMINISTRATOR, $token->context->roles);
	}

	final public static function tokenIsChapterLead( $token, $chapter ){
		if( !isset( $chapter )) return in_array( Roles::CHAPTERLEAD, $token->context->roles );
		return in_array( Roles::CHAPTERLEAD, $token->context->roles ) && strcasecmp( $token->context->chapter, $chapter );
	}

	final public static function tokenIsUserOrSuper( $username, $token ){
		return self::tokenIsUser( $username, $token ) || self::tokenIsSuper( $token );
	}
}

function idFromKey( $key ){

}
