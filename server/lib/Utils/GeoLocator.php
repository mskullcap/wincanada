<?php
namespace Utils;

class GeoLocator extends ApiBase {
	private $baseUrl;

	function __construct( $logger ){
		parent::__construct( "womeninnuclear.org", "freegeoip", $logger );
		$this->baseUrl = "http://freegeoip.net/json/";
	}

	function urlFromPath($path){
		return $this->baseUrl . $path;
	}

	function lookupIp( $ip ){
		if( $ip === "127.0.0.1" ) $ip = "67.220.44.218"; // "69.162.81.155";
		return $this->connect( $ip, "GET");
	}
}
