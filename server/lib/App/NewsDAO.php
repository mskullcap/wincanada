<?php

namespace App;

class NewsDAO {
	private $db;
	private $logger;

	function __construct( $db, $logger ){
		$this->db = $db;
		$this->logger = $logger;
	}

	function findSince( $since = "-1 year" ){
		$statement = $this->db->prepare("
			SELECT
			  strftime( '%Y%m%d', DATE( NEWS.CREATE_DATE)) || '_' || NEWS.ID AS id,
			  NEWS.TITLE AS title,
			  NEWS.DESCRIPTION AS description,
			  NEWS.CONTENT AS content,
			  NEWS.CREATE_DATE as createDate,
			  NEWS.CREATE_DATE as publishedOn,
			  NEWS.REMOVE_BY as removeBy
			FROM NEWS
			WHERE
			  (publishedOn IS NULL OR publishedOn > date( 'now', :since )) AND
			  removeBy > date('now')
			ORDER BY publishedOn DESC;	
		");

		$statement->bindValue(':since', $since );
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}

	function findById( $key ){
		$id = NewsDAO::getIdFromKey( $key );
		$statement = $this->db->prepare("
			SELECT
			  strftime( '%Y%m%d', DATE( NEWS.CREATE_DATE)) || '_' || NEWS.ID AS id,
			  NEWS.TITLE AS title,
			  NEWS.DESCRIPTION AS description,
			  NEWS.CONTENT AS content,
			  NEWS.CREATE_DATE as createDate,
			  NEWS.PUBLISHED_ON as publishedOn,
			  NEWS.REMOVE_BY as removeBy
			FROM NEWS
			WHERE
				id = :id
			ORDER BY ID DESC
			LIMIT 1
		");
		$statement->bindValue( ":id", $id );
		$statement->execute();
		return $statement->fetch( \PDO::FETCH_ASSOC );
	}

	public static function getIdFromKey( $key ){
		$pos = strpos( $key, "_" );
		if( $pos == false ) return $key;
		return substr( $key, $pos );
	}

	function create( $title, $description, $content ){
		$statement = $this->db->prepare("
			INSERT INTO NEWS ( 
				TITLE, 
				DESCRIPTION, 
				CONTENT )
			VALUES (
				:title,
				:description,
				:content
			)
		");
		$statement->bindValue( ":title", $title );
		$statement->bindValue( ":description", $description );
		$statement->bindValue( ":content", $content );
		$statement->execute();

		$id = $this->db->lastInsertId();
		return $this->findById( $id );
	}

	function update( $key, $title, $description, $content ){
		$id = NewsDAO::getIdFromKey( $key );
		$statement = $this->db->prepare("
			UPDATE NEWS
			SET
			  TITLE = :title,
			  DESCRIPTION = :description,
			  CONTENT = :content,
			  PUBLISHED_ON = date( 'now' )
			WHERE
				ID = :id
		");
		$statement->bindValue( ":id", $id );
		$statement->bindValue( ":title", $title );
		$statement->bindValue( ":description", $description );
		$statement->bindValue( ":content", $content );
		$statement->execute();

		return $this->findById( $id );
	}

	function remove( $key ){
		$id = NewsDAO::getIdFromKey( $key );
		$statement = $this->db->prepare( "
			DELETE
			FROM NEWS
			WHERE
			  ID = :id;
		" );
		$statement->execute([$id]);
	}
}
