<?php
namespace App;

class MailChimpApi {
	private $logger;

	function __construct( $chapterDetails, $logger ){
		$this->api = new MailChimp( $chapterDetails, $logger );
		$this->listId =  $chapterDetails["mcListId"];
		$this->logger = $logger;
	}

	function getApi(){
		return $this->api;
	}

	function lists(){
		return $this->api->connect( "lists", "GET", [
			"fields" => "lists.id,lists.name,lists.stats.member_count"
		]);
	}

	function members( $listId = NULL ){
		if( !isset( $listId )) $listId = $this->listId;
		return $this->api->connect( "lists/" . $listId . "/members", "GET", [
			"fields" => "members.email_address,members.status,members.last_changed,members.list_id,members.merge_fields",
			"offset" => 0,
			"count" => 3000
		]);
	}

	function emailHashFromUser( $user ){
		return md5( strtolower( $user["email"]));
	}

	function getMember( $user, $listId = NULL ){
		if( !isset( $listId )) $listId = $this->listId;
		$hash = $this->emailHashFromUser( $user );
		return $this->api->connect( "lists/" . $listId . "/members/" . $hash, "GET" );
	}

	public static function memberToUser( $member ){
		return [
			"username" => $member["merge_fields"]["USERNAME"],
			"chapter" => $member["merge_fields"]["CHAPTER"],
			"firstName" => $member["merge_fields"]["FNAME"],
			"lastName" => $member["merge_fields"]["LNAME"],
			"email" => $member["email_address"],
			"status" => $member["status"]
		];
	}

	function addOrUpdateMember( $user, $listId = NULL ){
		if( !isset( $listId )) $listId = $this->listId;
		$hash = $this->emailHashFromUser( $user );
		return $this->api->connect( "lists/" . $listId . "/members/" . $hash, "PUT", [
			"email_address" => $user["email"],
			"status_if_new" => $user["optIn"] == 1 ? "subscribed" : "unsubscribed",
			"status" => $user["optIn"] == 1 ? "subscribed" : "unsubscribed",
			"merge_fields" => [
				"USERNAME" => $user["username"],
				"FNAME" => $user["firstName"],
				"LNAME" => $user["lastName"],
				"EMAIL" => $user["email"],
				"CHAPTER" => $user["chapter"]
			]
		]);
	}

	function deleteMember( $user, $listId = NULL ){
		if( !isset( $listId )) $listId = $this->listId;
		$hash = $this->emailHashFromUser( $user );
		return $this->api->connect( "lists/" . $listId . "/members/" . $hash, "DELETE" );
	}

	function syncMembers( $users, $listId = NULL ){
		if( !isset( $listId )) $listId = $this->listId;
		$chapterName = $this->api->chapterName;
		$activeUsers = array_values( array_filter( $users, function( $user ) use ( $chapterName ){
			if( strcasecmp( $chapterName, "canada" ) === 0 ) return $user["optIn"] == 1;
			if( strcasecmp( $chapterName, $user["chapter"] ) === 0 ) return $user["optIn"] == 1;
			return false;
		}));

		$this->logger->info( "Syncing " . count( $activeUsers ) . " with MailChimp for chapter " . $this->api->chapterName );

		$mcUsers = array_map(
			function( $user ){
				return [
					"update_existing" => true,
					"email_address" => $user["email"],
					"status" => $user["optIn"] == "1" ? "subscribed" : "unsubscribed",
					"merge_fields" => [
						"USERNAME" => $user["username"],
						"FNAME" => $user["firstName"],
						"LNAME" => $user["lastName"],
						"EMAIL" => $user["email"],
						"CHAPTER" => $user["chapter"]
					]
				];
			},
			$activeUsers
		);

		$pages = array_chunk( $mcUsers, 500 );
		foreach( $pages as $page ){
			$members["members"] = $page;
			$members["update_existing"] = true;
			$result = $this->api->connect( "lists/" . $listId, "POST", $members );
			if( $result["errors"]) return false;
		}
		return true;
	}
}
