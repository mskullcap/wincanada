<?php

namespace App;

class EventsDAO {
	private $db;
	private $logger;

	function __construct( $db, $logger ){
		$this->db = $db;
		$this->logger = $logger;
	}

	function findById( $id ){
		$statement = $this->db->prepare( "
			SELECT
			  strftime( '%Y%m%d', DATE( EVENTS.START_DATE )) || '_' || EVENTS.ID AS id,
			  EVENTS.TITLE AS title,
			  EVENTS.DESCRIPTION AS description,
			  EVENTS.CONTENT AS content,
			  EVENTS.LOCATION AS location,
			  CHAPTERS.ID as chapterId,
			  CHAPTERS.NAME as chapter,
			  EVENTS.CREATE_DATE as createDate,
			  EVENTS.START_DATE as startDate,
			  EVENTS.END_DATE as endDate
			FROM EVENTS, CHAPTERS
			WHERE
			  EVENTS.ID = :id AND
			  CHAPTERS.ID = EVENTS.CHAPTER_ID
			ORDER BY EVENTS.ID DESC
			LIMIT 1;
		");
		$statement->bindValue( ":id", $id );
		$statement->execute();
		return $statement->fetch( \PDO::FETCH_ASSOC );
	}

	function findSince($since = "-1 year" ){
		$statement = $this->db->prepare("
			SELECT
			  strftime( '%Y%m%d', DATE( EVENTS.START_DATE )) || '_' || EVENTS.ID AS id,
			  EVENTS.TITLE AS title,
			  EVENTS.DESCRIPTION AS description,
			  EVENTS.CONTENT AS content,
			  EVENTS.LOCATION AS location,
			  CHAPTERS.ID as chapterId,
			  CHAPTERS.NAME as chapter,
			  EVENTS.CREATE_DATE as createDate,
			  EVENTS.START_DATE as startDate,
			  EVENTS.END_DATE as endDate
			FROM EVENTS, CHAPTERS
			WHERE
			  endDate > date( 'now', :since) AND
			  CHAPTERS.ID = EVENTS.CHAPTER_ID
			ORDER BY startDate DESC;
		");
		$statement->bindValue(':since', $since );
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}

	function create( $title, $chapter, $description, $content, $location, $startDate, $endDate ){
		$statement = $this->db->prepare("
			INSERT INTO EVENTS ( 
				TITLE, 
				CHAPTER_ID,
				DESCRIPTION, 
				CONTENT,
				LOCATION,
				START_DATE,
				END_DATE )
			VALUES (
				:title,
				(SELECT ID FROM CHAPTERS WHERE CHAPTERS.NAME=:chapter),
				:description,
				:content,
				:location,
				:startDate,
				:endDate
			)
		");
		$statement->bindParam( ':title', $title );
		$statement->bindParam( ':chapter', $chapter);
		$statement->bindParam( ':description', $description );
		$statement->bindParam( ':content', $content );
		$statement->bindParam( ':location', $location );
		$statement->bindParam( ':startDate', $startDate );
		$statement->bindParam( ':endDate', $endDate );
		$statement->execute();

		$id = $this->db->lastInsertId();
		return $this->findById( $id );
	}

	function update( $id, $title, $chapter, $description, $content, $location, $startDate, $endDate ){
		$statement = $this->db->prepare("
			UPDATE EVENTS
			SET
			  TITLE = :title,
			  DESCRIPTION = :description,
			  CONTENT = :content,
			  LOCATION = :location,
			  START_DATE = :startDate,
			  END_DATE = :endDate,
			  CHAPTER_ID = (SELECT ID FROM CHAPTERS WHERE CHAPTERS.NAME = :chapter)
			WHERE
				ID = :id
		");
		$statement->bindValue( ":id", $id );
		$statement->bindValue( ":title", $title );
		$statement->bindValue( ":description", $description );
		$statement->bindValue( ":chapter", $chapter );
		$statement->bindValue( ":content", $content );
		$statement->bindValue( ":location", $location );
		$statement->bindValue( ":startDate", $startDate );
		$statement->bindValue( ":endDate", $endDate );
		$statement->execute();

		return $this->findById( $id );
	}

	function deleteById( $id ){
		$statement = $this->db->prepare( "
			DELETE
			FROM EVENTS
			WHERE
			  ID = :id;
		" );
		$statement->execute([$id]);
	}
}
