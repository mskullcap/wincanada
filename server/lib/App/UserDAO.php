<?php
namespace App;
use \App\UserStatus;

class UserDAO{
	private $db;
	private $logger;

	const USER_SELECT_PUBLIC = "
		SELECT
			USERS.FIRSTNAME as firstName,
			USERS.LASTNAME as lastName,
			USERS.USERNAME as username,
			USERS.JOB_TITLE as job,
			USERS.EMPLOYER as employer,
			CHAPTERS.name as chapter
		FROM USERS, CHAPTERS
		WHERE
			USERS.CHAPTER_ID = CHAPTERS.ID AND
			USERS.STATUS_ID = 1 AND
			USERS.PUBLIC = 1
		ORDER BY
			USERS.LASTNAME
	";

	const USER_SELECT = "
		SELECT
			USERS.ID as id,
			USERS.EMAIL as email,
			USERS.USERNAME as username,
			STATUS.STATUS as status,
			USERS.CREATE_DATE as createdOn,
			USERS.UPDATE_DATE as updatedOn,
			USERS.IS_WIN_GLOBAL_MEMBER as winGlobal,
			USERS.SALUTATION as salutation,
			USERS.FIRSTNAME as firstName,
			USERS.LASTNAME as lastName,
			USERS.JOB_TITLE as jobTitle,
			USERS.EMPLOYER as employer,
			USERS.ADDRESS_1 as address1,
			USERS.ADDRESS_2 as address2,
			USERS.CITY as city,
			USERS.PROVINCE as province,
			USERS.POSTAL_CODE as postalCode,
			USERS.BACKGROUND as background,
			USERS.PROF_QUALIFICATIONS as qualifications,
			JOBS.NAME as job,
			USERS.OPT_IN as optIn,
			USERS.PUBLIC as public,
			USERS.BIO_INTRO as bioIntro,
			USERS.BIO_BACKGROUND as bioBackground,
			USERS.BIO_IMAGE as bioImage,
			USERS.BIO_FEATURED as bioFeatured,
    		GROUP_CONCAT(SITE_ROLES.ROLE) AS roles,
    		CHAPTERS.NAME as chapter
	";

	const USER_SELECT_WITH_PASSWORD = UserDAO::USER_SELECT . ", USERS.PASSWORD as password, USERS.SALT as salt";

	const USER_FROM = "
		FROM USERS, SITE_ROLES, USER_ROLES, CHAPTERS, STATUS, JOBS
	";

	const USER_WHERE = "
		WHERE
			USERS.ID = USER_ROLES.USER_ID AND
			USER_ROLES.ROLE_ID = SITE_ROLES.ID AND
			USERS.CHAPTER_ID = CHAPTERS.ID AND
			USERS.STATUS_ID = STATUS.ID AND
			JOBS.ID = USERS.JOB_ID
	";

	const USER_GROUP = "
		GROUP BY USERS.ID
		ORDER BY USERS.CREATE_DATE DESC
	";

	function __construct($db, $logger){
		$this->db = $db;
		$this->logger = $logger;
	}

	function listAll( $withPassword = false, $activatedOnly = false ){
		if( $withPassword === false ) $select = UserDAO::USER_SELECT . UserDAO::USER_FROM;
		else $select = UserDAO::USER_SELECT_WITH_PASSWORD . UserDAO::USER_FROM;

		$where =
			( $activatedOnly === true ?
				" AND STATUS.STATUS = 'ACTIVATED' AND USERS.OPT_IN = 1" : ""
			);

		$query = $select . UserDAO::USER_WHERE . $where . UserDAO::USER_GROUP;
		$statement = $this->db->prepare( $query );
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}

	function makePresentable( $user ){
		return [
			"username" => $user["username"],
			"firstName" => $user["firstName"],
			"lastName" => $user["lastName"],
			"chapter" => $user["chapter"],
			"salutation" => $user["salutation"],
			"jobTitle" => $user["jobTitle"],
			"employer" => $user["employer"],
			"background" => $user["background"],
			"qualifications" => $user["qualifications"],
			"job" => $user["job"],
			"bioIntro" => $user["bioIntro"],
			"bioBackground" => $user["bioBackground"],
			"bioImage" => $user["bioImage"]
		];
	}

	function listAllPublic(){
		$statement = $this->db->prepare( UserDAO::USER_SELECT_PUBLIC );
		$statement->execute();
		$short = $statement->fetchAll( \PDO::FETCH_ASSOC );
		$result = [];
		forEach( $short as $s ){
			$row = array( $s["firstName"], $s["lastName"], $s["username"], $s["job"], $s["employer"], $s["chapter"] );
			$result[] = $row;
		}
		return $result;
	}

	function listByUsernameOrEmail( $value, $withPassword = false ){
		if( $withPassword === false ) $select = UserDAO::USER_SELECT . UserDAO::USER_FROM;
		else $select = UserDAO::USER_SELECT_WITH_PASSWORD . UserDAO::USER_FROM;

		$query = $select .
			UserDAO::USER_WHERE .
			"AND ( USERS.EMAIL = :email OR USERS.USERNAME = :username )" .
			UserDAO::USER_GROUP;

		$statement = $this->db->prepare( $query );
		$statement->bindValue( ":email", strtolower( $value ));
		$statement->bindValue( ":username", strtolower( $value ));
		$statement->execute();
		return $statement->fetch( \PDO::FETCH_ASSOC );
	}

	function listByUsername( $value, $withPassword = false ){
		if( $withPassword === false ) $select = UserDAO::USER_SELECT . UserDAO::USER_FROM;
		else $select = UserDAO::USER_SELECT_WITH_PASSWORD . UserDAO::USER_FROM;

		$query = $select .
			UserDAO::USER_WHERE .
			"AND USERS.USERNAME = :username" .
			UserDAO::USER_GROUP;

		$statement = $this->db->prepare( $query );
		$statement->bindValue( ":username", strtolower( $value ));
		$statement->execute();
		return $statement->fetch( \PDO::FETCH_ASSOC );
	}

	function removeUserById( $userId ){
		$statement = $this->db->prepare( "
			DELETE FROM USERS
			WHERE ID = :id
		" );
		$statement->bindParam( ":id", $userId );
		$statement->execute();
		return true;
	}

	function userExistsByUsername( $username ){
		$statement = $this->db->prepare( "
			SELECT count(*)
			FROM users
			WHERE users.USERNAME = :username
		" );
		$statement->bindParam( ':username', strtolower( $username ));
		$statement->execute();
		$count = $statement->fetchColumn();
		return $count > 0;
	}

	function userExistsByEmail( $email ){
		$statement = $this->db->prepare( "
			SELECT count(*)
			FROM users
			WHERE users.email = :email
		" );
		$statement->bindParam( ':email', strtolower( $email ));
		$statement->execute();
		$count = $statement->fetchColumn();
		return $count > 0;
	}

	function listAllByChapter( $chapterName, $activatedOnly = true ){ // assume activatedOnly includes optIn only
		$select = UserDAO::USER_SELECT . UserDAO::USER_FROM . UserDAO::USER_WHERE;
		$where = "AND CHAPTERS.NAME = :chapter" .
			( $activatedOnly === true ?
				" AND STATUS.STATUS = 'ACTIVATED' AND USER.OPT_IN = 1" : ""
			);

		$query = $select . $where . UserDAO::USER_GROUP;

		$statement = $this->db->prepare( $query );
		$statement->bindValue( ":chapter", $chapterName );
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}

	function updateUser( $user ){
		if( empty( $user["job"] )) $user["job"] = "Other";

		if( $this->assignRolesToUser( $user["id"], explode( ",", $user["roles"])) === false ){
			$this->logger->warn("Update failed: roles not valid for {$user['username']}");
			return false;
		}

		$query = "
			UPDATE USERS
			SET
				EMAIL = :email,
				STATUS_ID = (SELECT ID FROM STATUS WHERE STATUS.STATUS = :status),
				IS_WIN_GLOBAL_MEMBER = :winGlobal,
				SALUTATION = :salutation,
				FIRSTNAME = :firstName,
				LASTNAME = :lastName,
				JOB_TITLE = :jobTitle,
				JOB_ID = (SELECT ID FROM JOBS WHERE JOBS.NAME = :job),
				EMPLOYER = :employer,
				ADDRESS_1 = :address1,
				ADDRESS_2 = :address2,
				CITY = :city,
				PROVINCE = :province,
				POSTAL_CODE = :postalCode,
				BACKGROUND = :background,
				PROF_QUALIFICATIONS = :qualifications,
				OPT_IN = :optIn,
				PUBLIC = :pub,
				BIO_INTRO = :bioIntro,
				BIO_BACKGROUND = :bioBackground,
				BIO_IMAGE = :bioImage,
				BIO_FEATURED = :bioFeatured,
				CHAPTER_ID = (SELECT ID FROM CHAPTERS WHERE CHAPTERS.NAME = :chapter)
			WHERE
				ID = :userId
		";

		$statement = $this->db->prepare( $query );

		$statement->bindValue( ":email", $user["email"]);
		$statement->bindValue( ":status", $user["status"]  );
		$statement->bindValue( ":winGlobal", $user["winGlobal"]);
		$statement->bindValue( ":salutation", $user["salutation"]);
		$statement->bindValue( ":firstName", $user["firstName"]);
		$statement->bindValue( ":lastName", $user["lastName"]);
		$statement->bindValue( ":jobTitle", $user["jobTitle"]);
		$statement->bindValue( ":job", $user["job"] );
		$statement->bindValue( ":employer", $user["employer"]);
		$statement->bindValue( ":address1", $user["address1"]);
		$statement->bindValue( ":address2", $user["address2"]);
		$statement->bindValue( ":city", $user["city"]);
		$statement->bindValue( ":province", $user["province"]);
		$statement->bindValue( ":postalCode", $user["postalCode"]);
		$statement->bindValue( ":background", $user["background"]);
		$statement->bindValue( ":qualifications", $user["qualifications"]);
		$statement->bindValue( ":optIn", $user["optIn"]);
		$statement->bindValue( ":pub", $user["public"]);
		$statement->bindValue( ":bioIntro", $user["bioIntro"]);
		$statement->bindValue( ":bioBackground", $user["bioBackground"]);
		$statement->bindValue( ":bioImage", $user["bioImage"]);
		$statement->bindValue( ":bioFeatured", $user["bioFeatured"]);
		$statement->bindValue( ":chapter", $user["chapter"] );
		$statement->bindValue( ":userId", $user["id"]);

		return $statement->execute(); // true or false
	}

	function createUser( $email, $username, $password, $chapter, $winGlobal, $salutation,
		$firstName, $lastName, $jobTitle, $employer, $city, $province, $postalCode, $background,
		$qualifications, $optIn ){

		$hash = password_hash( $password , PASSWORD_DEFAULT );

		$statement = $this->db->prepare( "
			INSERT INTO USERS (
			  EMAIL,
			  USERNAME,
			  PASSWORD,
			  SALT,
			  CHAPTER_ID,
			  IS_WIN_GLOBAL_MEMBER,
			  SALUTATION,
			  FIRSTNAME,
			  LASTNAME,
			  JOB_TITLE,
			  EMPLOYER,
			  CITY,
			  PROVINCE,
			  POSTAL_CODE,
			  BACKGROUND,
			  PROF_QUALIFICATIONS,
		   	  OPT_IN,
		   	  STATUS_ID )
			VALUES (
			  :email,
			  :username,
			  :password,
			  '',
			  :chapterId,
			  :winGlobal,
			  :salutation,
			  :firstName,
			  :lastName,
			  :jobTitle,
			  :employer,
			  :city,
			  :province,
			  :postalCode,
			  :background,
			  :qualifications,
			  :optIn,
			  :statusId
			)
		");

		$c = Config::CHAPTER_DETAILS[$chapter];
		$cId = $c["dbId"];
		$statement->bindParam( ':email', $email );
		$statement->bindParam( ':username', $username );
		$statement->bindParam( ':password', $hash );
		$statement->bindParam( ':chapterId', $cId );
		$statement->bindParam( ':winGlobal', $winGlobal );
		$statement->bindParam( ':salutation', $salutation );
		$statement->bindParam( ':firstName', $firstName );
		$statement->bindParam( ':lastName', $lastName );
		$statement->bindParam( ':jobTitle', $jobTitle );
		$statement->bindParam( ':employer', $employer );
		$statement->bindParam( ':city', $city );
		$statement->bindParam( ':province', $province );
		$statement->bindParam( ':postalCode', $postalCode );
		$statement->bindParam( ':background', $background );
		$statement->bindParam( ':qualifications', $qualifications );
		$statement->bindParam( ':optIn', $optIn );
		$state = UserStatus::ACTIVATED;
		$statement->bindParam( ':statusId', $state );

		if( $statement->execute() ){
			$userId = $this->db->lastInsertId();

			$this->assignRolesToUser( $userId, [Roles::STANDARD] );
			return $userId;
		}
		return null;
	}

	function updatePasswordForUsername( $username, $password ){
		$hash = password_hash( $password, PASSWORD_DEFAULT );
		$statement = $this->db->prepare( "
			UPDATE USERS
			SET
				PASSWORD = :hash,
				SALT = ''
			WHERE
				USERNAME = :username;
		" );
		$statement->bindParam( ':hash', $hash );
		$statement->bindParam( ':username', $username );
		return $statement->execute();
	}




	//-----------------------
	function findAllAdmin(){
		$statement = $this->db->prepare( "
			SELECT
			  USERS.CREATE_DATE             AS joined,
			  USERS.UPDATE_DATE             AS updated,
			  USERS.ID                      AS id,
			  USERS.EMAIL                   AS email,
			  USERS.USERNAME                AS username,
			  CHAPTERS.NAME                 AS chapter,
			  STATUS.STATUS                 AS status,
			  GROUP_CONCAT(SITE_ROLES.ROLE) AS roles,
			  USERS.FIRSTNAME               AS firstName,
			  USERS.LASTNAME                AS lastName,
			  USERS.OPT_IN                  AS optIn,
			  USERS.PUBLISH                 AS publicProfile,
			  PROFILES.CREATE_DATE          AS profileCreateDate,
			  PROFILES.UPDATE_DATE          AS profileUpdateDate,
			  PROFILES.FEATURED             AS profileFeatured
			FROM USERS, SITE_ROLES, USER_ROLES, CHAPTERS, STATUS
			LEFT JOIN PROFILES ON USERS.ID = PROFILES.USER_ID
			WHERE
			  USERS.ID = USER_ROLES.USER_ID AND
			  ROLE_ID = SITE_ROLES.ID AND
			  CHAPTERS.ID = USERS.CHAPTER_ID AND
			  USERS.STATUS_ID = STATUS.ID
			GROUP BY USERS.ID
			ORDER BY joined
			  DESC;
			");
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}

	function findAllByChapterAdmin( $chapter ){
		$statement = $this->db->prepare( "
			SELECT
			  USERS.CREATE_DATE             AS joined,
			  USERS.UPDATE_DATE             AS updated,
			  USERS.ID                      AS id,
			  USERS.EMAIL                   AS email,
			  USERS.USERNAME                AS username,
			  CHAPTERS.NAME                 AS chapter,
			  STATUS.STATUS                 AS status,
			  GROUP_CONCAT(SITE_ROLES.ROLE) AS roles,
			  USERS.FIRSTNAME               AS firstName,
			  USERS.LASTNAME                AS lastName,
			  USERS.OPT_IN                  AS optIn,
			  USERS.PUBLISH                 AS publicProfile,
			  PROFILES.CREATE_DATE          AS profileCreateDate,
			  PROFILES.UPDATE_DATE          AS profileUpdateDate,
			  PROFILES.FEATURED             AS profileFeatured
			FROM USERS, SITE_ROLES, USER_ROLES, CHAPTERS, STATUS
			LEFT JOIN PROFILES ON USERS.ID = PROFILES.USER_ID
			WHERE
			  USERS.ID = USER_ROLES.USER_ID AND
			  ROLE_ID = SITE_ROLES.ID AND
			  CHAPTERS.ID = USERS.CHAPTER_ID AND
			  USERS.STATUS_ID = STATUS.ID AND
			  CHAPTERS.NAME = :chapterName
			GROUP BY USERS.ID
			ORDER BY joined
			  DESC;
			");
		$statement->bindValue( ":chapterName", $chapter );
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}

	function findAllByChapter( $chapterName ){
		$statement = $this->db->prepare( "
			SELECT
			  USERS.CREATE_DATE             AS joined,
			  USERS.ID                      AS id,
			  USERS.EMAIL                   AS email,
			  USERS.USERNAME                AS username,
			  CHAPTERS.NAME                 AS chapter,
			  STATUS.STATUS                 AS status,
			  GROUP_CONCAT(SITE_ROLES.ROLE) AS roles,
			  USERS.IS_WIN_GLOBAL_MEMBER    AS winGlobal,
			  USERS.SALUTATION              AS salutation,
			  USERS.FIRSTNAME               AS firstName,
			  USERS.LASTNAME                AS lastName,
			  USERS.JOB_TITLE               AS jobTitle,
			  USERS.EMPLOYER                AS employer,
			  USERS.ADDRESS_1               AS address1,
			  USERS.ADDRESS_2               AS address2,
			  USERS.CITY                    AS city,
			  USERS.PROVINCE                AS province,
			  USERS.POSTAL_CODE             AS postalCode,
			  USERS.BACKGROUND              AS background,
			  USERS.PROF_QUALIFICATIONS     AS profQualifications,
			  USERS.OPT_IN                  AS optIn,
			  USERS.PUBLISH                 AS publish
			FROM USERS, SITE_ROLES, USER_ROLES, CHAPTERS, STATUS
			WHERE
			  USERS.ID = USER_ROLES.USER_ID AND
			  ROLE_ID = SITE_ROLES.ID AND
			  CHAPTERS.ID = USERS.CHAPTER_ID AND
			  USERS.STATUS_ID = STATUS.ID AND
			  CHAPTERS.NAME = :chapterName
			GROUP BY USERS.ID
			ORDER BY joined
			  DESC;
		");
		$statement->bindValue( ":chapterName", $chapterName );
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}

	function findAll(){
		$statement = $this->db->prepare( "
			SELECT
			  USERS.CREATE_DATE             AS joined,
			  USERS.ID                      AS id,
			  USERS.EMAIL                   AS email,
			  USERS.USERNAME                AS username,
			  CHAPTERS.NAME                 AS chapter,
			  STATUS.STATUS                 AS status,
			  GROUP_CONCAT(SITE_ROLES.ROLE) AS roles,
			  USERS.IS_WIN_GLOBAL_MEMBER    AS winGlobal,
			  USERS.SALUTATION              AS salutation,
			  USERS.FIRSTNAME               AS firstName,
			  USERS.LASTNAME                AS lastName,
			  USERS.JOB_TITLE               AS jobTitle,
			  USERS.EMPLOYER                AS employer,
			  USERS.ADDRESS_1               AS address1,
			  USERS.ADDRESS_2               AS address2,
			  USERS.CITY                    AS city,
			  USERS.PROVINCE                AS province,
			  USERS.POSTAL_CODE             AS postalCode,
			  USERS.BACKGROUND              AS background,
			  USERS.PROF_QUALIFICATIONS     AS profQualifications,
			  USERS.OPT_IN                  AS optIn,
			  USERS.PUBLISH                 AS publish
			FROM USERS, SITE_ROLES, USER_ROLES, CHAPTERS, STATUS
			WHERE
			  USERS.ID = USER_ROLES.USER_ID AND
			  ROLE_ID = SITE_ROLES.ID AND
			  CHAPTERS.ID = USERS.CHAPTER_ID AND
			  USERS.STATUS_ID = STATUS.ID
			GROUP BY USERS.ID
			ORDER BY joined
			  DESC;
			");
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}

	function findByUsername( $username, $includePassword = false ){
		if( !$includePassword ){
			$statement = $this->db->prepare( "
				SELECT
				  USERS.CREATE_DATE             AS joined,
				  USERS.ID                      AS id,
				  USERS.EMAIL                   AS email,
				  USERS.USERNAME                AS username,
				  CHAPTERS.NAME                 AS chapter,
				  STATUS.STATUS                 AS status,
				  GROUP_CONCAT(SITE_ROLES.ROLE) AS roles,
				  USERS.IS_WIN_GLOBAL_MEMBER    AS winGlobal,
				  USERS.SALUTATION              AS salutation,
				  USERS.FIRSTNAME               AS firstName,
				  USERS.LASTNAME                AS lastName,
				  USERS.JOB_TITLE               AS jobTitle,
				  USERS.EMPLOYER                AS employer,
				  USERS.ADDRESS_1               AS address1,
				  USERS.ADDRESS_2               AS address2,
				  USERS.CITY                    AS city,
				  USERS.PROVINCE                AS province,
				  USERS.POSTAL_CODE             AS postalCode,
				  USERS.BACKGROUND              AS background,
				  USERS.PROF_QUALIFICATIONS     AS profQualifications,
				  USERS.OPT_IN                  AS optIn,
				  USERS.PUBLISH                 AS publish
				FROM USERS, SITE_ROLES, USER_ROLES, CHAPTERS, STATUS
				WHERE
				  USERS.ID = USER_ROLES.USER_ID AND
				  ROLE_ID = SITE_ROLES.ID AND
				  CHAPTERS.ID = USERS.CHAPTER_ID AND
				  USERS.STATUS_ID = STATUS.ID AND
				  USERS.USERNAME = :username;
			" );
		} else {
			$statement = $this->db->prepare( "
				SELECT
				  USERS.ID       AS id,
				  USERS.EMAIL    AS email,
				  USERS.USERNAME AS username,
				  CHAPTERS.NAME  AS chapter,
				  STATUS.STATUS  AS status,
				  USERS.PASSWORD AS password,
				  USERS.SALT     AS salt,
				  GROUP_CONCAT(SITE_ROLES.ROLE) AS roles
				FROM USERS, SITE_ROLES, USER_ROLES, CHAPTERS, STATUS
				WHERE
				  USERS.ID = USER_ROLES.USER_ID AND
				  ROLE_ID = SITE_ROLES.ID AND
				  CHAPTERS.ID = USERS.CHAPTER_ID AND
				  USERS.STATUS_ID = STATUS.ID AND
				  USERS.USERNAME = :username;
			" );
		}
		$statement->execute([$username]);
		return $statement->fetchObject();
	}

	function findByUsernameOrEmail( $username ){
		$statement = $this->db->prepare( "
			SELECT
			  USERS.ID       AS id,
			  USERS.EMAIL    AS email,
			  USERS.USERNAME AS username,
			  CHAPTERS.NAME  AS chapter,
			  STATUS.STATUS  AS status,
			  USERS.PASSWORD AS password,
			  USERS.SALT     AS salt,
			  GROUP_CONCAT(SITE_ROLES.ROLE) AS roles
			FROM USERS, SITE_ROLES, USER_ROLES, CHAPTERS, STATUS
			WHERE
			  USERS.ID = USER_ROLES.USER_ID AND
			  ROLE_ID = SITE_ROLES.ID AND
			  CHAPTERS.ID = USERS.CHAPTER_ID AND
			  USERS.STATUS_ID = STATUS.ID AND
			  ( USERS.USERNAME = :username OR USERS.EMAIL = :email )
		");
		$statement->bindValue( ":username", $username );
		$statement->bindValue( ":email", strtolower( $username ));
		$statement->execute();
		return $statement->fetchObject();
	}

	function findRolesForUser( $id ){
		$statement = $this->db->prepare( "
			SELECT ROLE AS role
			FROM USER_ROLES, SITE_ROLES
			WHERE
			  USER_ID = :id AND
			  ROLE_ID = SITE_ROLES.ID;
		");
		$statement->bindValue( ':id', $id );
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}

	function assignRolesToUser( $userId, $roleNames ){
		foreach( $roleNames as &$roleName ) $roleName= $this->db->quote( $roleName );
		$listOfRoles = implode( ",", $roleNames );

		$this->db->beginTransaction();
		$statement = $this->db->prepare( "
			DELETE
			FROM USER_ROLES
			WHERE USER_ID = :userId
		" );
		$statement->bindValue( ":userId", $userId );
		$result = $statement->execute();

		$statement = $this->db->prepare( "
			INSERT INTO USER_ROLES ( USER_ID, ROLE_ID )
			SELECT :userId, ID as id FROM SITE_ROLES WHERE ROLE IN ({$listOfRoles})
		" );

		$statement->bindValue( ":userId", $userId );
		$result = $statement->execute();
		$this->db->commit();
		return $result;
	}

	function updatePasswordForUser($username, $password ){
		$hash = password_hash( $password, PASSWORD_DEFAULT );
		$statement = $this->db->prepare( "
			UPDATE USERS
			SET
				PASSWORD = :hash,
				SALT = ''
			WHERE
				USERNAME = :username;
		" );
		$statement->bindValue( ':hash', $hash );
		$statement->bindValue( ':username', $username );
		return $statement->execute();
	}

	function setUserStatus( $username, $status ){
		$statement = $this->db->prepare( "
			UPDATE USERS
			SET
			  STATUS_ID = :status
			WHERE
			  USERNAME = :username
		" );
		$statement->bindValue(':status', $status );
		$statement->bindParam(':username', $username);
		$statement->execute();
	}

	function unsubscribe( $username ){
		$statement = $this->db->prepare( "
			UPDATE USERS
			SET
			  OPT_IN = 0
			WHERE
			  USERNAME = :username
		" );
		$statement->bindParam(':username', $username);
		$statement->execute();
	}

	function subscribe( $username ){
		$statement = $this->db->prepare( "
			UPDATE USERS
			SET
			  OPT_IN = 1
			WHERE
			  USERNAME = :username
		" );
		$statement->bindParam(':username', $username);
		$statement->execute();
	}

	function userExists( $username, $email ){
		if( $email ){
			$statement = $this->db->prepare( "
				SELECT count(*)
				FROM users
				WHERE
				  users.email = :email OR
				  users.USERNAME = :username;
			" );
			$statement->bindParam( ':email', $email );
		} else {
			$statement = $this->db->prepare( "
				SELECT count(*)
				FROM users
				WHERE
				  users.USERNAME = :username;
			" );
		}
		$statement->bindParam( ':username', $username );
		$statement->execute();
		$count = $statement->fetchColumn();
		return $count > 0;
	}

	function deleteUser( $username ){
		$statement = $this->db->prepare( "
			SELECT
			  ID AS id
			FROM USERS
			WHERE
			  USERS.USERNAME = :username
			LIMIT 1
		" );
		$statement->bindParam( ":username", $username );
		$statement->execute();

		$user = $statement->fetch( \PDO::FETCH_ASSOC );
		if( empty( $user )) return false;

		$statement = $this->db->prepare( "
			DELETE
			FROM USERS
			WHERE
			  ID = :id;
		" );
		$statement->bindParam( ":id", $user["id"]);
		$statement->execute();

		$statement = $this->db->prepare( "
			DELETE
			FROM PROFILES
			WHERE
			  USER_ID = :id;
		" );
		$statement->bindParam( ":id", $user["id"]);
		$statement->execute();
		return true;
	}
}
