<?php

namespace App;

class ChaptersDAO {
	private $db;
	private $logger;

	function __construct( $db, $logger ){
		$this->db = $db;
		$this->logger = $logger;
	}

	// can be an id, path segment, or name
	function getDetails( $c ){
		return $this->chapterDetails->getDetails( $c );
	}

	function findAll(){
		$statement = $this->db->prepare("
			SELECT
			  CHAPTERS.NAME AS name,
			  CHAPTERS.DESCRIPTION AS description,
			  CHAPTERS.ADDRESS AS address,
			  CHAPTERS.EMAIL AS email
			FROM CHAPTERS;
		");
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}

	function findAllContacts(){
		$statement = $this->db->prepare("
			SELECT
			  ('[' || USERS.FIRSTNAME || ' ' || USERS.LASTNAME || '](mailto:' || CHAPTERS.EMAIL || ')') AS name,
			  SITE_ROLES.ROLE AS role,
			  CHAPTERS.NAME AS chapter,
			  CHAPTERS.EMAIL AS email,
			  USERS.EMPLOYER AS employer
			FROM CHAPTERS, USERS, USER_ROLES, SITE_ROLES
			WHERE
			  CHAPTERS.ID = USERS.CHAPTER_ID AND
			  USER_ROLES.USER_ID = USERS.ID AND
			  SITE_ROLES.ID = USER_ROLES.ROLE_ID AND
			  USER_ROLES.ROLE_ID > 0 AND
			  USER_ROLES.ROLE_ID <= 100
			ORDER BY
			  CHAPTERS.ID;
		");
		$statement->execute();
		return $statement->fetchAll( \PDO::FETCH_ASSOC );
	}
}
