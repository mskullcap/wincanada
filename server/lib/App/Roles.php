<?php
namespace App;

abstract class Roles {
	const ROLES = [
		Roles::STANDARD,
		Roles::SUPERUSER,
		Roles::ADMINISTRATOR,
		Roles::CHAPTERLEAD,
		Roles::MEMBERSATLARGE,
		Roles::PASTPRESIDENT,
		Roles::PRESIDENT,
		Roles::SECRETARY,
		Roles::TREASURER
	];
	const STANDARD = "Standard";
	const SUPERUSER = "Super User";
	const ADMINISTRATOR = "Administrator";
	const CHAPTERLEAD = "Chapter Lead";
	const MEMBERSATLARGE = "Members At Large";
	const PASTPRESIDENT = "Past President";
	const PRESIDENT = "President";
	const SECRETARY = "Secretary";
	const TREASURER = "Treasurer";
}
