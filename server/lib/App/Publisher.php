<?php
namespace App;

class Publisher{
	private $logger;
	private $folder;
	private $staticFolder;
	private $tmpFolder;

	function __construct( $folder, $tmpFolder, $staticFolder, $logger ){
		$this->logger = $logger;
		$this->folder = $folder;
		$this->tmpFolder = $tmpFolder;
		$this->staticFolder = $staticFolder;
	}

	private static function removeFolder( $folder ) {
		$files = array_diff( scandir( $folder ), array( '.' , '..' ));
		foreach( $files as $file) (is_dir( "$folder/$file" )) ? removeFolder( "$folder/$file" ) : unlink( "$folder/$file" );
		return rmdir($folder);
	}

	private function folderByKey( $type, $key ){
		return $this->folder . "${type}/${key}/";
	}

	private function staticFolderByKey( $type, $key ){
		return $this->staticFolder . "${type}/${key}/";
	}

	private function publishFile( $file, $json ){
		$this->logger->warning( "publishing to $file");
		$fp = fopen( $file, "w+");
		fwrite( $fp, json_encode( $json, JSON_PRETTY_PRINT ));
		fclose( $fp );
	}

	function publishChapters( $data ){
		$this->publishFile( $this->folder . "chapters.json", $data );
	}

	function publishContacts( $data ){
		$this->publishFile( $this->folder . "contacts.json", $data );
	}

	// -------------------------------------------------------------------------------------------
	// Conference
	// -------------------------------------------------------------------------------------------
	private function conferenceStaticFolder( ){
		return $this->staticFolderByKey( "content", "conference" );
	}

	function loadConference(){
		$destinationFolder = $this->conferenceStaticFolder();
		return json_decode( file_get_contents( $destinationFolder . "/content.json" ));
	}

	function publishConference( $conference ){
		$destinationFolder = $this->conferenceStaticFolder();
		if( is_dir($destinationFolder) === false ) mkdir($destinationFolder, 0777, true);
		$this->publishFile( $destinationFolder . "content.json", $conference );
	}

	function publishConferenceBackground( $path ){
		$destinationFolder = $this->conferenceStaticFolder();
		if( is_dir($destinationFolder) === false ) mkdir($destinationFolder, 0777, true);
		$destination = $destinationFolder . '/' . "background.jpg" ;
		$this->logger->warning('copying conference background from ' . $path . " to " . $destination );
		copy( $path, $destination );
		return array( 'path' => $destination );
	}

	function publishConferenceSponsors( $path ){
		$destinationFolder = $this->conferenceStaticFolder();
		if( is_dir($destinationFolder) === false ) mkdir($destinationFolder, 0777, true);
		$destination = $destinationFolder . '/' . "sponsors.png" ;
		$this->logger->warning('copying conference sponsors from ' . $path . " to " . $destination );
		copy( $path, $destination );
		return array( 'path' => $destination );
	}

	// -------------------------------------------------------------------------------------------
	// Events
	// -------------------------------------------------------------------------------------------
	private function eventsStaticFolderByKey( $key ){
		return $this->staticFolderByKey( "events", $key );
	}

	function publishEvents( $events ){
		$this->publishFile( $this->folder . "events.json", $events );
	}

	function publishEventPhoto( $event, $path ){
		$destinationFolder = $this->eventsStaticFolderByKey( $event->id );
		if( is_dir($destinationFolder) === false ) mkdir($destinationFolder, 0777, true);
		$destination = $destinationFolder . '/' . "thumbnail.jpg" ;
		$this->logger->warning( "copying thumbnail from $path to $destination" );
		copy( $path, $destination );
		return [
			"path" => "/static/events/$event->id/thumbnail.jpg"
		];
	}

	// -------------------------------------------------------------------------------------------
	// News
	// -------------------------------------------------------------------------------------------
	private function newsStaticFolderByKey( $key ){
		return $this->staticFolderByKey( "news", $key );
	}

	function publishNews( $news ){
		$this->publishFile( $this->folder . "news.json", $news );
	}

	function publishNewsPhoto( $id, $path ){
		$destinationFolder = $this->newsStaticFolderByKey( $id );
		if( is_dir($destinationFolder) === false ) mkdir($destinationFolder, 0777, true);
		$destination = $destinationFolder . '/' . "thumbnail.jpg" ;
		$this->logger->warning('copying thumbnail from ' . $path . " to " . $destination );
		copy( $path, $destination );
		return array( 'path' => '/static/news/' . $id . '/' . 'thumbnail.jpg' );
	}

	// -------------------------------------------------------------------------------------------
	// Profiles
	// -------------------------------------------------------------------------------------------
	private function profileStaticFolderByUsername( $username ){
		return $this->staticFolderByKey( "profiles", strtolower( trim( $username )));
	}

	private function profileFolderByUsername( $username ){
		return $this->folderByKey( "profiles", strtolower( trim( $username )));
	}

	function keyFromProfile($profile ){
		return $profile["username"];
	}

	function removeProfileByUsername( $username ){
		$destinationFolder = $this->profileFolderByUsername( $username );
		if( is_dir($destinationFolder) === false ) return;
		$this->logger->warning("removing $destinationFolder" );
		self::removeFolder( $destinationFolder );

		$destinationFolder = $this->profileStaticFolderByUsername( $username );
		if( is_dir($destinationFolder) === false ) return;
		$this->logger->warning("removing $destinationFolder" );
		self::removeFolder( $destinationFolder );
	}

	function publishProfilePhoto( $username, $path ){
		$destinationFolder = $this->profileStaticFolderByUsername( $username );
		if( is_dir($destinationFolder) === false ) mkdir($destinationFolder, 0777, true);
		$destination = "{$destinationFolder}thumbnail.jpg";
		$this->logger->warning( "copying thumbnail from {$path} to {$destination}" );
		copy( $path, $destination );
		return [
			"path" => "/static/profiles/{$username}/thumbnail.jpg"
		];
	}

	function publishProfile( $profile ){
		$destinationFolder = $this->profileFolderByUsername( $this->keyFromProfile( $profile ));
		$this->logger->warning("publishing to $destinationFolder" );

		if( is_dir($destinationFolder) === false ) mkdir($destinationFolder, 0777, true);
		$fp = fopen($destinationFolder . "profile.json", "w+");
		fwrite($fp, json_encode($profile, JSON_PRETTY_PRINT));
		fclose($fp);
	}

	function listProfilesByUsername() {
		$filenames = array_diff( scandir($this->folder . "profiles/" ), array( "..", "."));
		return array_filter( $filenames, function( $filename ) {
			return (is_dir( $this->profileFolderByUsername( $filename ))) ? true : false;
		});
	}

	function publishProfiles( $profiles ){
		$existingKeys = $this->listProfilesByUsername();
		$keys = array_map( function( $p ){ return $this->keyFromProfile( $p ); }, $profiles );
		$toBeRemoved = array_diff( $existingKeys, $keys );

		forEach( $toBeRemoved as $username ) $this->removeProfileByUsername( $username );
		forEach( $profiles as $profile ) $this->publishProfile( $profile );
	}
}
