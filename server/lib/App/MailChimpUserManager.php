<?php
namespace App;

class MailChimpUserManager{
	private $logger;
	private $mailChimp;
	private $canadaMailChimp;
	private $chapters;

	function __construct($mailchimp, $chapters, $logger){
		$this->canadaMailChimp = $mailchimp( "canada" );
		$this->mailChimp = $mailchimp;
		$this->chapters = $chapters;
		$this->logger = $logger;
	}

	function changeChapters($user, $currentChapter, $newChapter ){
		$this->logger->info( "currentChapter $currentChapter newChapter $newChapter");
		$mailChimp = $this->mailChimp; // stupid php, have to do this for closures

		$canadaMailChimp = $this->canadaMailChimp;
		$currentMailChimp = $mailChimp( $currentChapter );
		$newMailChimp = $mailChimp( $newChapter );

		$this->logger->info( "updating user {$user['username']} in the Canada chapter..." );
		$updatedUser = $user;
		$updatedUser["chapter"] = $newChapter;
		$result = $canadaMailChimp->addOrUpdateMember( $updatedUser );

		if( !($result["status"] != 200 || $result["status"] != 204 )) return $result;
		if( strcasecmp( $currentChapter, "Canada") === 0 ){
			$this->logger->info( "scenario A");
			$this->logger->info( "putting user {$user['username']} in the {$newChapter} chapter..." );
			$result = $newMailChimp->addOrUpdateMember( $updatedUser );
		} else if( strcasecmp( $newChapter, "Canada" ) === 0 ){
			$this->logger->info( "scenario B");
			$this->logger->info( "deleting user {$user['username']} from the {$currentChapter} chapter..." );
			$result = $currentMailChimp->deleteMember( $user );
		} else {
			$this->logger->info( "deleting user {$user['username']} from the {$currentChapter} chapter..." );
			$this->logger->info( "scenario C");
			$currentMailChimp->deleteMember( $user );
			$result = $newMailChimp->addOrUpdateMember( $updatedUser );
		}
		return $result;
	}

	function subscribeUser( $user, $chapter ){
		return $this->updateUser( $user, $chapter );
	}

	function deleteUser( $user, $chapter ){
		$mailChimp = $this->mailChimp;
		$canadaMailChimp = $this->canadaMailChimp;
		$currentMailChimp = $mailChimp( $chapter );
		$this->logger->info( "deleting mailchimp contact {$user['username']} from Canada chapter..." );
		$result = $canadaMailChimp->deleteMember( $user );
		if( !($result["status"] != 200 || $result["status"] != 204 )) return $result;
		if( strcasecmp( $chapter, "Canada") !== 0 ){
			$this->logger->info( "deleting mailchimp contact {$user['username']} from {$chapter} chapter..." );
			$result = $currentMailChimp->deleteMember( $user );
		}
		return $result;
	}

	function updateUser( $user, $chapter ){
		$mailChimp = $this->mailChimp;
		$canadaMailChimp = $this->canadaMailChimp;
		$currentMailChimp = $mailChimp( $chapter );
		$this->logger->info( "updating mailchimp contact {$user['username']} in the Canada chapter..." );
		$result = $canadaMailChimp->addOrUpdateMember( $user );
		if( !($result["status"] != 200 || $result["status"] != 204 )) return $result;
		if( strcasecmp( $chapter, "Canada") !== 0 ){
			$this->logger->info( "updating mailchimp contact {$user['username']} in the {$chapter} chapter..." );
			$result = $currentMailChimp->addOrUpdateMember( $user );
		}
		return $result;
	}
}
