<?php
namespace App;
use \Utils\ApiBase;

class MailChimp extends ApiBase{
	public $chapterName;
	private $baseUrl;

	function __construct( $chapterDetails, $logger ){
		parent::__construct( "PHP-MCAPI/3.0", "MailChimp", $logger );

		$this->chapterName = $chapterDetails["name"];
		$apiKey = $chapterDetails["mcApikey"];
		$dataCenter = substr( $apiKey,strpos( $apiKey,"-" ) + 1 );
		$this->baseUrl = "https://{$dataCenter}.api.mailchimp.com/3.0/";
		$this->addHeader( "Authorization: apikey {$apiKey}" );
	}

	function urlFromPath($path){
		return $this->baseUrl . $path;
	}
}
