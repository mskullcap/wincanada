<?php

namespace App;
use PDO;

class ProfileDAO{
	private $db;
	private $logger;

	function __construct($db, $logger){
		$this->db = $db;
		$this->logger = $logger;
	}

	function findAll(){
		$statement = $this->db->prepare("
			SELECT
				( users.FIRSTNAME || ' ' || users.LASTNAME || ' [' || users.username || ']' ) as name,
				lower( users.username ) as username,
				jobs.name as job,
				profiles.intro as introduction,
				profiles.background as background,
				profiles.image as image,
				users.background as userBackground,
				chapters.name as chapterName,			
				users.prof_qualifications as qualifications,
				PROFILES.CREATE_DATE as createdOn
			FROM profiles, users, jobs, chapters
			WHERE
				chapters.id = users.chapter_id AND
				profiles.job_id=jobs.id AND
				profiles.USER_ID=users.id AND
				users.STATUS_ID = 1 AND
				users.PASSWORD != ''
			ORDER BY
			  createdOn DESC
		");
		$statement->execute();
		$profiles = $statement->fetchAll( PDO::FETCH_ASSOC );

		$statement = $this->db->prepare("
			SELECT
				( users.FIRSTNAME || ' ' || users.LASTNAME || ' [' || users.username || ']' ) as name,
				lower( users.username ) as username,
				users.background as userBackground,
				chapters.NAME as chapterName,
				users.prof_qualifications as qualifications,
				users.CREATE_DATE as createdOn
			FROM users, chapters
			WHERE 
				chapters.id = users.chapter_id AND
				users.status_id =1 AND 
				users.PASSWORD != '' AND
				users.id NOT IN ( select user_id from profiles );
		");
		$statement->execute();
		return array_merge( $profiles, $statement->fetchAll( \PDO::FETCH_ASSOC ));
	}

	function findAllIndex(){
		$statement = $this->db->prepare( "
			SELECT
				( users.FIRSTNAME || ' ' || users.LASTNAME ) as name, 
				lower( users.USERNAME ) as username, 
				users.JOB_TITLE as job, 
				users.EMPLOYER as employer,
				chapters.name as chapterName
			FROM users, chapters
			WHERE
				users.CHAPTER_ID = CHAPTERS.id AND
				users.STATUS_ID = 1 AND 
				users.PUBLISH = 1 AND
				users.PASSWORD != ''
			ORDER BY
				users.LASTNAME
		");
		$statement->execute();
		$short = $statement->fetchAll( \PDO::FETCH_ASSOC );
		$result = array();
		forEach( $short as $s ){
			$row = array( $s['name'], $s['username'], $s['job'], $s['employer'], $s['chapterName'] );
			$result[] = $row;
		}
		return $result;
	}

	function findByUsername( $username ){
		$statement = $this->db->prepare("
			SELECT
				jobs.name as job,
				profiles.intro as publishedIntroduction,
				profiles.background as publishedBackground,
				profiles.image as image,
				PROFILES.CREATE_DATE as createdOn,
				PROFILES.UPDATE_DATE as updatedOn
			FROM profiles, users, jobs, chapters
			WHERE
				chapters.id = users.chapter_id AND
				profiles.job_id=jobs.id AND
				profiles.USER_ID=users.id AND
				users.STATUS_ID = 1 AND
				users.PASSWORD != '' AND 
				users.username = :username
		");
		$statement->execute();
		$statement->bindValue(':username', $username);

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}


	function update( $username, $intro, $background ){
		$statement = $this->db->prepare("
			UPDATE profiles
			SET
			  intro = :intro,
			  background = :background
			FROM profiles, users
			WHERE
			  profiles.USER_ID=users.id AND
			  users.username = :username
		");

		$statement->bindValue(':intro', $intro);
		$statement->bindValue(':background', $background);
		$statement->bindValue(':username', $username);
		$statement->execute();
	}

}
