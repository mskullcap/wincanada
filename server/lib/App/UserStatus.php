<?php
namespace App;

abstract class UserStatus {
	const DEACTIVATED = -1;
	const PENDING = 0;
	const ACTIVATED = 1;
	const UNCONFIRMED = 2;

	public static function getIdFromStatus( $status ){
		if( empty( $status )) return UserStatus::ACTIVATED;
		if( strcasecmp( $status, "DEACTIVATED" )) return UserStatus::DEACTIVATED;
		if( strcasecmp( $status, "PENDING" )) return UserStatus::PENDING;
		if( strcasecmp( $status, "ACTIVATED" )) return UserStatus::ACTIVATED;
		if( strcasecmp( $status, "UNCONFIRMED" )) return UserStatus::UNCONFIRMED;
		return UserStatus::ACTIVATED;
	}
}
