<?php
namespace App;
use \Cloudinary\Uploader;
use \Cloudinary\Api;

class CloudinaryApi {
	private $logger;

	function __construct( $logger ){
		$this->logger = $logger;
		$this->api = new Api();
	}

	function upload( $type, $key, $from, $copyFunction, $deleteAfter = false ){
		$this->logger->info( "cloudinary upload {$type} ${key}" );
		$result = Uploader::upload( $from, [
			"public_id" => $key,
			"invalidate" => true,
			"folder" => $type,
			"overwrite" => true,
			"upload_preset" => "upload_" . $type
		]);

		$this->logger->info( "copying upload locally {$type} ${key}" );
		$copyFunction( $result["url"]);
		if( $deleteAfter ){
			$this->logger->info( "deleting upload on cloudinary {$type} ${key}" );
			$this->api->delete_resources([ $key ]);
		}
		return $result;
	}
}
