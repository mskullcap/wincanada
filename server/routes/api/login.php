<?php
use Firebase\JWT\JWT;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->post("/login", function ( Request $request, Response $response, $arguments) {
	$parsedBody = $request->getParsedBody();
	if( !isset( $parsedBody["username"] ) || !isset( $parsedBody["password"] )) return $response->withStatus( 422 );

	$username = $parsedBody["username"];
	$password = $parsedBody["password"];

	$user = $this->users->listByUsernameOrEmail( $username, true );

	if( empty( $user ) === 0 ){
		$this->logger->notice( "An attempt to login as $username failed because this user does not exist.");
		return $response->withStatus( 422 );
	}

	if( $user["status"] !== "ACTIVATED" ){
		$this->logger->notice( "An attempt to refresh the token for $username failed because this user is not activated.");
		return $response->withStatus( 422 );
	}

	if( empty( $user["salt"] )){
		if( !password_verify( $password, $user["password"] )){
			$this->logger->info( "An attempt to login as $username failed.");
			return $response->withStatus( 422 );
		}
		$this->logger->info( "An attempt to login as $username succeeded.");
		if( password_needs_rehash($user["password"], PASSWORD_DEFAULT )) {
			$this->logger->info( "Password for $username needs rehash.");
			$this->users->updatePasswordForUser( $username, $password );
		}
	} else {
		$hash = hash('sha256', $user["salt"] . hash('sha256', $password) );
		if( $hash != $user["password"]){
			$this->logger->info( "Old hash: an attempt to login as $username failed.");
			return $response->withStatus( 422 );
		}
		$this->logger->info( "Old hash: an attempt to login as $username succeeded.");
		// update the password here to use the default php crypto
		$this->users->updatePasswordForUser( $username, $password );
		$this->logger->info( "Updated password hash for $username");
	}

	$roles = $this->users->findRolesForUser( $user["id"] );
	$r = array();
	forEach( $roles as $role ) $r[] = $role['role'];
	$payload = generatePayload( $user, $r );
	$token = JWT::encode($payload, $this->settings["jwt"]["secret"], "HS256" );

	$user["token"] = $token;
	unset( $user["password"] );
	unset( $user["salt"] );

	return $response->withStatus( 200 )->withHeader( "Authorization", "Bearer $token" )->withJson( $user, 200, JSON_PRETTY_PRINT );
});

function generatePayload( $user, $roles ){
	$now = new DateTime();
	$future = new DateTime( \App\Config::JWT_TOKEN_LIFE );
	return [
		"iat" => $now->getTimeStamp(),
		"exp" => $future->getTimeStamp(),
		"context" => [
			"chapter" => $user["chapter"],
			"roles" => $roles,
			"username" => $user["username"],
			"id" => $user["id"]
		]
	];
}
