<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get( "/geo", function (Request $request, Response $response) {
	if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
		$ip = $_SERVER["HTTP_CLIENT_IP"];
	} elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
		$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	} else {
		$ip = $_SERVER["REMOTE_ADDR"];
	}

	if( !isset( $ip )) return $response;
	$result = $this->geo->lookupIp( $ip );
	if( $result["isError"]) $response->withJson( $result, $result["status"], JSON_PRETTY_PRINT );

	$geo = $result["result"];
	if( isset( $geo["country_code"])){
		if( isset( $geo["region_code"] )){
			$chapterGuess = $this->chapters->guessChapter($geo["region_code"], $geo["latitude"], $geo["longitude"]);
			if( isset( $chapterGuess )) $geo["chapter"] = $chapterGuess;
		}
	}
	return $response->withJson( $geo, 200, JSON_PRETTY_PRINT );
});
