<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Firebase\JWT\JWT;

use \Utils\Mail;
use \Utils\Validator;

$app->post('/password', function (Request $request, Response $response) {
	$json = $request->getParsedBody();
	$token = $json["token"];
	if (empty($token)) return $response->withStatus(400)->withHeader("X-Status-Reason", "Token is invalid or not provided.");

	$decoded = JWT::decode($token, $this->settings["jwt"]["email_secret"], array("HS256"));
	$username = $decoded->context->username;
	if (empty($username)) return $response->withStatus(400)->withHeader("X-Status-Reason", "Username in token is invalid.");

	$user = $this->users->listByUsername( $username );
	if (empty($user)) return $response->withStatus(404)->withHeader("X-Status-Reason", "Username in token doesn't exist.");

	$password = $json["password"];
	$reasons = array();
	Validator::validPassword( $password, $reasons );
	foreach( $reasons as $key=>$field ) if( sizeof( $field ) == 0 ) unset( $reasons[$key]);
	if( sizeof( $reasons ) > 0 ) return $response->withStatus( 422 )->withJson( $reasons );

	$this->users->updatePasswordForUser( $username, $password );
	$this->logger->info( "Password updated for $username" );
	return $this->response->withJson( $user );
});

$app->get('/password/{username}', function (Request $request, Response $response) {
	$username = $request->getAttribute( "username" ); // or email?
	$user = $this->users->listByUsernameOrEmail( $username );
	if( empty( $user )) return $response->withStatus( 422 )->withHeader( "X-Status-Reason", "User with username or email {$username} doesn't exist." );

	$emailSuccess = Mail::sendForgotPasswordEmail( $user );
	$reasons = [];

	if( $emailSuccess ){
		$this->logger->info( "$username attempted to reset password.  An email has been sent to {$user["email"]}." );
		$result = [
			"status" => "Password reset confirmed!  An email has been sent to you with instructions on how to reset your password."
		];
		return $response->withJson( $result );
	}

	array_push( $reasons["email"], "We experienced a failure when emailing to this address.  This may be due to a problem with your email address, or a problem with our email server.  Possibly try another email address?" );
	return $response->withStatus( 422 )->withJson( $reasons );
});
