<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Utils\Perms;
use \Utils\Validator;

$app->get("/users", function (Request $request, Response $response) {
	if( !Perms::tokenIsSuper( $this->token )) return $response->withStatus( 403 );

	$publishMode = $request->getQueryParam( "publish", $default = NULL) != NULL;
	if( $publishMode === true ){
		$result = $this->users->listAllPublic();
		$this->logger->warning("publishing to ". $this->settings["publishFolder"] . "profiles.json");
		$fp = fopen($this->settings["publishFolder"] . "profiles.json", "w");
		fwrite($fp, json_encode($result, JSON_PRETTY_PRINT));
		fclose($fp);
		return $this->response->withJson( $result, 200, JSON_PRETTY_PRINT );
	}

	$result = $this->users->listAll();
	return $this->response->withJson( $result, 200, JSON_PRETTY_PRINT );
});

$app->delete( "/users/{username}", function (Request $request, Response $response) {
	$username = $request->getAttribute("username");
	if (!Perms::tokenIsAdministrator( $this->token )) return $response->withStatus(403);

	$user = $this->users->listByUsername( $username );
	if( empty( $user )) return $response->withStatus(404);

	$this->logger->info( "Deleting user  {$user['username']}" );
	$this->users->removeUserById( $user["id"] );
	$this->publisher->removeProfileByUsername( $username );

	$this->logger->info( "User {$user['username']} current optIn status {$user['optIn']}" );
	if( $user["optIn"] == 1 ){
		$this->logger->info( "Removing user {$user['username']} from Canada mailing list..." );
		$result = $this->mailChimpUserManager->deleteUser( $user, $user["chapter"] );
	}

	$this->logger->info( "User $username deleted." );
	$result = [ "status" => "User " . $username . " deleted." ];
	return $response->withStatus( 200 )->withJson( $result );
});

$app->get("/users/{username}", function (Request $request, Response $response) {
	$username = $request->getAttribute("username");
	$user = $this->users->listByUsername( $username );
	if( empty( $user )) return $this->response->withStatus( 404 );
	if( Perms::tokenIsUserOrSuper( $username, $this->token )) return $this->response->withJson( $user, 200, JSON_PRETTY_PRINT );
	if( $user["public"] !== 1 ) return $response->withStatus( 403 );
	return $this->response->withJson( $this->users->makePresentable( $user ), 200, JSON_PRETTY_PRINT );
});

$app->post("/users/{username}", function (Request $request, Response $response) {
	$username = $request->getAttribute("username");
	if( !Perms::tokenIsUserOrSuper( $username, $this->token )) return $response->withStatus( 403 );
	$user = $this->users->listByUsername( $username );
	if( empty( $user )) return $this->response->withStatus( 404 );

	$json = $request->getParsedBody();

	$reasons = [];

	$firstName = $json["firstName"];
	if( isset( $firstName )){
		$firstName = trim( $firstName );
		Validator::validFirstName( $firstName, $reasons );
	} else {
		$firstName = $user["firstName"];
	}

	$lastName = $json["lastName"];
	if( isset( $lastName )){
		$lastName = trim( $lastName );
		Validator::validLastName( $lastName, $reasons );
	} else {
		$lastName = $user["lastName"];
	}

	$jobTitle = $json["jobTitle"];
	if( isset( $jobTitle )) {
		$jobTitle = trim($jobTitle);
		Validator::validJobTitle($jobTitle, $reasons);
	} else {
		$jobTitle = $user["jobTitle"];
	}

	$employer = $json["employer"];
	if( isset( $employer )) {
		$employer = trim($employer);
		Validator::validEmployer($employer, $reasons);
	} else {
		$employer = $user["employer"];
	}

	$city = $json["city"];
	if( isset( $city )) {
		$city = trim($city);
		Validator::validCity($city, $reasons);
	} else {
		$city = $user["city"];
	}

	$province = $json["province"];
	if( isset( $province )) {
		$province = trim($province);
		Validator::validProvince($province, $reasons);
	} else {
		$province = $user["province"];
	}

	$email = $json["email"];
	if( isset( $email )) {
		$email = strtolower( trim($email));
		Validator::validEmail($email, $reasons);
	} else {
		$email = $user["email"];
	}

	$postalCode = $json["postalCode"];
	if( isset( $postalCode )) {
		$postalCode = strtoupper(trim($postalCode));
		Validator::validPostalCode($postalCode, $reasons);
	} else {
		$postalCode = $user["postalCode"];
	}

	$roles = $json["roles"];
	if( Perms::tokenIsSuper( $this->token )){
		if( isset( $roles )) {
			Validator::validRoles($roles, $reasons);
			$roles = implode( ",", $roles );
		} else {
			$roles = $user["roles"];
		}
	} else {
		$roles = $user["roles"];
	}

	$chapter = $json["chapter"];
	if( isset( $chapter )) $chapter = trim( $chapter );
	Validator::validChapter( $chapter, $reasons );

	$public = $json["public"];
	if( isset( $public )){
		Validator::validPublic( intval( $public ), $reasons );
	} else {
		$public = $user["public"];
	}

	$optIn = $json["optIn"];
	if( isset( $optIn )){
		Validator::validOptIn( intval( $optIn ), $reasons );
	} else {
		$optIn = $user["optIn"];
	}

	foreach( $reasons as $key=>$field ) if( sizeof( $field ) == 0 ) unset( $reasons[$key]);
	if( sizeof( $reasons ) > 0 ) return $response->withStatus( 422 )->withJson( $reasons );

	$salutation = $json["salutation"];
	if( !isset( $salutation ) || !in_array( $salutation, \App\Config::SALUTATIONS )) $salutation = $user["salutation"];

	$address1 = $json["address1"];
	if( !isset( $address1 )) $address1 = $user["address1"];

	$address2 = $json["address2"];
	if( !isset( $address2 )) $address2 = $user["address2"];

	$background = $json["background"];
	if( !isset( $background )) $background = $user["background"];

	$qualifications = $json["qualifications"];
	if( !isset( $qualifications )) $qualifications = $user["qualifications"];

	$bioBackground = $json["bioBackground"];
	if( !isset( $bioBackground )) $bioBackground = $user["bioBackground"];

	$bioFeatured = $json["bioFeatured"];
	if( !isset( $bioFeatured )) $bioFeatured = $user["bioFeatured"];

	$bioIntro = $json["bioIntro"];
	if( !isset( $bioIntro )) $bioIntro = $user["bioIntro"];

	$bioImage = $json["bioImage"];
	if( !isset( $bioImage )) $bioImage = $user["bioImage"];

	$isChapterChange = $user["chapter"] !== $chapter;
	$isOptInChange = $user["optIn"] !== $optIn;
	$isEmailChange = $user["email"] !== $email;
	$isPublicChange = $user["public"] !== $public;
	$isNameChange = $user["firstName"] !== $firstName || $user["lastName"] !== $lastName;

	$isRoleChange = $user["roles"] !== $roles;

	$updatedUser = $user;
	$updatedUser["firstName"] = $firstName;
	$updatedUser["lastName"] = $lastName;
	$updatedUser["jobTitle"] = $jobTitle;
	$updatedUser["employer"] = $employer;
	$updatedUser["city"] = $city;
	$updatedUser["email"] = $email;
	$updatedUser["address1"] = $address1;
	$updatedUser["address2"] = $address2;
	$updatedUser["postalCode"] = $postalCode;
	$updatedUser["province"] = $province;
	$updatedUser["postalCode"] = $postalCode;
	$updatedUser["roles"] = $roles;
	$updatedUser["chapter"] = $chapter;
	$updatedUser["salutation"] = $salutation;
	$updatedUser["background"] = $background;
	$updatedUser["qualifications"] = $qualifications;
	$updatedUser["optIn"] = intval( $optIn );
	$updatedUser["public"] = intval( $public );
	$updatedUser["winGlobal"] = $winGlobal;
	$updatedUser["bioBackground"] = $bioBackground;
	$updatedUser["bioFeatured"] = intval( $bioFeatured );
	$updatedUser["bioIntro"] = $bioIntro;
	$updatedUser["bioImage"] = $bioImage;

	$this->users->updateUser( $updatedUser ); // update the database first, we can deal with mailchimp problems later

	if( $optIn === 0 ){
		if( $isOptInChange ){
			$result = $this->mailChimpUserManager->deleteUser( $updatedUser, $user["chapter"] );
		}
	} else {
		if( $isOptInChange ){
			$result = $this->mailChimpUserManager->subscribeUser( $updatedUser, $user["chapter"] );
			// subscribe them
		}
		if( $isChapterChange ){
			$result = $this->mailChimpUserManager->changeChapters( $updatedUser, $user["chapter"], $chapter );
		} else if( $isNameChange || $isEmailChange ){
			$result = $this->mailChimpUserManager->updateUser( $updatedUser, $chapter );
		}
	}

	if( $isPublicChange ){
		$result = $this->users->listAllPublic();
		$this->logger->warning("publishing to ". $this->settings["publishFolder"] . "profiles.json");
		$fp = fopen($this->settings["publishFolder"] . "profiles.json", "w");
		fwrite($fp, json_encode($result, JSON_PRETTY_PRINT));
		fclose($fp);
	}

	if( $isRoleChange ){
		$results = $this->chapters->findAllContacts();
		$this->publisher->publishContacts( $results );
	}

	return $this->response->withJson( $updatedUser, 200, JSON_PRETTY_PRINT );
});


$app->post("/users/{username}/thumbnail", function (Request $request, Response $response) {
	$username = $request->getAttribute("username");
	if( !Perms::tokenIsUserOrSuper( $username, $this->token )) return $response->withStatus( 403 );

	$fileName = $_FILES["file"]["name"];
	$fileSize = $_FILES["file"]["size"];
	$fileTmpName = $_FILES["file"]["tmp_name"];
	$fileExtensions = ["jpeg","jpg","png"];
	$fileExtension = strtolower(end(explode(".", $fileName)));

	$this->logger->info( "File: $fileName $fileTmpName $fileSize" );

	$reasons = [];
	if( !in_array($fileExtension,$fileExtensions)) array_push( $reasons, "Only jpeg, jpg and png image types are accepted.");
	if( $fileSize > 10000000 ) array_push( $reasons, "Maximum acceptable image file size is 10Mb." );
	if( !empty($errors)) return $response->withStatus( 422 )->withJson( $reasons );

	$result = $this->cloudinary->upload( "users", $username, $fileTmpName, function( $url ) use ( $username ){
		$this->publisher->publishProfilePhoto( $username, $url );
	} );

	return $this->response->withJson( $result, 200, JSON_PRETTY_PRINT );
});
