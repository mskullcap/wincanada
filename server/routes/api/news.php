<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Utils\Validator;
use Utils\Perms;

function getIdFromKey( $key ){
	if( !preg_match( '/^[0-9]{8}_([0-9]+)$/', $key, $matches )) return false;
	return $matches[1];
}

$app->get("/news", function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$news = $this->news->findSince();
	$this->publisher->publishNews( $news );
	return $response->withJson($news);
});

$app->post('/news', function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$json = $request->getParsedBody();

	$reasons = array();

	$title = $json["title"];
	if( isset( $title )) $title = trim( $title );
	Validator::validTitle( $title, $reasons );

	$description = $json["description"];
	if( isset( $description )) $description = trim( $description );
	Validator::validDescription( $description, $reasons );

	$content = $json["content"];
	if( isset( $content )) $content = trim( $content );
	Validator::validContent( $content, $reasons );

	foreach( $reasons as $key=>$field ) if( sizeof( $field ) == 0 ) unset( $reasons[$key]);
	if( sizeof( $reasons ) > 0 ) return $response->withStatus( 422 )->withJson( $reasons );

	$newItem = $this->news->create( $title, $description, $content );

	return $response->withStatus( 201 )->withJson( $newItem );
});

$app->post('/news/{key}', function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$id = getIdFromKey( $request->getAttribute("key"));
	if( $id == false ) return $response->withStatus(404);

	$news = $this->news->findById( $id );
	if( empty( $news )) return $response->withStatus(404);

	$json = $request->getParsedBody();
	$reasons = array();

	$title = $json["title"];
	if( isset( $title )) $title = trim( $title );
	Validator::validTitle( $title, $reasons );

	$description = $json["description"];
	if( isset( $description )) $description = trim( $description );
	Validator::validDescription( $description, $reasons );

	$content = $json["content"];
	if( isset( $content )) $content = trim( $content );
	Validator::validContent( $content, $reasons );

	foreach( $reasons as $key=>$field ) if( sizeof( $field ) == 0 ) unset( $reasons[$key]);
	if( sizeof( $reasons ) > 0 ) return $response->withStatus( 422 )->withJson( $reasons );

	$item = $this->news->update( $id, $title, $description, $content ); // assume if you are saving, you are publishing

	$news = $this->news->findSince();
	$this->publisher->publishNews( $news );
	return $response->withJson( $item );
});

$app->delete('/news/{key}', function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$id = getIdFromKey( $request->getAttribute("key"));
	if( $id == false ) return $response->withStatus(404);

	$this->news->remove( $id );

	$news = $this->news->findSince();
	$this->publisher->publishNews( $news );
	$result = [ "status" => "News item " . $id . " deleted." ];
	return $response->withStatus( 200 )->withJson( $result );
});


$app->post("/news/{key}/thumbnail", function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$key = urldecode( $request->getAttribute('key'));

	$fileName = $_FILES["file"]["name"];
	$fileSize = $_FILES["file"]["size"];
	$fileTmpName = $_FILES["file"]["tmp_name"];
	$fileExtensions = ["jpeg","jpg","png"];
	$fileExtension = strtolower(end(explode(".", $fileName)));

	$this->logger->info( "File: $fileName $fileTmpName $fileSize" );

	$reasons = [];
	if( !in_array( $fileExtension, $fileExtensions)) array_push( $reasons, "Only jpeg, jpg and png image types are accepted.");
	if( $fileSize > 1000000 ) array_push( $reasons, "Maximum acceptable news image file size is 1Mb." );
	list($width, $height, $type, $attr) = getimagesize($fileTmpName);
	if( $width < 800 ) array_push( $reasons, "The news image should be a minimum of 800px wide." );
	if( $height < 500 ) array_push( $reasons, "The news image should be a minimum of 500px tall." );
	if( !empty($reasons)) return $response->withStatus( 422 )->withJson( $reasons );

	$result = $this->cloudinary->upload( "news", $key, $fileTmpName, function( $url ) use ( $key ){
		$this->publisher->publishNewsPhoto( $key, $url );
	} );

	return $this->response->withJson( $result, 200, JSON_PRETTY_PRINT );
});
