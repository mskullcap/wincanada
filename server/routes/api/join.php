<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Utils\Mail;
use \Utils\Validator;

$app->post("/join", function (Request $request, Response $response) {
	$json = $request->getParsedBody();

	$reasons = array();

	$username = $json["username"];
	if( isset( $username )) $username = strtolower( trim( $username ));
	Validator::validUsername( $username, $reasons );

	$email = $json["email"];
	if( isset( $email )) $email = strtolower( trim( $email ));
	Validator::validEmail( $email, $reasons );

	$emailTaken = $this->users->userExistsByEmail( $email );
	$usernameTaken = $this->users->userExistsByUsername( $username );

	if( $emailTaken === true ){
		if( !isset( $reasons["email"] )) $reasons["email"] = array();
		array_push( $reasons["email"], "This email has already been registered." );
	}
	if( $usernameTaken === true ){
		if( !isset( $reasons["email"] )) $reasons["email"] = array();
		array_push( $reasons["username"], "This username is taken." );
	}

	foreach( $reasons as $key=>$field ) if( sizeof( $field ) == 0 ) unset( $reasons[$key]);
	if( sizeof( $reasons ) > 0 ) return $response->withStatus( 422 )->withJson( $reasons );

	$emailSuccess = Mail::sendWelcomeEmail( $email, $username );

	if( $emailSuccess ){
		$this->logger->info( "$username attempted to join WiN Canada.  An email has been sent to $email." );
		$result = [
			"status" => "Welcome to WiN Canada!  An email has been sent to $email.  Once you've verified your email address we can complete the registration process."
		];
		return $response->withJson( $result );
	}

	if( !isset( $reasons["email"] )) $reasons["email"] = array();
	array_push( $reasons["email"], "We experienced a failure when emailing to this address.  This may be due to a problem with your email address, or a problem with our email server.  Possibly try another email address?" );
	return $response->withStatus( 422 )->withJson( $reasons );
});
