<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Utils\Perms;
use Utils\Validator;

$app->get('/events', function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$events = $this->events->findSince();
	$this->publisher->publishEvents( $events );
	return $response->withStatus(200)->withJson( $events );
});

$app->post('/events', function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$json = $request->getParsedBody();

	$reasons = array();

	$title = $json["title"];
	if( isset( $title )) $title = trim( $title );
	Validator::validTitle( $title, $reasons );

	$description = $json["description"];
	if( isset( $description )) $description = trim( $description );
//	Validator::validDescription( $description, $reasons );

	$content = $json["content"];
	if( isset( $content )) $content = trim( $content );
	Validator::validContent( $content, $reasons );

	$startDate = $json["startDate"];
	if( isset( $startDate )) $startDate = trim( $startDate );
	$endDate = $json["endDate"];
	if( isset( $endDate )) $endDate = trim( $endDate );
	Validator::validStartDateEndDate( $startDate, $endDate, $reasons );

	$location = $json["location"];
	if( isset( $location )) $location = trim( $location );
	Validator::validLocation( $location, $reasons );

	$chapter = $json["chapter"];
	if( isset( $chapter )) $chapter = trim( $chapter );
	Validator::validChapter( $chapter, $reasons );

	foreach( $reasons as $key=>$field ) if( sizeof( $field ) == 0 ) unset( $reasons[$key]);
	if( sizeof( $reasons ) > 0 ) return $response->withStatus( 422 )->withJson( $reasons );

	$event = $this->events->create( $title, $chapter, $description, $content, $location, $startDate, $endDate );

	return $response->withJson( $event );
});


$app->post('/events/{key}', function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$key = $request->getAttribute("key");
	$id = getIdFromKey( $key );
	if( $id == false ) return $response->withStatus(404);

	$event = $this->events->findById( $id );
	if( empty( $event )) return $response->withStatus(404);

	$json = $request->getParsedBody();

	$reasons = array();

	$title = $json["title"];
	if( isset( $title )) $title = trim( $title );
	Validator::validTitle( $title, $reasons );

	$description = $json["description"];
	if( isset( $description )) $description = trim( $description );
//	Validator::validDescription( $description, $reasons );

	$content = $json["content"];
	if( isset( $content )) $content = trim( $content );
	Validator::validContent( $content, $reasons );

	$startDate = $json["startDate"];
	if( isset( $startDate )) $startDate = trim( $startDate );
	$endDate = $json["endDate"];
	if( isset( $endDate )) $endDate = trim( $endDate );
	Validator::validStartDateEndDate( $startDate, $endDate, $reasons );

	$location = $json["location"];
	if( isset( $location )) $location = trim( $location );
	Validator::validLocation( $location, $reasons );

	$chapter = $json["chapter"];
	if( isset( $chapter )) $chapter = trim( $chapter );
	Validator::validChapter( $chapter, $reasons );

	foreach( $reasons as $key=>$field ) if( sizeof( $field ) == 0 ) unset( $reasons[$key]);
	if( sizeof( $reasons ) > 0 ) return $response->withStatus( 422 )->withJson( $reasons );
	if( isset( $location )) $location = trim( $location );

	$event = $this->events->update( $id, $title, $chapter, $description, $content, $location, $startDate, $endDate );

	$since = $request->getQueryParam('since', $default = '-1 year');
	$events = $this->events->findSince( $since );
	$this->publisher->publishEvents( $events );

	return $response->withJson( $event );
});

$app->delete('/events/{key}', function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$key = $request->getAttribute("key");
	$id = getIdFromKey( $key );
	if( $id == false ) return $response->withStatus(404);

	$this->events->deleteById( $id );
	$events = $this->events->findSince();
	$this->publisher->publishEvents( $events );
	$result = [ "status" => "Events item " . $id . " deleted." ];
	return $response->withStatus( 200 )->withJson( $result );
});

$app->post("/events/{key}/thumbnail", function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$key = $request->getAttribute("key");
	$id = getIdFromKey( $key );
	if( $id == false ) return $response->withStatus(404);

	$fileName = $_FILES["file"]["name"];
	$fileSize = $_FILES["file"]["size"];
	$fileTmpName = $_FILES["file"]["tmp_name"];
	$fileExtensions = ["jpeg","jpg","png"];
	$fileExtension = strtolower(end(explode(".", $fileName)));

	$reasons = [];
	if( !in_array( $fileExtension, $fileExtensions)) array_push( $reasons, "Only jpeg, jpg and png image types are accepted.");
	if( $fileSize > 2000000 ) array_push( $reasons, "Maximum acceptable image file size is 2Mb." );
	if( !empty($errors)) return $response->withStatus( 422 )->withJson( $reasons );

	$result = $this->cloudinary->upload( "events", $key, $fileTmpName );
	return $response->withStatus( 200 )->withJson( $result );
});
