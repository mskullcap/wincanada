<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Utils\Perms;

$app->any("/mc/callback/{token}", function (Request $request, Response $response) {
	$token = $request->getAttribute( "token" );

	$chapterDetails = $this->chapters->getDetailsFromWebhookKey( $token );
	if( empty( $chapterDetails )){ // silent return if we got a bad key
		$this->logger->warn( "WARNING!!! Received BAD mc/callback with token {$token}" );
		return $response;
	}

	if( !$request->isPost()) return $response->withStatus( 200 ); // ignore non-posts

	$body = $request->getParsedBody();
	if( $body === null ) return $response->withStatus( 200 ); // ignore empty bodies

	$mailchimpRequestType = $body["type"];
	$chapterName = $chapterDetails["name"];
	$isSourceListCanada = ( strcasecmp( $chapterName, "Canada" ) === 0 );
	$this->logger->info( "Received mc/callback for Chapter {$chapterName}" );
	$this->logger->debug( json_encode( $body, JSON_PRETTY_PRINT ));

	// only handle these webhooks
	if( array_search( $mailchimpRequestType, ["subscribe", "unsubscribe", "profile"] ) === false ) return $response->withStatus( 200 );

	$username = strtolower( trim( $body["data"]["merges"]["USERNAME"] ));
	$email = strtolower( trim( $body["data"]["merges"]["EMAIL"] ));
	$lastName = trim( $body["data"]["merges"]["LNAME"]);
	$firstName = trim( $body["data"]["merges"]["FNAME"]);
	$chapter = trim( $body["data"]["merges"]["CHAPTER"]); // this will only be present for the National Chapter
	$user = $this->users->listByUsername( $username );
	if( empty( $user )){
		// we *could* create a new user... let's leave this for later
		$this->logger->warn( "WARNING!!! Received BAD mc/callback - user with username {$username} doesn't exist!" );
		return $response->withStatus( 200 );
	}

	$this->logger->info( "found {$username}" . json_encode( $user, JSON_PRETTY_PRINT ));

	$isChapterChange = ($isSourceListCanada && (strcasecmp( $user["chapter"], $chapter ) !== 0 ));
	$isEmailChange = ( strcasecmp( $user["email"], $email ) !== 0 );
	$isNameChange = ( $user["firstName"] !== $firstName || $user["lastName"] !== $lastName );
	$isOptInChange = ( $mailchimpRequestType === "unsubscribe" || $mailchimpRequestType === "subscribe" );
	$isUnsubscribe = $mailchimpRequestType === "unsubscribe";
	$isDelete = (isset( $body["data"]["action"] ) && $body["data"]["action"] === "delete" );

	if(!($isChapterChange || $isEmailChange || $isNameChange || $isOptInChange || $isDelete )){
		$this->logger->info( "mailchimp useless request for username {$username}");
		return $response->withStatus( 200 );
	}

	// OK!  ALl is good, let's process the webhook...

	// ---------
	// CHANGE OF PLAN....
	// Since historically "DEACTIVATED" didn't really mean deactivated (it meant opt-out), we are going to run out of space for
	// mailchimp members.  We will have to DELETE members from the lists when they unsubscribe,
	// and we will stop using DEACTIVATED status.
	// ---------

	if( $isOptInChange ){
		$this->logger->info( "optIn change for {$username} from {$user['optIn']} to {$mailchimpRequestType}" );
		$user["optIn"] = $mailchimpRequestType === "unsubscribe" ? 0 : 1;
	}

	if( $isNameChange ){
		$this->logger->info( "name change for {$username} from {$user['firstName']} {$user['lastName']} to {$firstName} {$lastName}" );
		$user["firstName"] = $firstName;
		$user["lastName"] = $lastName;
	}

	if( $isEmailChange ){ // we should send an email confirmation here
		$this->logger->info( "email change for {$username} from {$user['email']} to {$email}" );
		$user["email"] = $email;
	}

	$mailChimpUserManager = $this->mailChimpUserManager;

	if( $isChapterChange ){
		$this->logger->info( "chapter change for {$username} from {$user['chapter']} to {$chapter}" );
		$mailChimpUserManager->changeChapters( $user, $user["chapter"], $chapter );
		$user["chapter"] = $chapter;
	} else if( $isDelete || ( $isUnsubscribe && $isOptInChange )){
		$this->logger->info( "delete, unsubscribe, optin change for {$username}" );
		$mailChimpUserManager->deleteUser( $user, $user["chapter"] );
	} else {
		$mailChimpUserManager->updateUser( $user, $user["chapter"] );
	}

	$this->logger->info( "updating database for {$username}");
	$this->users->updateUser( $user );
	return $response->withStatus( 200 );
});

$app->any("/mc/{chapter}/api/{api:.+}", function (Request $request, Response $response) {
	if( !Perms::tokenIsSuper( $this->token )) return $response->withStatus( 403 );

	$api = $request->getAttribute( "api" );
	$chapter = $request->getAttribute( "chapter" );
	$chapterDetails = $this->chapters->getDetails( $chapter );
	if( !isset( $chapterDetails)) return $response->withStatus( 422 )->withJson( [ "chapter" => "Invalid chapter."] );

	$mailChimp = $this->mailChimp;
	$mc = $mailChimp( $chapter );
	$result = $mc->getApi()->connect( $api, $request->getMethod(), $request->getMethod() === "GET" ? $request->getQueryParams() : $request->getParsedBody());
	return $response->withJson( $result, $result["status"], JSON_PRETTY_PRINT );
});

$app->delete("/mc/{chapter}/members/{username}", function (Request $request, Response $response) {
	$chapter = $request->getAttribute( "chapter" );
	if(!( Perms::tokenIsSuper( $this->token ))) return $response->withStatus(403);

	$chapterDetails = $this->chapters->getDetails( $chapter );
	if( !isset( $chapterDetails)) return $response->withStatus( 422 )->withJson( [ "chapter" => "Invalid chapter."] );

	$username = $request->getAttribute( "username" );
	$user = $this->users->listByUsernameOrEmail( $username );
	if( empty( $user )) return $response->withStatus( 404 );

	$result = $this->mailChimpUserManager->deleteUser( $user, $user["chapter"] );
	if( $result["isError"] === true ) return $response->withJson( $result, $result["status"], JSON_PRETTY_PRINT );
	return $response->withJson( $result["result"], $result["status"], JSON_PRETTY_PRINT );
});

$app->post("/mc/{chapter}/members/{username}", function (Request $request, Response $response) {
	$chapter = $request->getAttribute( "chapter" );
	if(!( Perms::tokenIsSuper( $this->token ))) return $response->withStatus(403);

	$chapterDetails = $this->chapters->getDetails( $chapter );
	if( !isset( $chapterDetails)) return $response->withStatus( 422 )->withJson( [ "chapter" => "Invalid chapter."] );

	$username = $request->getAttribute( "username" );
	$user = $this->users->listByUsernameOrEmail( $username );
	if( empty( $user )) return $response->withStatus( 404 );

	$chapterName = $chapterDetails["name"];
	$mailChimp = $this->mailChimp;
	$mailChimp = $mailChimp( $chapterName );

	$result = $mailChimp->addOrUpdateMember( $user );
	if( $result["isError"] === true ) return $response->withJson( $result, $result["status"], JSON_PRETTY_PRINT );
	return $response->withJson( $result["result"], $result["status"], JSON_PRETTY_PRINT );
});

$app->get("/mc/{chapter}/members/{username}", function (Request $request, Response $response) {
	$chapter = $request->getAttribute( "chapter" );
	if(!( Perms::tokenIsSuper( $this->token ))) return $response->withStatus(403);

	$chapterDetails = $this->chapters->getDetails( $chapter );
	if( !isset( $chapterDetails)) return $response->withStatus( 422 )->withJson( [ "chapter" => "Invalid chapter."] );

	$username = $request->getAttribute( "username" );
	$user = $this->users->listByUsernameOrEmail( $username );
	if( empty( $user )) return $response->withStatus( 404 );

	$chapterName = $chapterDetails["name"];
	$mailChimp = $this->mailChimp;
	$mailChimp = $mailChimp( $chapterName );

	$result = $mailChimp->getMember( $user );
	if( $result["isError"] === true ) return $response->withJson( $result, $result["status"], JSON_PRETTY_PRINT );
	return $response->withJson( $result["result"], $result["status"], JSON_PRETTY_PRINT );
});

$app->get("/mc/{chapter}/diff", function (Request $request, Response $response) {
	$chapter = $request->getAttribute( "chapter" );
	if(!( Perms::tokenIsSuper( $this->token ))) return $response->withStatus(403);

	$chapterDetails = $this->chapters->getDetails( $chapter );
	if( !isset( $chapterDetails)) return $response->withStatus( 422 )->withJson( [ "chapter" => "Invalid chapter."] );

	$mailChimp = $this->mailChimp;
	$chapterName = $chapterDetails["name"];

	$result = $mailChimp( $chapterName )->members();
	if( $result["isError"] === true ) return $response->withJson( $result, $result["status"], JSON_PRETTY_PRINT );

	$members = array_map(function( $member ){
		return \App\MailChimpApi::memberToUser( $member );
	}, $result["result"]["members"]);

	usort( $members, function( $a, $b ){
		return strcasecmp( $a["username"], $b["username"]);
	});

	$membersUsernames = array_map( function( $member ){
		return $member["username"];
	}, $members );

	$members = array_combine( $membersUsernames, $members );

	if( strcasecmp( $chapterName, "canada") === 0 ) $users = $this->users->listAll( false, true );
	else $users = $this->users->listAllByChapter( $chapterName, true );

	$users = array_map(function( $user ){
		return [
			"username" => $user["username"],
			"chapter" => $user["chapter"],
			"firstName" => $user["firstName"],
			"lastName" => $user["lastName"],
			"email" => $user["email"],
			"status" => $user["optIn"] == 1 ? "subscribed" : "unsubscribed"
		];
	}, $users );

	usort( $users, function( $a, $b ){
		return strcasecmp( $a["username"], $b["username"]);
	});

	$usersUsernames = array_map( function( $user ){
		return $user["username"];
	}, $users );

	$users = array_combine( $usersUsernames, $users );

	$missingUsernamesFromDatabase = array_diff( $membersUsernames, $usersUsernames );
	$missingUsernamesFromMailchimp = array_diff( $usersUsernames, $membersUsernames );

	$missingFromMailChimp = [];
	$missingFromDatabase = [];

	foreach( $missingUsernamesFromMailchimp as $missing ){
		array_push( $missingFromMailChimp, $users[$missing] );
		unset( $users[$missing]);
	}

	foreach( $missingUsernamesFromDatabase as $missing ){
		array_push( $missingFromDatabase, $members[$missing]);
		unset( $members[$missing]);
	}

	$mismatched = [];
	foreach( $users as $user ){
		$member = $members[$user["username"]];
		$mismatchedFields = [];
		foreach( ["email", "firstName", "lastName", "chapter", "status"] as $field ){
			if( $member[$field] !== $user[$field]) array_push( $mismatchedFields, $field );
		}
		if( count( $mismatchedFields ) > 0 ){
			array_push( $mismatched,[
				"mismatchedFields" => $mismatchedFields,
				"mailChimp" => $member,
				"user" => $user
			]);
		}
	}

	$result = [
		"missingFromMailChimp" => $missingFromMailChimp,
		"missingFromDatabase" => $missingFromDatabase,
		"mismatched" => $mismatched
	];

	return $response->withStatus(200)->withJson( $result, 200, JSON_PRETTY_PRINT );
});

$app->get("/mc/{chapter}/sync", function (Request $request, Response $response) {
	$chapter = $request->getAttribute( "chapter" );
	if(!( Perms::tokenIsSuper( $this->token ))) return $response->withStatus(403);

	$chapterDetails = $this->chapters->getDetails( $chapter );
	if( !isset( $chapterDetails)) return $response->withStatus( 422 )->withJson( [ "chapter" => "Invalid chapter."] );

	$mailChimp = $this->mailChimp;
	$chapterName = $chapterDetails["name"];

	// we made it here, we are going to try to sync between MailChimp and our database
	// If we are syncing everything, we start with the chapters, then end with the national chapter.
	$users = $this->users->listAll( false, true );
	if( strcasecmp( $chapter, "canada" ) === 0 ){ // for the canada chapter, sync all chapters AND canada
		$chapters = array_keys( $this->chapters->getDetails());
		foreach( $chapters as $c ){
			$mcApi = $mailChimp( $c );
			$mcApi->syncMembers( $users );
		}
	} else {
		$mcApi = $mailChimp( $chapterName );
		$mcApi->syncMembers( $users );
		$mcApi = $mailChimp( "canada" );
		$mcApi->syncMembers( $users );
	}
	return $response->withStatus( 200 )->withJson( $mailChimp( $chapter )->members());
});

$app->get("/mc/{chapter}", function (Request $request, Response $response) {
	$chapter = $request->getAttribute( "chapter" );
	if(!( Perms::tokenIsSuper( $this->token ))) return $response->withStatus(403);

	$chapterDetails = $this->chapters->getDetails( $chapter );
	if( !isset( $chapterDetails)) return $response->withStatus( 422 )->withJson( [ "chapter" => "Invalid chapter."] );

	$mailChimp = $this->mailChimp;
	$chapterName = $chapterDetails["name"];
	$result = $mailChimp( $chapterName )->members();
	if( $result["isError"]) return $response->withJson( $result, $result["status"], JSON_PRETTY_PRINT );
	return $response->withJson( array_map( function( $member ){
		return \App\MailChimpApi::memberToUser( $member );
	}, $result["result"]["members"]), $result["status"], JSON_PRETTY_PRINT );
});
