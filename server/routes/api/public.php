<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Utils\Perms;
use \Utils\Validator;

$app->get("/public/users/{username}", function (Request $request, Response $response) {
	$username = $request->getAttribute("username");
	$user = $this->users->listByUsername( $username );
	if( empty( $user )) return $this->response->withStatus( 404 );
	if( $user["public"] != 1 ) return $response->withStatus( 403 );
	return $this->response->withJson( $this->users->makePresentable( $user ), 200, JSON_PRETTY_PRINT );
});
