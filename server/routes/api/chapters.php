<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/chapters', function (Request $request, Response $response) {
	$publishMode = $request->getQueryParam('publish', $default = NULL) != NULL;
	$chapters = $this->chapters->findAll();
	forEach( $chapters as $key => $chapter ){
		$chapters[$key]["address"] = json_decode( trim( $chapter["address"], '"' ));
	}
	if( $publishMode ) $this->publisher->publishChapters( $chapters );
	return $response->withJson( $chapters );
});

$app->get('/contacts', function (Request $request, Response $response) {
	$publishMode = $request->getQueryParam('publish', $default = NULL) != NULL;
	$results = $this->chapters->findAllContacts();
	if( $publishMode ) $this->publisher->publishContacts( $results );
	return $response->withJson($results);
});
