<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Firebase\JWT\JWT;

use \Utils\Validator;

$app->post('/confirm', function (Request $request, Response $response) {
	$json = $request->getParsedBody();
	$token = $json["token"];
	if (empty($token)) return $response->withStatus(400)->withHeader("X-Status-Reason", "Token is invalid or not provided.");
	$decoded = JWT::decode($token, $this->settings["jwt"]["email_secret"], array("HS256"));

	$username = $decoded->context->username;
	if (empty($username)) return $response->withStatus(400)->withHeader("X-Status-Reason", "Username in token is invalid.");

	$email = $decoded->context->email;
	if (empty($email)) return $response->withStatus(400)->withHeader("X-Status-Reason", "Email in token is invalid.");

	if( $this->users->userExistsByUsername( $username )) {
		return $response->withStatus(409)->withHeader("X-Status-Reason", "Username $username already exists.");
	}

	$reasons = [];

	$password = $json["password"];
	if( isset( $password )) $password = trim( $password );
	Validator::validPassword( $password, $reasons );

	$firstName = $json["firstName"];
	if( isset( $firstName )) $firstName = trim( $firstName );
	Validator::validFirstName( $firstName, $reasons );

	$lastName = $json["lastName"];
	if( isset( $lastName )) $lastName = trim( $lastName );
	Validator::validLastName( $lastName, $reasons );

	$jobTitle = $json["jobTitle"];
	if( isset( $jobTitle )) {
		$jobTitle = trim($jobTitle);
		Validator::validJobTitle($jobTitle, $reasons);
	} else {
		$jobTitle = null;
	}

	$employer = $json["employer"];
	if( isset( $employer )) {
		$employer = trim($employer);
		Validator::validEmployer($employer, $reasons);
	} else {
		$employer = null;
	}

	$city = $json["city"];
	if( isset( $city )) {
		$city = trim($city);
		Validator::validCity($city, $reasons);
	} else {
		$city = null;
	}

	$province = $json["province"];
	if( isset( $province )) {
		Validator::validProvince($province, $reasons);
		$province = trim($province);
	} else {
		$province = null;
	}

	$postalCode = $json["postalCode"];
	if( isset( $postalCode )) {
		$postalCode = strtoupper(trim($postalCode));
		Validator::validPostalCode($postalCode, $reasons);
	} else {
		$postalCode = null;
	}

	$chapter = $json["chapter"];
	if( isset( $chapter )) $chapter = trim( $chapter );
	Validator::validChapter( $chapter, $reasons );

	foreach( $reasons as $key=>$field ) if( sizeof( $field ) == 0 ) unset( $reasons[$key]);
	if( sizeof( $reasons ) > 0 ) return $response->withStatus( 422 )->withJson( $reasons );

	$salutation = $json["salutation"];
	if( !isset( $salutation ) || !in_array( $salutation, \App\Config::SALUTATIONS )) $salutation = "";

	$background = $json["background"];
	if( !isset( $background )) $background = "";

	$qualifications = $json["qualifications"];
	if( !isset( $qualifications )) $qualifications = "";

	$optIn = $json["optIn"];
	if( !isset( $optIn )) $optIn = 1;

	$winGlobal = $json["winGlobal"];
	if( !isset( $winGlobal )) $winGlobal = 0;

	$userId = $this->users->createUser( $email, $username, $password, $chapter, $winGlobal, $salutation,
		$firstName, $lastName, $jobTitle, $employer, $city, $province, $postalCode, $background,
		$qualifications, $optIn );

	$this->logger->info( "Created user with id $userId and $username ");

	$user = $this->users->listByUsername( $username );
	$this->logger->debug( json_encode( $user, JSON_PRETTY_PRINT ));

	if( $optIn == 1 ){
		$chapterDetails = $this->chapters->getDetails( "Canada" );
		if( !isset( $chapterDetails)) return $response->withStatus( 422 )->withJson( [ "chapter" => "Invalid chapter."] );

		$mailChimp = $this->mailChimp;
		$mailChimp( "Canada" )->addOrUpdateMember( $user );

		if( $chapter !== "Canada" ){
			$chapterDetails = $this->chapters->getDetails( $chapter );
			if( !isset( $chapterDetails)) return $response->withStatus( 422 )->withJson( [ "chapter" => "Invalid chapter."] );
			$mailChimp( $chapterDetails["name"] ) ->addOrUpdateMember( $user );
		}
	}

	$result = $this->users->listAllPublic();
	$this->logger->warning('publishing to '. $this->settings['publishFolder'] . 'profiles.json');
	$fp = fopen($this->settings['publishFolder'] . 'profiles.json', 'w');
	fwrite($fp, json_encode($result, JSON_PRETTY_PRINT));
	fclose($fp);

	return $this->response->withJson( $user );
});
