<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Utils\Perms;
use Utils\Validator;

$app->post('/conference', function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$json = $request->getParsedBody();

	$reasons = array();

	$title = $json["title"];
	if( isset( $title )) $title = trim( $title );
	Validator::validTitle( $title, $reasons );

	$description = $json["description"];
	if( isset( $description )) $description = trim( $description );
	Validator::validDescription( $description, $reasons );

	$lede = $json["lede"];
	if( isset( $lede )) $lede = trim( $lede );
	Validator::validLede( $lede, $reasons );

	$content = $json["content"];
	if( isset( $content )) $content = trim( $content );
	Validator::validContent( $content, $reasons );

	foreach( $reasons as $key=>$field ) if( sizeof( $field ) == 0 ) unset( $reasons[$key]);
	if( sizeof( $reasons ) > 0 ) return $response->withStatus( 422 )->withJson( $reasons );

	$this->publisher->publishConference( $json );
	return $response->withStatus( 201 );
});

$app->post("/conference/background", function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$key = urldecode( $request->getAttribute('key'));

	$fileName = $_FILES["file"]["name"];
	$fileSize = $_FILES["file"]["size"];
	$fileTmpName = $_FILES["file"]["tmp_name"];
	$fileExtensions = ["jpeg","jpg"];
	$fileExtension = strtolower(end(explode(".", $fileName)));

	$this->logger->info( "File: $fileName $fileTmpName $fileSize" );

	$reasons = [];
	if( !in_array( $fileExtension, $fileExtensions)) array_push( $reasons, "Only jpeg, jpg image types are accepted.");
	if( $fileSize > 2000000 ) array_push( $reasons, "Maximum acceptable image file size is 2Mb." );

	list($width, $height, $type, $attr) = getimagesize($fileTmpName);
	if( $width < 2000 ) array_push( $reasons, "The conference background must be a minimum of 2000px wide." );
	if( $height < 900 ) array_push( $reasons, "The conference background must be a minimum of 900px tall." );

	if( !empty($reasons)) return $response->withStatus( 422 )->withJson( $reasons );

	$result = $this->cloudinary->upload( "conference", $key, $fileTmpName, function( $url ) use ( $key ){
		$this->publisher->publishConferenceBackground( $url );
	} );

	$conference = $this->publisher->loadConference();
	$conference->background = $result["secure_url"];
	$this->publisher->publishConference( $conference );

	return $this->response->withJson( $result, 200, JSON_PRETTY_PRINT );
});

$app->post("/conference/sponsors", function (Request $request, Response $response) {
	if (!Perms::tokenIsSuper($this->token)) return $response->withStatus(403);
	$key = urldecode( $request->getAttribute('key'));

	$fileName = $_FILES["file"]["name"];
	$fileSize = $_FILES["file"]["size"];
	$fileTmpName = $_FILES["file"]["tmp_name"];
	$fileExtensions = ["png"];
	$fileExtension = strtolower(end(explode(".", $fileName)));

	$this->logger->info( "File: $fileName $fileTmpName $fileSize" );

	$reasons = [];
	if( !in_array( $fileExtension, $fileExtensions)) array_push( $reasons, "Only the png image type is accepted.");
	if( $fileSize > 2000000 ) array_push( $reasons, "Maximum acceptable image file size is 2Mb." );
	list($width, $height, $type, $attr) = getimagesize($fileTmpName);
	if( $width < 900 ) array_push( $reasons, "The sponsors image should be a minimum of 900px wide." );

	if( !empty($reasons)) return $response->withStatus( 422 )->withJson( $reasons );

	$result = $this->cloudinary->upload( "sponsors", $key, $fileTmpName, function( $url ) use ( $key ){
		$this->publisher->publishConferenceSponsors( $url );
	} );

	$conference = $this->publisher->loadConference();
	$conference->sponsors = $result["secure_url"];
	$this->publisher->publishConference( $conference );

	return $this->response->withJson( $result, 200, JSON_PRETTY_PRINT );
});
