<?php
require __DIR__ . "/vendor/autoload.php";
require "lib/Slim/Handlers/ApiError.php";

require __DIR__ . "/../_do_not_commit/Config.php";

use \App\Config as Config;

$app = new \Slim\App([
    "settings" => [
		'determineRouteBeforeAppMiddleware' => true,
		'displayErrorDetails' => true,
		'addContentLengthHeader' => false,
		"cloudinaryUrl" => Config::CLOUDINARY_URL,
    	"tmpFolder" => __DIR__ . "/" . Config::TMP_FOLDER,
    	"publishFolder" => __DIR__ . "/" . Config::PUBLISHED_FOLDER,
		"staticFolder" => __DIR__ . "/" . Config::STATIC_FOLDER,
        "displayErrorDetails" => true,
        "db" => [
            "path" => __DIR__ . "/" . Config::DATABASE
        ],
        "logs" => [
            "path" => __DIR__ . "/" . Config::LOG_FOLDER
        ],
		"jwt" => [
			"tokenRefreshRate" => 5 * 60,
			"email_secret" => Config::JWT_EMAIL_SECRET,
			"secret" => Config::JWT_SECRET,
			"secure" => false,
			"path" => ["/"],
			"passthrough" => ["/login", "/join", "/confirm", "/password", "/mc/callback", "/geo", "/public"]
		]
    ]
]);

require __DIR__ . "/config/logger.php";
require __DIR__ . "/config/handlers.php";
require __DIR__ . "/config/authentication.php";
require __DIR__ . "/config/database.php";
require __DIR__ . "/config/publisher.php";

require __DIR__ . "/routes/index.php";
require __DIR__ . "/routes/api/chapters.php";
require __DIR__ . "/routes/api/conference.php";
require __DIR__ . "/routes/api/confirm.php";
require __DIR__ . "/routes/api/events.php";
require __DIR__ . "/routes/api/geo.php";
require __DIR__ . "/routes/api/join.php";
require __DIR__ . "/routes/api/login.php";
require __DIR__ . "/routes/api/mailchimp.php";
require __DIR__ . "/routes/api/news.php";
require __DIR__ . "/routes/api/password.php";
require __DIR__ . "/routes/api/public.php";
require __DIR__ . "/routes/api/users.php";

$app->run();
