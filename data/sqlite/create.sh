#!/usr/bin/env bash
rm -f ../../_db/win.db
echo "" > ../../_db/win.db
echo chapters
cat chapters.sql | sqlite3 ../../_db/win.db
echo jobs
cat jobs.sql | sqlite3 ../../_db/win.db
echo site_roles
cat site_roles.sql | sqlite3 ../../_db/win.db
echo status
cat status.sql | sqlite3 ../../_db/win.db
echo user_roles
cat user_roles.sql | sqlite3 ../../_db/win.db
echo users
cat users.sql | sqlite3 ../../_db/win.db
echo news
cat news.sql | sqlite3 ../../_db/win.db
echo events
cat events.sql | sqlite3 ../../_db/win.db
