DROP TABLE IF EXISTS EVENTS;

CREATE TABLE EVENTS (
	ID INTEGER PRIMARY KEY AUTOINCREMENT,
	TITLE TEXT NOT NULL,
	CHAPTER_ID INTEGER NOT NULL,
	DESCRIPTION TEXT,
	CONTENT TEXT,
	LOCATION TEXT,
	START_DATE DATE NOT NULL,
	END_DATE DATE NOT NULL,
	CREATE_DATE DATE DEFAULT CURRENT_DATE,
	UPDATE_DATE DATE DEFAULT CURRENT_DATE,
	FOREIGN KEY( CHAPTER_ID ) REFERENCES CHAPTERS( ID )
);

CREATE TRIGGER UPDATE_EVENTS_LASTMODIFIED
BEFORE UPDATE ON EVENTS
BEGIN
	UPDATE EVENTS
	SET UPDATE_DATE = CURRENT_DATE
	WHERE ID = old.ID;
END;
