DROP TABLE IF EXISTS STATUS;
CREATE TABLE STATUS(
	ID INTEGER PRIMARY KEY NOT NULL,
	STATUS TEXT NOT NULL COLLATE NOCASE,
	CONSTRAINT STATUS_UNIQUE UNIQUE ( STATUS )
);

-- before removing a user, they should be deactivated
INSERT INTO STATUS VALUES ( -1, 'DEACTIVATED' );
-- pending state happens AFTER a user email has been confirmed
INSERT INTO STATUS VALUES ( 0, 'PENDING' );
-- activated is a normal state
INSERT INTO STATUS VALUES ( 1, 'ACTIVATED' );
-- immediately after joining, a user has an unconfirmed email address
INSERT INTO STATUS VALUES ( 2, 'UNCONFIRMED' );
-- on import some users did not have unique emails or usernames!
--- so I import them with username_xxx and email_xxx
INSERT INTO STATUS VALUES ( 3, 'DUPLICATE' );
