const fs = require( "fs-extra" );
const path = require( "path" );
const config = require( "../../_do_not_commit/config");
const sqlite3 = require('sqlite3').verbose();
const sanitize = require( "sanitize-html" );
const glob = require( "glob" );

const db = new sqlite3.Database( `../../_db/${ config.sqliteDatabase }.db` );
const imageSrc = "../../../justhost/public_html/large";
const dest = "../../site/static/profiles/";

db.each( "SELECT USERS.USERNAME as username, PROFILES.IMAGE as image FROM USERS, PROFILES WHERE PROFILES.USER_ID=USERS.ID", ( err, row ) => {
	console.log( row.username, row.image );
	try {
		fs.copySync(path.resolve( imageSrc, row.image ), path.resolve( `${ dest }/${ row.username.trim().toLowerCase()}`, "profile.jpg" ));
	} catch( err ){
		console.log( err );
	}
});
