const sanitizeHtml = require( "sanitize-html" );
const toMarkdown = require( "to-markdown" );

const markdownOptions = { gfm: true };
const sanitizeOptions = {
	transformTags: {
		"div" : "p"
	}
};

const sanitize = content => {
	const result = toMarkdown( sanitizeHtml( content, sanitizeOptions ), markdownOptions );
	if( result === null || result === "null" ) return undefined;
	return result;
};

const sanitizeToHtml = content => {
	const result = sanitizeHtml( content, sanitizeOptions ).replace( /â€™/g, "'" );
	if( result === null || result === "null" ) return undefined;
	return result.split( /\n\n/g ).map( l => `<p>${ l }</p>` ).join( "\n" );
};

module.exports = {
	sanitize,
	sanitizeToHtml
};
