const fs = require( "fs" );
const mysql = require( "mysql" );
const config = require( "../../_do_not_commit/config");
const sqlite3 = require('sqlite3').verbose();
const sanitize = require( "./sanitize" );

const connection = mysql.createConnection({
	host: config.mysqlHost,
	user: config.mysqlUser,
	password: config.mysqlPassword,
	database: config.mysqlDatabase
});

connection.connect();

const today = new Date();
const date = `${ today.getFullYear() }${ today.getMonth()+1 < 10 ? "0"+today.getMonth()+1 : today.getMonth()+1 }${ today.getDate() < 10 ? "0"+today.getDate() : today.getDate()}`;

var db = new sqlite3.Database( `../../_db/${ config.sqliteDatabase }.db` );

connection.query( "select * from win_women", function( err, results ){
	if( err ) throw err;

	var stmt = db.prepare(
		"INSERT INTO PROFILES( ID, USER_ID, JOB_ID, INTRO, BACKGROUND, IMAGE, UPDATE_DATE, CREATE_DATE, FEATURED ) " +
		"VALUES (?,?,?,?,?,?,DATE( ?/1000, 'unixepoch', 'localtime' ),DATE( ?/1000, 'unixepoch', 'localtime' ),?)");

	results.forEach( row => {
		stmt.run( row.id, row.woman_id, row.job_category, sanitize( row.intro ), sanitize( row.background ),
			row.imagelarge, row.update_date, row.create_date, 0 );
	});
});

connection.end();
