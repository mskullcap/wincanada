const fs = require( "fs-extra" );
const path = require( "path" );
const config = require( "../../_do_not_commit/config");
const sqlite3 = require('sqlite3').verbose();
const sanitize = require( "sanitize-html" );
const glob = require( "glob" );

const db = new sqlite3.Database( `../../_db/${ config.sqliteDatabase }.db` );
const imageSrc = "../../private/win";
const dest = "../../site/static/profiles/";

// After running the code in facedetect, you will get a zip archive of the profiles pics.
// This script takes those images and puts the in their appropriate profile folder.

db.each( "SELECT USERS.USERNAME as username, PROFILES.IMAGE as image FROM USERS, PROFILES WHERE PROFILES.USER_ID=USERS.ID", ( err, row ) => {
	console.log( row.username, row.image );
	try {
		const g = imageSrc + "/" + row.image.substring( 0, row.image.length - 4 ) + "*";
		glob( g, null, ( err, files ) => {
			if( files.length > 0 ){
				const file = files[0];
				fs.copySync( file, path.resolve( `${ dest }/${ row.username.trim().toLowerCase()}`, "thumbnail.jpg" ));
			}
		});
	} catch( err ){
		console.log( err );
	}
});
