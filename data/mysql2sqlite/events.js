const fs = require( "fs" );
const mysql = require( "mysql" );
const config = require( "../../_do_not_commit/config");
const sqlite3 = require('sqlite3').verbose();
const sanitize = require( "./sanitize" );

const connection = mysql.createConnection({
	host: config.mysqlHost,
	user: config.mysqlUser,
	password: config.mysqlPassword,
	database: config.mysqlDatabase
});

connection.connect();

const today = new Date();
const date = `${ today.getFullYear() }${ today.getMonth()+1 < 10 ? "0"+today.getMonth()+1 : today.getMonth()+1 }${ today.getDate() < 10 ? "0"+today.getDate() : today.getDate()}`;

var db = new sqlite3.Database( `../../_db/${ config.sqliteDatabase }.db` );

connection.query( "select * from events", function( err, results ){
	if( err ) throw err;

	var stmt = db.prepare(
		"INSERT INTO EVENTS( ID, CHAPTER_ID, TITLE, DESCRIPTION, CONTENT, LOCATION, START_DATE, END_DATE ) " +
		"VALUES (?,?,?,?,?,?,DATE( ?/1000, 'unixepoch', 'localtime' ),DATE( ?/1000, 'unixepoch', 'localtime' ))");

	results.forEach( row => {
		const w = [];

		if( row.venue && row.venue.trim().length > 0 ) w.push( (row.venue + " " + row.room).trim());
		if( row.city && row.city.trim().length > 0 ) w.push( row.city.trim() );
		if( row.province && row.province.trim().length > 0 ) w.push( row.province.trim() );
		if( row.country && row.country.trim().length > 0 ) w.push( row.country.trim() );

		stmt.run( row.id, row.category, row.title, row.description, sanitize.sanitizeToHtml( row.content ), w.join( ", " ), row.eventstartdate, row.eventenddate );
	});
});

connection.end();
