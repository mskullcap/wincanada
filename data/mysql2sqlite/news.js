const fs = require( "fs" );
const mysql = require( "mysql" );
const config = require( "../../_do_not_commit/config");
const sqlite3 = require('sqlite3').verbose();
const sanitize = require( "./sanitize" );

const connection = mysql.createConnection({
	host: config.mysqlHost,
	user: config.mysqlUser,
	password: config.mysqlPassword,
	database: config.mysqlDatabase
});

connection.connect();

const today = new Date();
const date = `${ today.getFullYear() }${ today.getMonth()+1 < 10 ? "0"+today.getMonth()+1 : today.getMonth()+1 }${ today.getDate() < 10 ? "0"+today.getDate() : today.getDate()}`;

var db = new sqlite3.Database( `../../_db/${ config.sqliteDatabase }.db` );

connection.query( "select * from news where removaldate > now()", function( err, results ){
	if( err ) throw err;

	// DATE( NEWS.CREATE_DATE/1000, 'unixepoch', 'localtime'))
	var stmt = db.prepare(
		"INSERT INTO NEWS( ID, TITLE, DESCRIPTION, CONTENT, PUBLISHED_ON, CREATE_DATE, REMOVE_BY ) " +
		"VALUES (?,?,?,?, DATE( ?/1000, 'unixepoch', 'localtime' ), DATE( ?/1000, 'unixepoch', 'localtime' ),  DATE( ?/1000, 'unixepoch', 'localtime' ))");

	results.forEach( row => {
		stmt.run( row.id, row.title, row.description, sanitize.sanitizeToHtml( row.content ), row.publishdate, row.publishdate, row.removaldate );
	});
});

connection.end();
