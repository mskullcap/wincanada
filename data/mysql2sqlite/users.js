const fs = require( "fs" );
const mysql = require( "mysql" );
const config = require( "../../_do_not_commit/config");
const sqlite3 = require('sqlite3').verbose();
const sanitize = require( "./sanitize" );

const mysqlQuery = `
	SELECT
	  win_members.id as id,
	  chapter_id as chapterId,
	  is_win_global_member as winGlobal,
	  salutation as salutation,
	  lcase( trim( username )) as username,
	  password as password,
	  salt as salt,
	  trim( firstname ) as firstName,
	  trim( lastname ) as lastName,
	  job_title as jobTitle,
	  employer as employer,
	  address_1 as address1,
	  address_2 as address2,
	  city as city,
	  lcase( trim( province )) as province,
	  postal_code as postalCode,
	  lcase( trim( email )) as email,
	  win_members.background as background,
	  prof_qualifications as qualifications,
	  win_members.create_date as createDate,
	  win_members.update_date as updateDate,
	  win_members.status as status,
	  pending as pending,
	  job_category as jobId,
	  1 as optIn,
	  0 as public,
	  intro as bioIntro,
	  win_women.background as bioBackground,
	  imagelarge as bioImage,
	  featured as bioFeatured
	FROM
	  win_members
	LEFT JOIN win_women ON win_women.woman_id = win_members.id
	ORDER BY win_members.id DESC;
`;

const userInsert = `
	INSERT INTO USERS(
		ID, 
		EMAIL, 
		USERNAME, 
		PASSWORD, 
		SALT, 
		STATUS_ID, 
		CHAPTER_ID, 
		CREATE_DATE, 
		UPDATE_DATE,
		IS_WIN_GLOBAL_MEMBER, 
		SALUTATION, 
		FIRSTNAME, 
		LASTNAME, 
		JOB_TITLE, 
		EMPLOYER,
		ADDRESS_1, 
		ADDRESS_2, 
		CITY, 
		PROVINCE, 
		POSTAL_CODE, 
		BACKGROUND, 
		PROF_QUALIFICATIONS,
		JOB_ID,
		OPT_IN,
		PUBLIC,
		BIO_INTRO,
		BIO_BACKGROUND,
		BIO_IMAGE,
		BIO_FEATURED		
	)
	VALUES (
		?, -- ID
		?, -- EMAIL
		?, -- USERNAME
		?, -- PASSWORD
		?, -- SALT
		?, -- STATUS_ID
		?, -- CHAPTER_ID
		DATE( ?/1000, 'unixepoch', 'localtime' ), -- CREATE_DATE
		DATE( ?/1000, 'unixepoch', 'localtime' ), -- UPDATE_DATE
		?, -- IS_WIN_GLOBAL_MEMBER
		?, -- SALUTATION
		?, -- FIRSTNAME
		?, -- LASTNAME
		?, -- JOB_TITLE
		?, -- EMPLOYER
		?, -- ADDRESS_1
		?, -- ADDRESS_2
		?, -- CITY
		?, -- PROVINCE
		?, -- POSTAL_CODE
		?, -- BACKGROUND
		?, -- PROF_QUALIFICATIONS
		?, -- JOB_ID
		?, -- OPT_IN,
		?, -- PUBLIC,
		?, -- BIO_INTRO,
		?, -- BIO_BACKGROUND,
		?, -- BIO_IMAGE,
		?  -- BIO_FEATURED
	)
`;

const roleInsert = "INSERT INTO USER_ROLES( USER_ID, ROLE_ID ) VALUES ( ?, ? )";

const sanitizeProvince = p => {
	if( !p ) return "";

	if( p.indexOf( "on" ) === 0 ) return "ON";
	if( p.indexOf( "ma" ) === 0 ) return "MB";
	if( p.indexOf( "mb" ) === 0 ) return "MB";
	if( p.indexOf( "sa" ) === 0 ) return "SK";
	if( p.indexOf( "qu" ) === 0 ) return "QC";
	if( p.indexOf( "qc" ) === 0 ) return "QC";
	if( p.indexOf( "bc" ) === 0 ) return "BC";
	if( p.indexOf( "british" ) === 0 ) return "BC";
	if( p.indexOf( "qu" ) === 0 ) return "QC";
	if( p.indexOf( "alb" ) === 0 ) return "AB";
	if( p.indexOf( "ab" ) === 0 ) return "AB";
	if( p.indexOf( "yu" ) === 0 ) return "YT";
	if( p.indexOf( "yk" ) === 0 ) return "YT";
	if( p.indexOf( "nb" ) === 0 ) return "NB";
	if( p.indexOf( "new " ) === 0 ) return "NB";
	if( p.indexOf( "ns" ) === 0 ) return "NS";
	if( p.indexOf( "nova" ) === 0 ) return "NS";
	if( p.indexOf( "nfl" ) === 0 ) return "NL";
	if( p.indexOf( "newf" ) === 0 ) return "NL";
	return "";
};

const connection = mysql.createConnection({
	host: config.mysqlHost,
	user: config.mysqlUser,
	password: config.mysqlPassword,
	database: config.mysqlDatabase
});

connection.connect();

const today = new Date();
const date = `${ today.getFullYear() }${ today.getMonth()+1 < 10 ? "0"+today.getMonth()+1 : today.getMonth()+1 }${ today.getDate() < 10 ? "0"+today.getDate() : today.getDate()}`;

const db = new sqlite3.Database( `../../_db/${ config.sqliteDatabase }.db` );

const presidentUsername = "klebh";
const adminUsernames = ["mskullcap", "carlysilberstein", "klebh"];
const leadEmails = ["sonia.qureshi@candu.org", "gclark@nbpower.com", "lisa.marshall@opg.com", "heather.convay@opg.com", "tracy.primeau@brucepower.com", "mhawkes@nbpower.com", "larkin.kee@cnl.ca", "anne_gent@cameco.com"];
const superUsernames = ["mskullcap", "carlysilberstein", "klebh"];
const secretaryUsername = "mosscropl";
const treasurerUsername = "weishark";
const membersAtLargeUsernames = ["Laurie Fraser", "mosscropl", "lmachan", "amanda.rivers", "marinaoey", "pauline.watson" ];
const pastPresidentsUsernames = [ "colleen.sidford", "susan.brissette" ];
const salutations = ["mr", "mrs", "dr", "mme", "mlle", "ms", "miss", "prof"];

const processResults = ( results ) => {
	const stmt = db.prepare( userInsert );
	const rolestmt = db.prepare( roleInsert );

	results.forEach( row => {
		const winGlobalMember = row.winGlobal.indexOf("Yes") > -1 ? 1 : 0;

		let salutation = row.salutation;
		if( salutation !== null && salutation.length > 0 ){
			if( !salutations.find( s => salutation.toLowerCase().indexOf( s ) === 0 )) salutation = "";
		}
		row.email = row.email.toLowerCase().trim().replace( /\s/g, "" );
		row.username = row.username.trim();

		row.qualifications = row.qualifications.toLowerCase() === "placeholder" ? "" : row.qualifications;
		row.background = row.background;
		row.bioIntro = row.bioIntro;
		row.bioBackground = sanitize.sanitizeToHtml( row.bioBackground );

		row.winGlobalMember = winGlobalMember;
		row.salutation = salutation;
		row.province = sanitizeProvince( row.province );

		// ok, so deactivated members are probably better thought of as
		// unsubscribed members
		if( row.pending ){ // pending
			row.statusId = 0;
			row.optIn = 0;
			row.public = 0;
		} else if( row.status.indexOf( "deactivated" ) > -1 ){ // deactivated
			row.statusId = 1; // set as activated, just opt out
			row.optIn = 0;
			if( row.bioBackground ) row.public = 1;
			else row.public = 0;
		} else { // ok, now for the truly activated members
			row.statusId = 1;
			row.optIn = 1;
			if( row.bioBackground ) row.public = 1;
			else row.public = 0;
		}

		stmt.run(
			row.id,
			row.email,
			row.username,
			row.password,
			row.salt,
			row.statusId,
			row.chapterId,
			row.createDate,
			row.updateDate,
			winGlobalMember,
			row.salutation,
			row.firstName,
			row.lastName,
			row.jobTitle,
			row.employer,
			row.address1,
			row.address2,
			row.city,
			row.province,
			row.postalCode,
			row.background === "null" ? undefined : row.background,
			row.qualifications,
			row.jobId === null ? 1000 : row.jobId,
			row.optIn,
			row.public,
			row.bioIntro === "null" ? undefined : row.bioIntro,
			row.bioBackground === "null" ? undefined : row.bioBackground,
			row.bioImage,
			row.bioFeatured === "yes" ? 1 : 0
			, err => {
				if( !err ) return;

				if( err.errno === 19 ){
					const badField = err.toString().replace( /^.*: USERS\.(.*)$/, "$1" );
					if( badField ){
						console.log( badField + " not unique for id: " + row.id + ", ", row.firstName, row.lastName, "email: " + row.email, "username: " + row.username );
						// row.email = row.email + "_xxx";
						// row.username = row.username + "_xxx";
						// row.statusId = 3; // duplicate entry, try again
						// stmt.run( row.id, row.email, row.username, row.password, row.salt, row.statusId, row.chapter_id, row.create_date, row.update_date,
						// 	row.winGlobalMember, row.salutation, row.firstname, row.lastname, row.job_title, row.employer,
						// 	row.address_1, row.address_2, row.city, row.province, row.postal_code, row.phone, row.background, row.prof_qualifications );
					}
				}
			}
		);

		rolestmt.run( row.id, 0 );
		if( leadEmails.find( e => e === row.email )) rolestmt.run( row.id, 1 );
		if( treasurerUsername === row.username ) rolestmt.run( row.id, 3 );
		if( membersAtLargeUsernames.find( e => e === row.username )) rolestmt.run( row.id, 4 );
		if( secretaryUsername === row.username ) rolestmt.run( row.id, 5 );
		if( pastPresidentsUsernames.find( e => e === row.username )) rolestmt.run( row.id, 6 );
		if( presidentUsername === row.username ) rolestmt.run( row.id, 100 );
		if( adminUsernames.find( e => e === row.username )) rolestmt.run( row.id, 999 );
		if( superUsernames.find( e => e === row.username )) rolestmt.run( row.id, 200 );
	});
};


connection.query( mysqlQuery, function( err, results ){
	if( err ) throw err;

	processResults( results );
});

connection.end();
