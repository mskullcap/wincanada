CREATE TABLE about
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    about LONGTEXT
);
CREATE TABLE blog
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title LONGTEXT,
    postedby INT(10),
    posteddate DATE,
    content LONGTEXT
);
CREATE TABLE boardmembers
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    boardmembers LONGTEXT
);
CREATE TABLE chapters
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    chapter VARCHAR(255) NOT NULL,
    description LONGTEXT NOT NULL,
    contactname VARCHAR(255) NOT NULL,
    contactemail VARCHAR(255) NOT NULL
);
CREATE TABLE contact
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    contact LONGTEXT
);
CREATE TABLE countries
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    country_code VARCHAR(2) DEFAULT '' NOT NULL,
    country_name VARCHAR(100) DEFAULT '' NOT NULL
);
CREATE TABLE event_files
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    event_id INT(11) NOT NULL,
    file_name VARCHAR(255) NOT NULL,
    file_path VARCHAR(500) NOT NULL,
    status INT(11) NOT NULL COMMENT '0=>Inactive 1=>Active'
);
CREATE TABLE event_links
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    event_id INT(11) NOT NULL,
    link_name VARCHAR(500) NOT NULL,
    link VARCHAR(500) NOT NULL
);
CREATE TABLE events
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    category LONGTEXT NOT NULL,
    title LONGTEXT NOT NULL,
    description LONGTEXT NOT NULL,
    content LONGTEXT NOT NULL,
    eventimage LONGTEXT NOT NULL,
    venue LONGTEXT NOT NULL,
    room LONGTEXT NOT NULL,
    address LONGTEXT NOT NULL,
    city LONGTEXT NOT NULL,
    province LONGTEXT NOT NULL,
    postal LONGTEXT NOT NULL,
    country LONGTEXT NOT NULL,
    publishdate DATE NOT NULL,
    removaldate DATE NOT NULL,
    eventstartdate DATE NOT NULL,
    eventenddate DATE NOT NULL,
    gallery INT(11) NOT NULL,
    list_order INT(11) NOT NULL
);
CREATE TABLE gallery
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL
);
CREATE TABLE gallery_files
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    gallery_id INT(11) NOT NULL,
    file_path VARCHAR(100) NOT NULL,
    file_thumb_path VARCHAR(255) NOT NULL,
    image_code VARCHAR(255) NOT NULL
);
CREATE TABLE job_category
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    job VARCHAR(255)
);
CREATE TABLE joinwin
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    joinwin LONGTEXT
);
CREATE TABLE linkheadings
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title LONGTEXT,
    list_order INT(11) NOT NULL
);
CREATE TABLE links
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    linkcategory LONGTEXT,
    title LONGTEXT,
    url LONGTEXT,
    list_order INT(11) NOT NULL
);
CREATE TABLE login
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    firstname VARCHAR(255) NOT NULL,
    lastname VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    created_at DATETIME NOT NULL,
    modified_at DATETIME NOT NULL,
    superuser INT(11) NOT NULL
);
CREATE TABLE member
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    firstname VARCHAR(255) NOT NULL,
    lastname VARCHAR(255) NOT NULL,
    email VARCHAR(50) NOT NULL,
    username VARCHAR(30) NOT NULL,
    password CHAR(128) NOT NULL,
    salt CHAR(128) NOT NULL,
    created_at DATETIME NOT NULL,
    superuser INT(11) NOT NULL
);
CREATE TABLE news
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    category LONGTEXT NOT NULL,
    title LONGTEXT NOT NULL,
    description LONGTEXT NOT NULL,
    content LONGTEXT NOT NULL,
    publishdate DATE NOT NULL,
    removaldate DATE NOT NULL,
    gallery INT(11) NOT NULL,
    list_order INT(11) NOT NULL
);
CREATE TABLE newscategory
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title LONGTEXT NOT NULL
);
CREATE TABLE president
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    president LONGTEXT
);
CREATE TABLE privacy
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    privacy LONGTEXT
);
CREATE TABLE sitemap
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    sitemap LONGTEXT
);
CREATE TABLE t_user
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    chapter_id INT(11),
    forum_id INT(11),
    is_win_global_member INT(11),
    title VARCHAR(25),
    username VARCHAR(40),
    password VARCHAR(128),
    salt VARCHAR(128),
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    job_title VARCHAR(100),
    employer VARCHAR(100),
    address_1 VARCHAR(100),
    address_2 VARCHAR(100),
    city VARCHAR(50),
    province VARCHAR(50),
    postal_code VARCHAR(20),
    phone VARCHAR(50),
    fax VARCHAR(50),
    email VARCHAR(100),
    background TEXT,
    prof_qualifications TEXT,
    updates VARCHAR(100),
    is_admin INT(11),
    last_login DATETIME,
    status INT(1),
    created_at DATETIME,
    updated_at DATETIME,
    created_by INT(11)
);
CREATE UNIQUE INDEX t_user_email_unique ON t_user (email);
CREATE INDEX t_user_FI_1 ON t_user (chapter_id);
CREATE INDEX t_user_FI_2 ON t_user (forum_id);
CREATE UNIQUE INDEX t_user_username_unique ON t_user (username);
CREATE TABLE users
(
    uid INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    username VARCHAR(255),
    password VARCHAR(100),
    email VARCHAR(255),
    profile_image VARCHAR(200),
    profile_image_small VARCHAR(200)
);
CREATE UNIQUE INDEX email ON users (email);
CREATE UNIQUE INDEX username ON users (username);
CREATE TABLE win_members
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    chapter_id INT(11),
    is_win_global_member VARCHAR(255),
    salutation VARCHAR(25),
    username VARCHAR(30) NOT NULL,
    password CHAR(128) NOT NULL,
    salt CHAR(128) NOT NULL,
    firstname VARCHAR(255) NOT NULL,
    lastname VARCHAR(255) NOT NULL,
    job_title VARCHAR(255) NOT NULL,
    employer VARCHAR(255) NOT NULL,
    address_1 VARCHAR(255) NOT NULL,
    address_2 VARCHAR(255) NOT NULL,
    city VARCHAR(255) NOT NULL,
    province VARCHAR(255) NOT NULL,
    postal_code VARCHAR(255) NOT NULL,
    phone VARCHAR(255) NOT NULL,
    fax VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    background LONGTEXT,
    prof_qualifications LONGTEXT,
    create_date DATETIME,
    update_date DATETIME,
    status VARCHAR(255) NOT NULL,
    pending INT(11) NOT NULL
);
CREATE TABLE win_women
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    woman_id INT(11),
    job_category INT(11),
    intro LONGTEXT,
    background LONGTEXT,
    imagelarge VARCHAR(255) NOT NULL,
    update_date DATETIME,
    create_date DATETIME NOT NULL,
    status VARCHAR(255) NOT NULL,
    featured VARCHAR(255) NOT NULL,
    sidebar VARCHAR(255) NOT NULL
);
