#!/usr/bin/env bash
cd /home/win/server/canada.womeninnuclear.org/scripts
config_file="../_do_not_commit/Config.php"

dropbox_token=`cat $config_file | grep DROPBOX_APP_ACCESS_TOKEN | sed 's/^.*"\(.*\)".*$/\1/'`
openssl_password=`cat $config_file | grep OPENSSL_BACKUP_PASSWORD | sed 's/^.*"\(.*\)".*$/\1/'`

echo Loaded dropbox token from $config_file;
echo Loaded openssl password from $config_file;

echo Tarring canada.womeninnuclear.org...
tar cfz canada.womeninnuclear.org.tgz ../_db/* ../site/static/profiles/* ../site/static/news/*

echo Encrypting...
openssl enc -aes-256-cbc -salt -in canada.womeninnuclear.org.tgz -out canada.womeninnuclear.org.tgz.enc -k $openssl_password

echo Uploading to dropbox...
curl -X POST https://content.dropboxapi.com/2/files/upload \
  --header "Authorization: Bearer $dropbox_token" \
  --header 'Content-Type: application/octet-stream' \
  --header 'Dropbox-API-Arg: {"path":"/canada.womeninnuclear.org.tgz.enc","mode":{".tag":"overwrite"}}' \
  --data-binary @'canada.womeninnuclear.org.tgz.enc'

echo Complete.  See comments in this file to learn how to decrypt

#
# To decrypt:
# openssl enc -aes-256-cbc -d -salt -in canada.womeninnuclear.org.tgz.enc -out 2017-10-02_wincanada.tgz -k <password>
#
