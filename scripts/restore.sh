#!/usr/bin/env bash

config_file="../_do_not_commit/Config.php"

dropbox_token=`cat $config_file | grep DROPBOX_APP_ACCESS_TOKEN | sed 's/^.*"\(.*\)".*$/\1/'`
openssl_password=`cat $config_file | grep OPENSSL_BACKUP_PASSWORD | sed 's/^.*"\(.*\)".*$/\1/'`

echo Loaded dropbox token from $config_file;
echo Loaded openssl password from $config_file;
echo Decrypting canada.womeninnuclear.org

openssl enc -aes-256-cbc -d -salt -in canada.womeninnuclear.org.tgz.enc -out canada.womeninnuclear.org.tgz -k $openssl_password

echo Untarring canada.womeninnuclear.org...
tar xfz canada.womeninnuclear.org.tgz

echo
echo Complete.  See comments in this file to learn how to decrypt
