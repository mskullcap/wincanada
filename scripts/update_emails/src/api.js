const request = require( "request-promise-native" );
const mailchimp = require( "mailchimp-api-v3" );
const config = require( "../../../_do_not_commit/config.json");
const md5 = require('md5');

const url = "https://canada.womeninnuclear.org";

//
// public static function memberToUser( $member ){
//     return [
//         "username" => $member["merge_fields"]["USERNAME"],
//         "chapter" => $member["merge_fields"]["CHAPTER"],
//         "firstName" => $member["merge_fields"]["FNAME"],
//         "lastName" => $member["merge_fields"]["LNAME"],
//         "email" => $member["email_address"],
//         "status" => $member["status"]
// ];
// }
//
// function addOrUpdateMember( $user, $listId = NULL ){
//     if( !isset( $listId )) $listId = $this->listId;
//     $hashEmail = $this->emailHashFromUser( $user );
//     return $this->api->connect( "lists/" . $listId . "/members/" . $hashEmail, "PUT", [
//         "email_address" => $user["email"],
//         "status_if_new" => $user["optIn"] == 1 ? "subscribed" : "unsubscribed",
//         "status" => $user["optIn"] == 1 ? "subscribed" : "unsubscribed",
//         "merge_fields" => [
//         "USERNAME" => $user["username"],
//         "FNAME" => $user["firstName"],
//         "LNAME" => $user["lastName"],
//         "EMAIL" => $user["email"],
//         "CHAPTER" => $user["chapter"]
// ]
// ]);
// }
//
// function deleteMember( $user, $listId = NULL ){
//     if( !isset( $listId )) $listId = $this->listId;
//     $hashEmail = $this->emailHashFromUser( $user );
//     return $this->api->connect( "lists/" . $listId . "/members/" . $hashEmail, "DELETE" );
// }


class WinApi {
    constructor( url ){
        this.url = url;
        this.users = [];
        this.token = undefined;
    }

    async login( username, password ){
        this.token = (await request.post( {
            url: `${this.url}/api/login`,
            json: { username, password }
        })).token;
        return this;
    }

    async listUsers(){
        this.users = await request.get({
            url: `${url}/api/users`,
            headers: {
                Authorization: `Bearer ${this.token}`
            },
        }).json();
        return this.users;
    }

    async listUsersByChapter( chapter ){
        if( this.users.length === 0 ) this.users = await this.listUsers();
        return Promise.resolve( this.users.filter( u => u.chapter === chapter ));
    }
}

class Chimp {
	constructor( key, chapter ){
		this.chimp = new mailchimp( key );
		this.chapter = chapter;
	}

	async lists(){
		const lists = await this.chimp.get( {
			path: "/lists",
			query: {
				fields: "lists.id,lists.name,lists.stats.member_count,lists.stats.unsubscribe_count,lists.stats.cleaned_count"
			}
		});
		return lists.lists.filter( l => l.stats.member_count > 0);
	}

	async members( listId, status = "subscribed,unsubscribed,cleaned" ){
		if( !listId ) listId = (await this.lists())[0].id;
		const members = await this.chimp.get( {
			path: `/lists/${ listId }/members`,
			query: {
				fields: "members.email_address,members.status,members.last_changed,members.list_id,members.merge_fields",
				status: status,
				offset: 0,
				count: 3000
			}
		});
		return members.members;
	}

	hashEmail( email ){
		return md5( email.trim().toLowerCase());
	}

	async deleteMember( email, listId  ){
		return this.chimp.delete( {
			path: `lists/${listId}/members/${this.hashEmail( email )}`
		} );
	}

	async changeUserEmail( originalEmailAddress, updatedUser, listId ){
		let result = await this.deleteMember( originalEmailAddress, listId );
		if( result.statusCode === 204 ){
			console.log( "deleted " + originalEmailAddress );
			console.log( result );
			return this.addOrUpdateUser( updatedUser, listId );
		} else {
			console.log( "WARNING: Couldn't delete member " + oldUser.email + " from " + this.chapter + " on mailchimp." );
		}
	}

	async addOrUpdateUser( user, listId ){
		if( user.merge_fields ) user = this.memberToUser( user );
		const hash = this.hashEmail( user.email );
		const request = {
			path: `lists/${listId}/members/${hash}`,
			body: {
				email_address: user.email,
				status_if_new: user.optIn === 1 ? "subscribed" : "unsubscribed",
				status: user.optIn === 1 ? "subscribed" : "unsubscribed",
				merge_fields: {
					USERNAME: user.username.length === 0 ? user.email : user.username,
					FNAME: user.firstName,
					LNAME: user.lastName,
					EMAIL: user.email,
					CHAPTER: user.chapter
				}
			}
		};
		console.log( request );
		return await this.chimp.put( request )
	}

	memberToUser( member ){
		return {
			"username": member["merge_fields"]["USERNAME"],
			"chapter": member["merge_fields"]["CHAPTER"] || "Eastern Ontario",
			"firstName": member["merge_fields"]["FNAME"].length === 0 ? "Unknown" : member["merge_fields"]["FNAME"],
			"lastName": member["merge_fields"]["LNAME"].length === 0 ? "Unknown" : member["merge_fields"]["LNAME"],
			"email": member["email_address"],
			"status": member["status"],
			"optIn": member["status"] === "cleaned" ? 0 : 1
		}
	}
}

/**
 * - change all aecl.ca email addresses on the mailchimp chapter
 * 		- get all aecl.ca users
 * 		- for each one, delete them, then add them with the update email address, and set them to unsubscribed
 *
 * 	- update members on the Canada mailchimp chapter
 *
 * - change all aecl.ca email addresses directly in wincanada database with:
 * UPDATE users SET email = REPLACE(email, "@aecl.ca", "@cnl.ca") where email like "%@aecl.ca";
 *
 * - synchronize the subscription status for all users
 */

(async function() {
					// const chimp = new Chimp( config["CHAPTER_DETAILS"]["Eastern Ontario"].mcApikey, "Eastern Ontario" );
					// const listId = (await chimp.lists())[0].id;

	// update mailing list email address, set them all to unsubscribed

					// const members = (await chimpEO.members( listId )).filter( m => {
					// 	return m.email_address.indexOf( "@aecl.ca" ) !== -1;
					// });
					//
					// for( let i = 0; i < members.length; i++ ){
					// 	const user = chimpEO.memberToUser( members[i]);
					// 	const oldEmail = user.email;
					// 	user.email = user.email.replace( "@aecl.ca", "@cnl.ca" );
					// 	console.log( "Updating from: ", oldEmail, user.email );
					// 	const result = await chimpEO.changeUserEmail( oldEmail, user, listId );
					// 	console.log( result.statusCode )
					// }


	// update status from unsubscribed where applicable

					// const chapter = "Eastern Ontario";
					// const win = await new WinApi( "https://canada.womeninnuclear.org" ).login( "mskullcap", "daedalus" );
					// const allUsers = await win.listUsers();
					// const users = allUsers.filter( u => u.chapter === chapter && u.email.indexOf( "@cnl.ca" ) !== -1 && u.optIn == 1 ); // all subscribed cnl.ca members
					//
					// for( let i = 62; i < users.length; i++ ){
					// 	users[i].optIn = 1;
					// 	console.log( "Setting " + i + " " + users[i].email + " to 'subscribed'" );
					// 	const result = await chimpEO.addOrUpdateUser( users[i], listId );
					// 	console.log( result.statusCode );
					// }



	const chapters = ["Bruce", "Golden Horseshoe", "Durham", "New Brunswick", "WIM/WiN Saskatchewan"];
	const j = 4;

	const chapter = chapters[j];
	const chimp = new Chimp( config["CHAPTER_DETAILS"][chapter].mcApikey, chapter );
	const listId = (await chimp.lists()).find( l => {
		console.log( l );
		return l.name === "WiN Members"
	} ).id;

	const members = (await chimp.members( listId )).filter( m => {
		return m.email_address.indexOf( "@aecl.ca" ) !== -1;
	});

	for( let i = 0; i < members.length; i++ ){
		const user = chimp.memberToUser( members[i]);
		const oldEmail = user.email;
		user.email = user.email.replace( "@aecl.ca", "@cnl.ca" );
		console.log( "Updating " + i + " from: ", oldEmail, user.email );
		const result = await chimp.changeUserEmail( oldEmail, user, listId );
		console.log( result.statusCode )
	}

	const win = await new WinApi( "https://canada.womeninnuclear.org" ).login( config["ADMIN_USER"], config["ADMIN_PASSWORD"] );
	const allUsers = await win.listUsers();
	const users = allUsers.filter( u => u.chapter === chapter && u.email.indexOf( "@cnl.ca" ) !== -1 && u.optIn == 1 ); // all subscribed cnl.ca members

	for( let i = 0; i < users.length; i++ ){
		users[i].optIn = 1;
		console.log( "Setting " + i + " " + users[i].email + " to 'subscribed'" );
		try{
			const result = await chimp.addOrUpdateUser( users[i], listId );
			console.log( result.statusCode );
		} catch( e ){
			console.log( e );
		}
	}

	// console.log( members );

	//
	//
	// const member = members.filter( m => {
	// 	return m.email_address.indexOf( "@aecl.ca" ) !== -1 )
	// 	// return m.merge_fields.LNAME.toLowerCase() === "torvi"
	// });
	// const user = chimpEO.memberToUser( member );
	// const oldEmail = user.email;
	// user.email = "test_ellen.torvi@cnl.ca";
	// await chimpEO.changeUserEmail( oldEmail, user, listId );
	// result = await chimpEO.addOrUpdateUser( user, listId );
	// console.log( result );

	// const chapter = "Eastern Ontario";
	// const win = await new WinApi( "https://canada.womeninnuclear.org" ).login( "mskullcap", "daedalus" );
	// const allUsers = await win.listUsers();
	// const users = allUsers.filter( u => u.chapter === chapter && u.email.indexOf( "@aecl.ca" ) !== -1 );
	// console.log( users );

    // // Cleaned members have to be deleted, then re-added?
    // const mu = members.members.filter( m => m.status === "cleaned")[0];
    // const fixUser = users.filter( u => u.email === mu.email_address );
	//
    // const fixedUser = JSON.parse( await request.post({
    //     url: `${url}/api/users/${fixUser.username}`,
    //     headers: {
    //         Authorization: `Bearer ${token}`
    //     },
    //     json: fixUser
    // }));


})();

// , ( error, response, body ) => {
//     console.log( body );
// } );
