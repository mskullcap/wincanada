#!/usr/bin/env bash
read token

server=$1

if [[ -z $token ]] || [[ -z $server ]]
then
	echo "login.sh -u <username> -p <password> <server> | get-users <server>"
	exit 1
fi

curl --silent \
	 -X "GET" "$server/api/users" \
     -H "Content-Type: application/json; charset=utf-8" \
     -H "Authorization: Bearer $token" \
     | jq
