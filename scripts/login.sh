#!/usr/bin/env bash

readopt='getopts $opts opt;rc=$?;[ $rc$opt == 0? ]&&exit 1;[ $rc == 0 ]||{ shift $[OPTIND-1];false; }'
opts=u:p:

while eval $readopt
do
  case $opt in
	  u)
		 username=$OPTARG
		 ;;
	  p)
		 password=$OPTARG
		 ;;
  esac
done

server=$1

if [[ -z $username ]] || [[ -z $password ]] || [[ -z $server ]]
then
	echo "login -u <username> -p <password> <server>"
	exit 1
fi


function generate_post_data {
  cat <<EOF
{
  "username": "$username",
  "password": "$password"
}
EOF
}

# curl -X "POST" "http://localhost:8080/api/login" \
curl --silent \
	 -X "POST" "$server/api/login" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d "$(generate_post_data)" \
     | jq '.token'  \
     | sed -e 's/^"//' -e 's/"$//'
