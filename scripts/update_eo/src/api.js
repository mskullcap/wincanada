const request = require( "request-promise-native" );
const mailchimp = require( "mailchimp-api-v3" );
const config = require( "../../../_do_not_commit/config.json");
const md5 = require( "md5");
const csv = require("fast-csv");

const url = "https://canada.womeninnuclear.org";

class WinApi {
    constructor( url ){
        this.url = url;
        this.users = [];
        this.token = undefined;
    }

    async login( username, password ){
        this.token = (await request.post( {
            url: `${this.url}/api/login`,
            json: { username, password }
        })).token;
        return this;
    }

    async listUsers(){
        this.users = await request.get({
            url: `${url}/api/users`,
            headers: {
                Authorization: `Bearer ${this.token}`
            },
        }).json();
        return this.users;
    }

    async listUsersByChapter( chapter ){
        if( this.users.length === 0 ) this.users = await this.listUsers();
        return Promise.resolve( this.users.filter( u => u.chapter === chapter ));
    }
}

class Chimp {
	constructor( key, chapter ){
		this.chimp = new mailchimp( key );
		this.chapter = chapter;
	}

	async lists(){
		const lists = await this.chimp.get( {
			path: "/lists",
			query: {
				fields: "lists.id,lists.name,lists.stats.member_count,lists.stats.unsubscribe_count,lists.stats.cleaned_count"
			}
		});
		return lists.lists.filter( l => l.stats.member_count > 0);
	}

	async members( listId, status = "subscribed,unsubscribed,cleaned" ){
		if( !listId ) listId = (await this.lists())[0].id;
		const members = await this.chimp.get( {
			path: `/lists/${ listId }/members`,
			query: {
				fields: "members.email_address,members.status,members.last_changed,members.list_id,members.merge_fields",
				status: status,
				offset: 0,
				count: 3000
			}
		});
		return members.members;
	}

	hashEmail( email ){
		return md5( email.trim().toLowerCase());
	}

	async deleteMember( email, listId  ){
		return this.chimp.delete( {
			path: `lists/${listId}/members/${this.hashEmail( email )}`
		} );
	}

	async changeUserEmail( originalEmailAddress, updatedUser, listId ){
		let result = await this.deleteMember( originalEmailAddress, listId );
		if( result.statusCode === 204 ){
			console.log( "deleted " + originalEmailAddress );
			console.log( result );
			return this.addOrUpdateUser( updatedUser, listId );
		} else {
			console.log( "WARNING: Couldn't delete member " + oldUser.email + " from " + this.chapter + " on mailchimp." );
		}
	}

	async addOrUpdateUser( user, listId ){
		if( user.merge_fields ) user = this.memberToUser( user );
		const hash = this.hashEmail( user.email );
		const request = {
			path: `lists/${listId}/members/${hash}`,
			body: {
				email_address: user.email,
				status_if_new: user.optIn === 1 ? "subscribed" : "unsubscribed",
				status: user.optIn === 1 ? "subscribed" : "unsubscribed",
				merge_fields: {
					USERNAME: user.username.length === 0 ? user.email : user.username,
					FNAME: user.firstName,
					LNAME: user.lastName,
					EMAIL: user.email,
					CHAPTER: user.chapter
				}
			}
		};
		console.log( request );
		return await this.chimp.put( request )
	}

	memberToUser( member ){
		return {
			"username": member["merge_fields"]["USERNAME"],
			"chapter": member["merge_fields"]["CHAPTER"] || "Eastern Ontario",
			"firstName": member["merge_fields"]["FNAME"].length === 0 ? "Unknown" : member["merge_fields"]["FNAME"],
			"lastName": member["merge_fields"]["LNAME"].length === 0 ? "Unknown" : member["merge_fields"]["LNAME"],
			"email": member["email_address"],
			"status": member["status"],
			"optIn": member["status"] === "cleaned" ? 0 : 1
		}
	}
}

(async function() {

	//	First Name,Last Name,Email,In Database?,CNL?,CNSC>,Subscribed?,From Larkin's List?,From the Website?,BOUNCE?,Potential exceptions?
	const csvMembers = await new Promise(( resolve, reject ) => {
		const m = [];
		csv.fromPath( "./eo_members_list_2018.csv")
			.on("data", data => {
				m.push({
					firstName: data[0].trim(),
					lastName: data[1].trim(),
					email: data[2].toLowerCase(),
					optIn: data[6] === "Yes" ? 1 : 0
				});
			})
			.on("end", function () {
				resolve(
					m.sort(( a, b ) => {
						return a.lastName.localeCompare( b.lastName ) || a.firstName.localeCompare( b.firstName ) || a.email.localeCompare( b.email )
					})
				);
			});
	});

	const chapter = "Eastern Ontario";
	const win = await new WinApi( "https://canada.womeninnuclear.org" ).login( config["ADMIN_USER"], config["ADMIN_PASSWORD"] );
	const allUsers = await win.listUsers();
	const winMembers = allUsers
		.filter( u => u.chapter === chapter )
		.map( u => {
			return {
				firstName: u.firstName,
				lastName: u.lastName,
				email: u.email,
				optIn: u.optIn
			}
		})
		.sort(( a, b ) => {
			return a.lastName.localeCompare( b.lastName ) || a.firstName.localeCompare( b.firstName ) || a.email.localeCompare( b.email )
		});

	// let intersection = csvMembers.filter(x => !winMembers.includes(x));
	// console.log( intersection );

	csvMembers.forEach( (csvMember, i) => {
		if( !winMembers.find( m => {
			return m.lastName === csvMember.lastName && m.firstName.split(" ")[0] === csvMember.firstName.split(" ")[0]
		})){
			console.log("Missing from win database: ", csvMember.lastName, ",", csvMember.firstName, csvMember.email );
			delete csvMembers[i];
		}
	});

	csvMembers.forEach( (csvMember, i) => {
		if( !winMembers.find( m => {
			return m.lastName === csvMember.lastName &&
				m.firstName.split(" ")[0] === csvMember.firstName.split(" ")[0] &&
				m.optIn == csvMember.optIn

		})){
			console.log("Imported list user optIn status incorrect: ", csvMember.lastName, ",", csvMember.firstName, csvMember.email, csvMember.optIn == 1 ? "subscribed" : "unsubscribed" );
		}
	});

	const chimp = new Chimp( config["CHAPTER_DETAILS"]["Eastern Ontario"].mcApikey, "Eastern Ontario" );
	const listId = (await chimp.lists())[0].id;
	let chimpMembers = (await chimp.members( listId )).filter( m => {
		return m.status === "subscribed"
	});

	chimpMembers = chimpMembers.map( m => {
		return {
			firstName: m.merge_fields["FNAME"],
			lastName: m.merge_fields["LNAME"],
			email: m.email_address,
			optIn: 1
		}
	});

	winMembers.filter( m => m.optIn === 1 ).forEach( chimpMember => {
		if( !winMembers.find( m => {
			return m.lastName === chimpMember.lastName &&
				m.firstName.split(" ")[0] === chimpMember.firstName.split(" ")[0] &&
				m.optIn == chimpMember.optIn

		})){
			console.log("Imported list user optIn status incorrect: ", chimpMember.lastName, ",", chimpMember.firstName, chimpMember.email, chimpMember.optIn == 1 ? "subscribed" : "unsubscribed" );
		}
	});




})();

