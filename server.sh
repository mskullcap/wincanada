#!/usr/bin/env bash
ROOT=`pwd`;
cd server/api
php -d file_uploads=On -S 0.0.0.0:8080 -t ${ROOT}/server index.php
